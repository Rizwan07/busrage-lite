//
//  ADRequestResult.h
//  Bus Rage
//
//  Created by Viktor on 20.11.12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADRequestResult : NSObject

@property(nonatomic,retain) UIImage * bannerImage;
@property int bannerId;
@property int bannerState;

- (id)initWithImageUrl:(NSURL*) anUrl bannerState:(int) state bannerId:(int) Id;

@end
