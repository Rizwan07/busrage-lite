//
//  ADRequester.h
//  Bus Rage
//
//  Created by Viktor on 20.11.12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ADRequester : NSObject
+ (NSArray*) getADBanners;

@end
