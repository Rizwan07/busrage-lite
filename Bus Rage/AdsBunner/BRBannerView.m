//
//  ADBannerView.m
//  Bus Rage
//
//  Created by Viktor on 20.11.12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "BRBannerView.h"
#import "ADRequester.h"
#import "ADRequestResult.h"

@interface BRBannerView () {
    NSArray * banners;
}

- (void) setBanners:(NSArray*) banners;

@end


@implementation BRBannerView

@synthesize bannerImageViews = _bannerImageViews;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

+ (void) getADBannerView:(NSObject<BRBannerViewDelegate>*) delegate
{
    NSArray * views = [[NSBundle mainBundle] loadNibNamed:@"BRBannerView" owner:nil options:nil];
    BRBannerView * bannerView = [views objectAtIndex:0];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray * banners = [ADRequester getADBanners];
        [bannerView setBanners:banners];
        [delegate bannerLoadded:bannerView];
    });
    
}

+ (void) getADBanners:(NSObject<BRBannerViewDelegate>*) delegate
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray * banners = [ADRequester getADBanners];
        [delegate imagesLoaded:banners];
    });
}

+ (void) getADImages:(NSObject<BRBannerViewDelegate>*) delegate
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSArray * banners = [ADRequester getADBanners];
        NSMutableArray * images = [NSMutableArray array];
        for (ADRequestResult * requestResult in banners) {
            [images addObject:requestResult.bannerImage];
        }
        [delegate imagesLoaded:images];
    });
}

- (void) setBanners:(NSArray*) _banners
{
    banners = _banners;
    int i = 0;
    for (UIImageView * imageView in self.bannerImageViews) {
         imageView.image = ((ADRequestResult*)[banners objectAtIndex:i]).bannerImage;
        
        if (i<banners.count) {
            i++;
        }
    }
    
}

- (void)dealloc {
    [_bannerImageViews release];
    [super dealloc];
}
@end
