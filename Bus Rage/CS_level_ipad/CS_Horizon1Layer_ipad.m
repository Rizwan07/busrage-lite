//
//  CS_Horizon1Layer_ipad.m
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CS_Horizon1Layer_ipad.h"

@implementation CS_Horizon1Layer_ipad
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        horizonSprite = [CCSprite spriteWithFile:@"Horizon_1_CS_ipad.png"];
        [self addChild:horizonSprite z:0];
        horizonSprite.visible = YES;
        horizonSprite.position = ccp(447*2.1333333, 150*2.4);//447...33        
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    if (runDistance == 0 || horizonSprite.position.x < 33.0) {
        horizonSprite.position = ccp(447*2.1333333, 150*2.4);
    }
    if (speed > 0) {
        CGPoint pos;
        pos = horizonSprite.position;
        pos.x -= 0.5 * speed/2;
        horizonSprite.position = pos; 
    }
}

@end
