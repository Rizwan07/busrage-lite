//
//  CS_Horizon1Layer_ipad.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CCLayer.h"

@interface CS_Horizon1Layer_ipad : CCLayer {
    CCSprite *horizonSprite;
}
- (void) runMove:(int)runDistance speed:(int)speed;

@end
