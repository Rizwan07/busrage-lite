//
//  CS_GameLayer_ipad.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CCLayer.h"

#import "CS_HousesLayer_ipad.h"
#import "CS_FenceLayer_ipad.h"
#import "CS_RoadLayer_ipad.h"
#import "CS_TreesLayer_ipad.h"
#import "CS_HillsLayer_ipad.h"
#import "CS_Horizon1Layer_ipad.h"
#import "CS_BaseLayer_ipad.h"

@interface CS_GameLayer_ipad : CCLayer {
    CS_HousesLayer_ipad *cs_houseLayer;
    CS_FenceLayer_ipad *cs_fenceLayer;
    CS_RoadLayer_ipad *cs_roadLayer;
    CS_TreesLayer_ipad *cs_treeLayer;
    CS_HillsLayer_ipad *cs_hillLayer;
    CS_Horizon1Layer_ipad *cs_horizon1Layer;
    CS_BaseLayer_ipad *cs_baseLayer;
    
    CCSprite *cloud_dustSprite;
    int m_timecount;
}
- (void) runMove:(int)runDistance speed:(int)speed;




@end
