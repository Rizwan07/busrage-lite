//
//  CS_FenceLayer_ipad.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CCLayer.h"

@interface CS_FenceLayer_ipad : CCLayer {
    CCSprite *roadFence1;
    CCSprite *roadFence2;
    CCSprite *roadFence3;
    CCSprite *roadFence4;
    int current_fence;
    int runCount;
    CCSprite *bush1Sprite_L;
    CCSprite *bush2Sprite_L;  
    CCSprite *bush1Sprite_R;
    CCSprite *bush2Sprite_R;
    CCSprite *grass1Sprite;
    CCSprite *grass2Sprite;
    
    int m_Bush1_L[3];
    int m_Bush2_L[3];
    int m_Bush1_R[3];
    int m_Bush2_R[3];
    int m_Grass1[3];
    int m_Grass2[3];
    
    NSArray*    m_Objects;
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) showBushes1_L:(int)runDistance;
- (void) showBushes2_L:(int)runDistance;
- (void) showBushes1_R:(int)runDistance;
- (void) showBushes2_R:(int)runDistance;
- (void) showGrass1:(int)runDistance;
- (void) showGrass2:(int)runDistance;
- (void) showFence:(int)speed;
- (void) changeFenceSprite;

@end
