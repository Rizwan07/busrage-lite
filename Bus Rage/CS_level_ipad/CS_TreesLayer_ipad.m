//
//  CS_TreesLayer_ipad.m
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CS_TreesLayer_ipad.h"

#import "EnumConstances.h"

#define SHOW_LIMIT 300

@implementation CS_TreesLayer_ipad
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        m_Objects = [NSArray arrayWithObjects:
                     @"2, 70",//windmill                     
                     @"0, 150",//tree1  
                     @"3, 170",//bush1
                     @"1, 200",//tree2  
                     @"4, 250",//bush2
                     
                     @"2, 370",//windmill                     
                     @"0, 450",//tree1  
                     @"3, 470",//bush1
                     @"1, 500",//tree2  
                     @"4, 550",//bush2
                     
                     @"2, 670",//windmill                     
                     @"0, 750",//tree1  
                     @"3, 770",//bush1                       
                     @"4, 850",//bush2
                     @"1, 1000",//tree2
                     
                     //@"2, 1100",//windmill
                     
                     nil];
        int tree1_index = 0;
        int tree2_index = 0;
        int windmill_index = 0;
        int bush1_index = 0;
        int bush2_index = 0;
        for (int i = 0; i < [m_Objects count]; i++)
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case T_OINFO_TREE1:                
                    m_Tree1[tree1_index] = [[params objectAtIndex:1] intValue];
                    tree1_index++;                
                    break;	
                case T_OINFO_TREE2:                
                    m_Tree2[tree2_index] = [[params objectAtIndex:1] intValue];
                    tree2_index++;                
                    break;   
                case T_OINFO_WINDMILL:                
                    m_Windmill[windmill_index] = [[params objectAtIndex:1] intValue];
                    windmill_index++;                
                    break;
                case T_OINFO_BUSH1:
                    m_Bush1[bush1_index] = [[params objectAtIndex:1] intValue];
                    bush1_index++;
                    break;
                case T_OINFO_BUSH2:
                    m_Bush2[bush2_index] = [[params objectAtIndex:1] intValue];
                    bush2_index++;
                    break;
            }
        }
        
//        CGSize size = [[CCDirector sharedDirector] winSize];

//        CCSprite *lineSprite;
//        float ss = 70.0;
//        float sum = -350.0;
//        for (int i = 0; i < 50; i ++)
//        {  
//            sum += ss;
//            lineSprite = [CCSprite spriteWithFile:@"line2.png"];
//            //[self addChild:lineSprite z:10 + 1];
//            lineSprite.position = ccp(sum, size.height/2);           
//            
//            NSLog(@"%i: %.9f, %.9f", i+1, ss, sum);
//            ss = ss * 0.897;             
//        }
//        
//        float ss1 = 25.0;
//        float sum1 = 0.0;
//        for (int i = 0; i < 50; i ++)
//        {           
//            lineSprite = [CCSprite spriteWithFile:@"line.png"];
//            //[self addChild:lineSprite z:10 + 1];
//            sum1 += ss1;
//            lineSprite.position = ccp(size.width/2, sum1);           
//            
//            NSLog(@"%i: %.9f, %.9f", i+1, ss1, sum1);
//            ss1 = ss1 * 0.831;//0.840;             
//        }
        
        tree1Sprite = [CCSprite spriteWithFile:@"Tree_1_CS_ipad.png"];
        [self addChild:tree1Sprite z:0];        
        tree1Sprite.visible = NO;
        //tree1Sprite.position = ccp(326, 147);
        //tree1Sprite.scale = 0.03;//0.2;//0.03;
        
        tree2Sprite = [CCSprite spriteWithFile:@"Tree_2_CS_ipad.png"];
        [self addChild:tree2Sprite z:0];        
        tree2Sprite.visible = NO;
        //tree2Sprite.position = ccp(326, 147);
        //tree2Sprite.scale = 0.03;//0.2;//0.03;
        
        windmillSprite = [CCSprite spriteWithFile:@"windmill_CS_ipad.png"];
        [self addChild:windmillSprite z:0];        
        windmillSprite.visible = NO;
        windmillflierSprite = [CCSprite spriteWithFile:@"windmill_flier_CS_ipad.png"];
        [windmillSprite addChild:windmillflierSprite];
        windmillflierSprite.position = ccp(106*2.1333333, 432*2.4);
        windmillflierSprite.rotation = 0;
        
        bush1Sprite = [CCSprite spriteWithFile:@"Bushes1_CS_ipad.png"];
        [self addChild:bush1Sprite z:0];
        bush1Sprite.visible = NO;
        
        bush2Sprite = [CCSprite spriteWithFile:@"Bushes2_CS_ipad.png"];
        [self addChild:bush2Sprite z:0];
        bush2Sprite.visible = NO;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance
{
    [self showTree1:runDistance];
    [self showTree2:runDistance];
    [self showWindmill:runDistance];
    [self showBush1:runDistance];
    [self showBush2:runDistance];
}
- (void) showTree1:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Tree1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    tree1Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        tree1Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = tree1Sprite.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;
        tree1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326*2.1333333, 147*2.4));//a 
        float dist2 = ccpDistance(cur_pos, CGPointMake(326*2.1333333, 147*2.4));//b
        tree1Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        tree1Sprite.scale = tree1Sprite.scale * 8;
    }
}
- (void) showTree2:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Tree2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    tree2Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        tree2Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = tree2Sprite.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        tree2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326*2.1333333, 147*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(326*2.1333333, 147*2.4));//b
        tree2Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        tree2Sprite.scale = tree2Sprite.scale * 2.604 * 4;
    }
}
- (void) showWindmill:(int)runDistance
{
    int diff_BusWindmill = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 4; i++)
    {
        tmpPos = m_Windmill[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusWindmill = diff[i];
        } else {
            diff_BusWindmill = MIN(diff_BusWindmill, diff[i]);                
        }
    }
    windmillSprite.visible = NO;
    if (diff_BusWindmill > 0 && diff_BusWindmill < SHOW_LIMIT) {
        windmillSprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusWindmill / 10) {
                sum += ss * 0.897 / 10 * (diff_BusWindmill % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusWindmill / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusWindmill % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = windmillSprite.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        windmillSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326*2.1333333, 147*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(326*2.1333333, 147*2.4));//b
        windmillSprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        windmillSprite.scale = windmillSprite.scale * 1.25 * 4;
        windmillflierSprite.rotation += 5;
        if (windmillflierSprite.rotation > 360)
            windmillflierSprite.rotation = 0;
    }
}
- (void) showBush1:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush1Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush1Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.897 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;// = bush1Sprite.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        bush1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326*2.1333333, 147*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(326*2.1333333, 147*2.4));//b
        bush1Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        bush1Sprite.scale = bush1Sprite.scale * 6;
    }
}
- (void) showBush2:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush2Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush2Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.897 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;// = bush1Sprite.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        bush2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(326*2.1333333, 147*2.4));//b
        bush2Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        bush2Sprite.scale = bush2Sprite.scale * 6;
    }
}

@end
