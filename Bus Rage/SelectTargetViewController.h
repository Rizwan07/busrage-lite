//
//  SelectTargetViewController.h
//  Bus Rage
//
//  Created by Jin Tie on 5/9/12.
//  Copyright (c) 2012 k. All rights reserved.
//
//com.yourcompany.${PRODUCT_NAME:rfc1034identifier}

#import <UIKit/UIKit.h>
//#import "FBConnect.h"
//#import "FBRequest.h"
#import "FBConnectSingleton.h"
#import "SimpleAudioEngine.h"
#import "RootViewController.h"

@interface SelectTargetViewController : UIViewController <UITextFieldDelegate, FBConnectDelegate, UITableViewDataSource, UITableViewDelegate> {
    RootViewController *_rootViewController;
    NSArray *m_imageObjects;
    //Facebook* faceBookT;
    FBConnectSingleton *fbConnect;
    int currentAPICall;
    UIActivityIndicatorView *activityIndicator;
    UITableView *facebookTableView;
    NSMutableArray *friends;
    //NSMutableArray *facebookImageObjectIDArray;
    UIImageView *facebookImage;
    bool bFacebookImageConnect;
    
    int currentSelectThumbnailNum;
    UILabel *messageLabel;
    UIView *messageView; 
    UIAlertView *alert1;   
    NSMutableArray *m_TargetButtonArray;
    bool m_bDoubleClickRun;
//    FBRequest *myProfile;
    IBOutlet UIButton *chooseForAllBtn;
    int currentTargetChoose4All;
    int selectedTargetTag;
}

@property (retain) RootViewController *rootViewController;
@property (nonatomic, retain) IBOutlet UIImageView *targetImageView;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UILabel *targetNationLabel;
@property (nonatomic, retain) IBOutlet UITextField *enterNameField;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
//@property (nonatomic, retain) Facebook* faceBookT;
@property (nonatomic, retain) IBOutlet UIButton *nextButton;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic, retain) IBOutlet UITableView *facebookTableView;
@property (nonatomic, retain) IBOutlet UIImageView *facebookImage;
@property (nonatomic, retain) UILabel *messageLabel;
@property (nonatomic, retain) UIView *messageView;
@property (nonatomic, retain) IBOutlet UIButton *m_friendButton;

- (IBAction)backTapped:(id)sender;
- (IBAction)nextTapped:(id)sender;
- (IBAction)chooseforallTapped:(id)sender;
- (IBAction)picClicked:(id)sender;
- (IBAction)friendsTapped:(id)sender;
- (void) facebookLogin;

- (void)showMessage:(NSString *)message;
- (UIImage *)imageForObject:(NSString *)objectID;
- (int) currentTargetSelectNum;
- (void) showAlert:(int)levelNum limitNum:(int)limitNum messageIndex:(int)Index;
@end
