//
//  CutscenePlayer.m
//  Bus Rage
//
//  Created by Jin Tie on 5/20/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CutscenePlayer.h"
#import "GameManager.h"
#import "SimpleAudioEngine.h"

@implementation CutscenePlayer
@synthesize theMovie;
@synthesize delegate;
static CutscenePlayer* instance = nil;

+(CutscenePlayer*) sharedInstance
{
    @synchronized(self) {
        if(instance == nil)
        {
            instance = [[CutscenePlayer alloc] init];
        }
    }
    return instance;
}

-(void) load:(int)num forIpad:(BOOL)isiPad
{
    m_current_num = num;
    if ([GameManager sharedGameManager].m_bEnabledMusic) {
        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    }
	NSBundle *bundle = [NSBundle mainBundle];
    
	NSString *moviePath = nil;
    switch (num) {
        case 1:
            if (!isiPad) {
                
                moviePath = [bundle pathForResource:@"splash screen_animation_ae" ofType:@"mov"];
            }
            else {
                moviePath = [bundle pathForResource:@"splash screen_animation_ae_ipad" ofType:@"mov"];
            }
            break;
        case 2:
            moviePath = [bundle pathForResource:@"Ambulance video redo" ofType:@"mov"];
            break;  
        case 3:
            moviePath = [bundle pathForResource:@"Dead_MOV" ofType:@"mov"];
            break; 
        case 4:
            moviePath = [bundle pathForResource:@"Ship Crash" ofType:@"mov"];
            break;
        default:
            //moviePath = nil;
            break;
    }
        
	NSURL *theURL = [NSURL fileURLWithPath:moviePath];
	
	theMovie = [[MPMoviePlayerController alloc] initWithContentURL:theURL];	
	
	theMovie.controlStyle = MPMovieControlStyleNone;
    [theMovie setFullscreen:YES animated:YES];
	
	[[NSNotificationCenter defaultCenter]
	 addObserver: self
	 selector: @selector(myMovieFinishedCallback:)
	 name: MPMoviePlayerPlaybackDidFinishNotification
	 object: theMovie];	
	[theMovie setInitialPlaybackTime:0.5];
    [theMovie prepareToPlay];
}

-(void) play
{
	[theMovie play];
}

-(void) myMovieFinishedCallback: (NSNotification*) aNotification
{
    [[NSNotificationCenter defaultCenter]
	 removeObserver: self
	 name: MPMoviePlayerPlaybackDidFinishNotification
	 object: theMovie];
	
	[theMovie.view removeFromSuperview];
    [theMovie release];
	
    [self.delegate didFinishedPlay];
	//iKissAppDelegate* mainDelegate = (iKissAppDelegate*)[[UIApplication sharedApplication] delegate];
	//[mainDelegate launchMainMenu];    
    if ([GameManager sharedGameManager].m_bEnabledMusic && m_current_num != 1) {
        [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
    }
}

@end
