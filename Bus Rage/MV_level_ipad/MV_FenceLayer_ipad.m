//
//  CS_FenceLayer_ipad.m
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "MV_FenceLayer_ipad.h"
#import "EnumConstances.h"

#define SHOW_LIMIT 300

@implementation MV_FenceLayer_ipad

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        roadFence1 = [CCSprite spriteWithFile:@"fence_roadside_MV_01_ipad.png"];
        [self addChild:roadFence1 z:1];   
        roadFence2 = [CCSprite spriteWithFile:@"fence_roadside_MV_02_ipad.png"];
        [self addChild:roadFence2 z:1]; 
        roadFence3 = [CCSprite spriteWithFile:@"fence_roadside_MV_03_ipad.png"];
        [self addChild:roadFence3 z:1]; 
        roadFence4 = [CCSprite spriteWithFile:@"fence_roadside_MV_04_ipad.png"];
        [self addChild:roadFence4 z:1]; 
        roadFence1.position = roadFence2.position = roadFence3.position = roadFence4.position = ccp(size.width/2, 160*2.4);
        roadFence1.visible = roadFence2.visible = roadFence3.visible = roadFence4.visible = NO;
        roadFence1.visible = YES;
        
        current_fence = 1;
        runCount = 0;
        
        m_Objects = [NSArray arrayWithObjects:
                     @"1, 10",//bush2
                     @"0, 30",//bush1 
                     @"2, 50",//grass1   
                     @"3, 200",//grass2                     
                     @"10, 10",
                     @"11, 50",
                     @"10, 120",
                     @"11, 170",//0-300
                     
                     @"1, 310",//bush2 300-600
                     @"0, 330",//bush1 
                     @"2, 350",//grass1   
                     @"3, 500",//grass2                     
                     @"10, 310",
                     @"11, 350",
                     @"10, 420",
                     @"11, 470",                     
                     
                     @"1, 610",//bush2 600-900
                     @"0, 630",//bush1 
                     @"2, 650",//grass1   
                     @"3, 800",//grass2                     
                     @"10, 610",
                     @"11, 650",
                     @"10, 720",
                     @"11, 770",
                     nil];
        int fence1_index_l = 0;
        int fence2_index_l = 0;
        int fence3_index_l = 0;//add MV
        int fence4_index_l = 0;//add MV
        int fence1_index_r = 0;
        int fence2_index_r = 0;
        int fence3_index_r = 0;//add MV
        int fence4_index_r = 0;//add MV
        int grass1_index = 0;
        int grass2_index = 0;
        int roadSign_index = 0;
        for (int i = 0; i < [m_Objects count]; i++ )
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case F_OINFO_FENCE1_L:
                    m_Bush1_L[fence1_index_l] = [[params objectAtIndex:1] intValue];
                    fence1_index_l++;
                    break;	
                case F_OINFO_FENCE2_L:
                    m_Bush2_L[fence2_index_l] = [[params objectAtIndex:1] intValue];
                    fence2_index_l++;                
                    break;
                case F_OINFO_FENCE1_R:                
                    m_Bush1_R[fence1_index_r] = [[params objectAtIndex:1] intValue];
                    fence1_index_r++;                
                    break;
                case F_OINFO_FENCE2_R:                
                    m_Bush2_R[fence2_index_r] = [[params objectAtIndex:1] intValue];
                    fence2_index_r++;                
                    break;
                case F_OINFO_GRASS1:                
                    m_Grass1[grass1_index] = [[params objectAtIndex:1] intValue];
                    grass1_index++;                
                    break;
                case F_OINFO_GRASS2:                
                    m_Grass2[grass2_index] = [[params objectAtIndex:1] intValue];
                    grass2_index++;                
                    break;
                    
                    //add MV
                case F_OINFO_FENCE3_L:                
                    m_Bush3_L[fence3_index_l] = [[params objectAtIndex:1] intValue];
                    fence3_index_l++;                
                    break; 
                case F_OINFO_FENCE4_L:                
                    m_Bush4_L[fence4_index_l] = [[params objectAtIndex:1] intValue];
                    fence4_index_l++;                
                    break;
                case F_OINFO_FENCE3_R:                
                    m_Bush3_R[fence3_index_r] = [[params objectAtIndex:1] intValue];
                    fence3_index_r++;                
                    break;
                case F_OINFO_FENCE4_R:                
                    m_Bush4_R[fence4_index_r] = [[params objectAtIndex:1] intValue];
                    fence4_index_r++;                
                    break;
                case F_OINFO_ROADSIGN:                
                    m_RoadSign[roadSign_index] = [[params objectAtIndex:1] intValue];
                    roadSign_index++;                
                    break;
            }
        }
        
        bush1Sprite_L = [CCSprite spriteWithFile:@"Bushes1_CS_ipad.png"];
        [self addChild:bush1Sprite_L z:0];        
        bush1Sprite_L.visible = NO;
        
        bush2Sprite_L = [CCSprite spriteWithFile:@"Bushes2_CS_ipad.png"];
        [self addChild:bush2Sprite_L z:0];        
        bush2Sprite_L.visible = NO;
        
        bush1Sprite_R = [CCSprite spriteWithFile:@"Bushes1_CS_ipad.png"];
        [self addChild:bush1Sprite_R z:0];        
        bush1Sprite_R.visible = NO;
        
        bush2Sprite_R = [CCSprite spriteWithFile:@"Bushes2_CS_ipad.png"];
        [self addChild:bush2Sprite_R z:0];        
        bush2Sprite_R.visible = NO;
        
        grass1Sprite = [CCSprite spriteWithFile:@"Grass_CS_ipad.png"];
        [self addChild:grass1Sprite z:2];        
        grass1Sprite.visible = NO;
        grass2Sprite = [CCSprite spriteWithFile:@"Grass_CS_ipad.png"];
        [self addChild:grass2Sprite z:2];        
        grass2Sprite.visible = NO;
        
        
        //add 
        bush3Sprite_L = [CCSprite spriteWithFile:@"Bush_1_MV_ipad.png"];
        [self addChild:bush3Sprite_L z:0];        
        bush3Sprite_L.visible = NO;
        
        bush4Sprite_L = [CCSprite spriteWithFile:@"Bush_2_MV_ipad.png"];
        [self addChild:bush4Sprite_L z:0];
        bush4Sprite_L.visible = NO;
        
        bush3Sprite_R = [CCSprite spriteWithFile:@"Bush_1_MV_ipad.png"];
        [self addChild:bush3Sprite_R z:0];
        bush3Sprite_R.visible = NO;
        
        bush4Sprite_R = [CCSprite spriteWithFile:@"Bush_2_MV_ipad.png"];
        [self addChild:bush4Sprite_R z:0];
        bush4Sprite_R.visible = NO;
        
        roadSignSprite = [CCSprite spriteWithFile:@"Road_sign_MV_ipad.png"];
        [self addChild:roadSignSprite z:0];
        roadSignSprite.visible = NO;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    [self showFence:speed];
    [self showBushes1_L:runDistance];
    [self showBushes2_L:runDistance];
    
    [self showBushes1_R:runDistance];
    [self showBushes2_R:runDistance];
    
    [self showGrass1:runDistance];
    [self showGrass2:runDistance];
    
    //add
    [self showBushes3_L:runDistance];
    [self showBushes4_L:runDistance];    
    [self showBushes3_R:runDistance];
    [self showBushes4_R:runDistance];
    [self showRoadSign:runDistance];
}
- (void) changeFenceSprite
{
    current_fence++;
    if (current_fence == 5)
        current_fence = 1;
    roadFence1.visible = roadFence2.visible = roadFence3.visible = roadFence4.visible = NO;
    switch (current_fence) {
        case 1:
            roadFence1.visible = YES;
            break;
        case 2:
            roadFence2.visible = YES;
            break;
        case 3:
            roadFence3.visible = YES;
            break;
        case 4:
            roadFence4.visible = YES;
            break;
        default:
            break;
    }
}

- (void) showFence:(int)speed
{  
    runCount++;
    if (runCount > 40000)
        runCount = 0;
    
    if (speed == 0)
        return;
    
    if (speed == 10) {
        [self changeFenceSprite];
    }
    if (speed == 8) {
        if (runCount % 2 == 0) {
            [self changeFenceSprite];
        }
    }
    if (speed == 6) {
        if (runCount % 2 == 0) {
            [self changeFenceSprite];
        }
    }   
    if (speed == 4) {
        if (runCount % 2 == 0) {
            [self changeFenceSprite];
        }
    }
    if (speed == 2) {
        if (runCount % 2 == 0) {
            [self changeFenceSprite];
        }
    }
    
}
- (void) showBushes1_L:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush1_L[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush1Sprite_L.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush1Sprite_L.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = bush1Sprite_L.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;
        bush1Sprite_L.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0*2.1333333, 0), CGPointMake(405*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405*2.1333333, 148.0*2.4));//b
        bush1Sprite_L.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1;
    }
}
- (void) showBushes2_L:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush2_L[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush2Sprite_L.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush2Sprite_L.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = bush2Sprite_L.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        bush2Sprite_L.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0*2.1333333, 0), CGPointMake(405*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405*2.1333333, 148.0*2.4));//b
        bush2Sprite_L.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1;
    }
}
- (void) showBushes1_R:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush1_R[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush1Sprite_R.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush1Sprite_R.visible = YES;
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;      
        }
        CGPoint cur_pos = bush1Sprite_R.position;
        cur_pos.x = 475*2.1333333;
        cur_pos.y = sum1*2.4;
        
        bush1Sprite_R.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(475.0*2.1333333, 0), CGPointMake(475.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(475.0*2.1333333, 148.0*2.4));//b
        bush1Sprite_R.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1;
    }
}
- (void) showBushes2_R:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush2_R[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush2Sprite_R.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush2Sprite_R.visible = YES;
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;      
        }
        CGPoint cur_pos = bush2Sprite_R.position;
        cur_pos.x = 475*2.1333333;
        cur_pos.y = sum1*2.4;
        
        bush2Sprite_R.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(475.0*2.1333333, 0), CGPointMake(475.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(475.0*2.1333333, 148.0*2.4));//b
        bush2Sprite_R.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1;
    }
}
- (void) showGrass1:(int)runDistance
{
    int diff_BusGrass = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Grass1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusGrass = diff[i];
        } else {
            diff_BusGrass = MIN(diff_BusGrass, diff[i]);                
        }
    }
    grass1Sprite.visible = NO;
    if (diff_BusGrass > 0 && diff_BusGrass < SHOW_LIMIT) {
        grass1Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = 20.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusGrass / 10) {
                sum += ss * 0.82 / 10 * (diff_BusGrass % 10);            
                break;
            }
            ss = ss * 0.82;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusGrass / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusGrass % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = grass1Sprite.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;
        
        grass1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(20.0*2.1333333, 0), CGPointMake(409.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(409.0*2.1333333, 148.0*2.4));//b
        grass1Sprite.scale = (dist2 + 0.111 * dist1) / 1.111 / dist1;
    }
}
- (void) showGrass2:(int)runDistance
{
    int diff_BusGrass = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Grass2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusGrass = diff[i];
        } else {
            diff_BusGrass = MIN(diff_BusGrass, diff[i]);                
        }
    }
    grass2Sprite.visible = NO;
    if (diff_BusGrass > 0 && diff_BusGrass < SHOW_LIMIT) {
        grass2Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = 20.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusGrass / 10) {
                sum += ss * 0.82 / 10 * (diff_BusGrass % 10);            
                break;
            }
            ss = ss * 0.82;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusGrass / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusGrass % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = grass2Sprite.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;
        
        grass2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(20.0*2.1333333, 0), CGPointMake(409.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(409.0*2.1333333, 148.0*2.4));//b
        grass2Sprite.scale = (dist2 + 0.111 * dist1) / 1.111 / dist1;
    }
}

- (void) showBushes3_L:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush3_L[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush3Sprite_L.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush3Sprite_L.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        bush3Sprite_L.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0*2.1333333, 0), CGPointMake(405.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405.0*2.1333333, 148.0*2.4));//b
        bush3Sprite_L.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showBushes4_L:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush4_L[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush4Sprite_L.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush4Sprite_L.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        bush4Sprite_L.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0*2.1333333, 0), CGPointMake(405.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405.0*2.1333333, 148.0*2.4));//b
        bush4Sprite_L.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showBushes3_R:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush3_R[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush3Sprite_R.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush3Sprite_R.visible = YES;
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;      
        }
        CGPoint cur_pos;
        cur_pos.x = 475.0*2.1333333;
        cur_pos.y = sum1*2.4;  
        bush3Sprite_R.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(475.0*2.1333333, 0), CGPointMake(475.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(475.0*2.1333333, 148.0*2.4));//b
        bush3Sprite_R.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
    
}
- (void) showBushes4_R:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush4_R[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush4Sprite_R.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush4Sprite_R.visible = YES;
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;      
        }
        CGPoint cur_pos;
        cur_pos.x = 475.0*2.1333333;
        cur_pos.y = sum1*2.4;  
        bush4Sprite_R.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(475.0*2.1333333, 0), CGPointMake(475.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(475.0*2.1333333, 148.0*2.4));//b
        bush4Sprite_R.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showRoadSign:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_RoadSign[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    roadSignSprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        roadSignSprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        roadSignSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0*2.1333333, 0), CGPointMake(405.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405.0*2.1333333, 148.0*2.4));//b
        roadSignSprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}

@end
