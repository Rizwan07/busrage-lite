//
//  CS_GameLayer_ipad.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CCLayer.h"

#import "MV_HousesLayer_ipad.h"
#import "MV_FenceLayer_ipad.h"
#import "MV_RoadLayer_ipad.h"
#import "MV_TreesLayer_ipad.h"
#import "MV_HillsLayer_ipad.h"
#import "MV_Horizon1Layer_ipad.h"
#import "MV_BaseLayer_ipad.h"

@interface MV_GameLayer_ipad : CCLayer {
    MV_HousesLayer_ipad *mv_houseLayer;
    MV_FenceLayer_ipad *mv_fenceLayer;
    MV_RoadLayer_ipad *mv_roadLayer;
    MV_TreesLayer_ipad *mv_treeLayer;
    MV_HillsLayer_ipad *mv_hillLayer;
    MV_Horizon1Layer_ipad *mv_horizon1Layer;
    MV_BaseLayer_ipad *mv_baseLayer;
    
    CCSprite *cloud_dustSprite;
    int m_timecount;
}
- (void) runMove:(int)runDistance speed:(int)speed;




@end
