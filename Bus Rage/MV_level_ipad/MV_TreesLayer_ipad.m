//
//  CS_TreesLayer_ipad.m
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "MV_TreesLayer_ipad.h"

#import "EnumConstances.h"

#define SHOW_LIMIT 300

@implementation MV_TreesLayer_ipad
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        m_Objects = [NSArray arrayWithObjects://MV:0,1,3,4,5,6
                     
                     @"0, 80",//tree1
                     @"5, 100",//rock1                      
                     @"6, 140",//rock2
                     @"3, 170",//bush1
                     @"1, 200",//tree2  
                     @"4, 250",//bush2
                     
                     @"0, 380",//tree1
                     @"5, 400",//rock1                       
                     @"6, 440",//rock2
                     @"3, 470",//bush1
                     @"1, 500",//tree2  
                     @"4, 550",//bush2                     
                     
                     @"0, 680",//tree1 
                     @"5, 700",//rock1                     
                     @"6, 740",//rock2
                     @"3, 770",//bush1                       
                     @"4, 850",//bush2
                     @"1, 1000",//tree2                    
                     nil];
        int tree1_index = 0;
        int tree2_index = 0;
        int bush1_index = 0;
        int bush2_index = 0;
        int rock1_index = 0;
        int rock2_index = 0;
        for (int i = 0; i < [m_Objects count]; i++)
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case T_OINFO_TREE1:                
                    m_Tree1[tree1_index] = [[params objectAtIndex:1] intValue];
                    tree1_index++;                
                    break;	
                case T_OINFO_TREE2:                
                    m_Tree2[tree2_index] = [[params objectAtIndex:1] intValue];
                    tree2_index++;                
                    break;   
                case T_OINFO_BUSH1:
                    m_Bush1[bush1_index] = [[params objectAtIndex:1] intValue];
                    bush1_index++;
                    break;
                case T_OINFO_BUSH2:
                    m_Bush2[bush2_index] = [[params objectAtIndex:1] intValue];
                    bush2_index++;
                    break;
                case T_OINFO_ROCK1:
                    m_Rock1[rock1_index] = [[params objectAtIndex:1] intValue];
                    rock1_index++;
                    break;
                case T_OINFO_ROCK2:
                    m_Rock2[rock2_index] = [[params objectAtIndex:1] intValue];
                    rock2_index++;
                    break;
            }
        }        
        tree1Sprite = [CCSprite spriteWithFile:@"tree1_MV_ipad.png"];
        [self addChild:tree1Sprite z:2];        
        tree1Sprite.visible = NO;
        
        tree2Sprite = [CCSprite spriteWithFile:@"tree2_MV_ipad.png"];
        [self addChild:tree2Sprite z:2];        
        tree2Sprite.visible = NO;
        
        bush1Sprite = [CCSprite spriteWithFile:@"Bush_1_MV_ipad.png"];
        [self addChild:bush1Sprite z:1];
        bush1Sprite.visible = NO;
        
        bush2Sprite = [CCSprite spriteWithFile:@"Bush_2_MV_ipad.png"];
        [self addChild:bush2Sprite z:1];
        bush2Sprite.visible = NO;
        
        rock1Sprite = [CCSprite spriteWithFile:@"Rock_1_MV_ipad.png"];
        [self addChild:rock1Sprite z:0];
        rock1Sprite.visible = NO;
        
        rock2Sprite = [CCSprite spriteWithFile:@"Rock_2_MV_ipad.png"];
        [self addChild:rock2Sprite z:0];
        rock2Sprite.visible = NO;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance
{
    [self showTree1:runDistance];
    [self showTree2:runDistance];
    [self showBush1:runDistance];
    [self showBush2:runDistance];
    [self showRock1:runDistance];
    [self showRock2:runDistance];    
}
- (void) showTree1:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Tree1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    tree1Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        tree1Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = tree1Sprite.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        tree1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a 
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        tree1Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        tree1Sprite.scale = tree1Sprite.scale * 4;
    }
}
- (void) showTree2:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Tree2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    tree2Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        tree2Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = tree2Sprite.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        tree2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        tree2Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        tree2Sprite.scale = tree2Sprite.scale * 4;
    }
}
- (void) showBush1:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush1Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush1Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.897 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;// = bush1Sprite.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        bush1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        bush1Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        bush1Sprite.scale = bush1Sprite.scale * 3;
    }
}
- (void) showBush2:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    bush2Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        bush2Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.897 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;// = bush1Sprite.position;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        bush2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        bush2Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        bush2Sprite.scale = bush2Sprite.scale * 3;
    }
}
- (void) showRock1:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Rock1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    rock1Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        rock1Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.897 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        rock1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        rock1Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        rock1Sprite.scale = rock1Sprite.scale * 2.0;
    }
}
- (void) showRock2:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Rock2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    rock2Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        rock2Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.897 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        rock2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        rock2Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        rock2Sprite.scale = rock2Sprite.scale * 4.0;
    }
}

@end
