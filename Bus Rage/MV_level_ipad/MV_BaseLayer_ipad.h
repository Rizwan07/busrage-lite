//
//  CS_BaseLayer_ipad.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CCLayer.h"

@interface MV_BaseLayer_ipad : CCLayer {
    CCSprite* m_groundSprite;
}
- (void) runMove:(int)runDistance speed:(int)speed;

@end
