//
//  CS_HillsLayer_ipad.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CCLayer.h"

@interface MV_HillsLayer_ipad : CCLayer {
    CCSprite *hill1Sprite;
    CCSprite *hill2Sprite;
    CCSprite *hill3Sprite;
}
- (void) runMove:(int)runDistance speed:(int)speed;

@end
