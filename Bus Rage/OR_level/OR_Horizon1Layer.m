//
//  OR_Horizon1Layer.m
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import "OR_Horizon1Layer.h"


@implementation OR_Horizon1Layer
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        horizonSprite = [CCSprite spriteWithFile:@"Horizon_1_OR.png"];
        [self addChild:horizonSprite z:0];
        horizonSprite.visible = YES;
        horizonSprite.position = ccp(447, 160);//447...33 
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    if (runDistance == 0 || horizonSprite.position.x < 33.0) {
        horizonSprite.position = ccp(447, 160);
    }
    if (speed > 0) {
        CGPoint pos;
        pos = horizonSprite.position;
        pos.x -= 0.5 * speed/2;
        horizonSprite.position = pos; 
    }    
}
@end
