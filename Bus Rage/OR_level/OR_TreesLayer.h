//
//  OR_TreesLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface OR_TreesLayer : CCLayer {
    CCSprite *cactus1Sprite;
    CCSprite *cactus2Sprite;
    CCSprite *bush1Sprite;
    CCSprite *bush2Sprite;
    
    int m_Cactus1[3];
    int m_Cactus2[3];
    int m_Bush1[3];
    int m_Bush2[3];
    
    NSArray*    m_Objects;
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) showCactus1:(int)runDistance;
- (void) showCactus2:(int)runDistance;
- (void) showBush1:(int)runDistance;
- (void) showBush2:(int)runDistance;
@end
