//
//  SelectCharacterViewController.m
//  Bus Rage
//
//  Created by Jin Tie on 5/9/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "SelectCharacterViewController.h"
#import "GameManager.h"

@implementation SelectCharacterViewController
@synthesize titleLabel;
@synthesize maleDriverLabel;
@synthesize enterNameField;
@synthesize driverImageView;
@synthesize maleThumbnailButton, femaleThumbnailButton;

@synthesize selectlevelViewController = _selectlevelViewController;
@synthesize nextButton;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization      
        [GameManager sharedGameManager].m_bMale = YES;
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
#pragma mark
#pragma Private Methods

- (void)_addKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(keyboardWillAnimate:) 
                                                 name:UIKeyboardWillShowNotification 
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAnimate:) 
                                                 name:UIKeyboardWillHideNotification 
                                               object:nil];
}


- (void)_removeKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //enterNameField.hidden = YES;
    [self performSelector:@selector(_addKeyboardNotification)];
    [GameManager sharedGameManager].m_DriverName = @"";
    //nextButton.enabled = NO;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [self performSelector:@selector(_removeKeyboardNotification)];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backTapped:(id)sender {
    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Next_screen_swoosh.mp3"];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextTapped:(id)sender {
    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Next_screen_swoosh.mp3"];
    }
    if (_selectlevelViewController == nil) {
        //NSLog(@"SelectLevelViewController alloc");
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            self.selectlevelViewController = [[[SelectLevelViewController alloc] initWithNibName:@"SelectLevelViewController" bundle:nil] autorelease];
        }
        else {
            self.selectlevelViewController = [[[SelectLevelViewController alloc] initWithNibName:@"SelectLevelViewController_iPad" bundle:nil] autorelease];
        }
    }
    if ([enterNameField.text isEqualToString:@""]) {
        UIAlertView* alert = [[UIAlertView alloc] init];	
        [alert setMessage:@"Please put Driver's name to go next screen"];
        [alert addButtonWithTitle:@"Ok"];
        [alert setDelegate:self];
        [alert show];
        [alert release];
    } else {
        [self.navigationController pushViewController:_selectlevelViewController animated:YES];
    }
}

- (IBAction)maleTapped:(id)sender {
    maleDriverLabel.text = @"Male Driver";
    
    driverImageView.image = [UIImage imageNamed:@"bus driver_male_correction_render.png"];
    
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//        driverImageView.image = [UIImage imageNamed:@"bus driver_male_correction_render.png"];
//    }
//    else {
//        driverImageView.image = [UIImage imageNamed:@"bus driver_male_correction_render_ipad.png"];
//    }
    [maleThumbnailButton setBackgroundImage:[UIImage imageNamed:@"thumbnail_cur_select_BG.png"] forState:UIControlStateNormal];
    [femaleThumbnailButton setBackgroundImage:[UIImage imageNamed:@"thumbnail_unselect_BG.png"] forState:UIControlStateNormal];
    [GameManager sharedGameManager].m_bMale = YES;
}

- (IBAction)femaleTapped:(id)sender {
    maleDriverLabel.text = @"Female Driver";
    
    driverImageView.image = [UIImage imageNamed:@"bus driver_female_render.png"];
    
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//        driverImageView.image = [UIImage imageNamed:@"bus driver_female_render.png"];
//    }
//    else {
//        driverImageView.image = [UIImage imageNamed:@"bus driver_female_render_ipad.png"];
//    }
    
    [maleThumbnailButton setBackgroundImage:[UIImage imageNamed:@"thumbnail_unselect_BG.png"] forState:UIControlStateNormal];
    [femaleThumbnailButton setBackgroundImage:[UIImage imageNamed:@"thumbnail_cur_select_BG.png"] forState:UIControlStateNormal];
    [GameManager sharedGameManager].m_bMale = NO;
}

//- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    UITouch *touch;
//    if ([[event allTouches] count] == 1) {
//        touch = [[[event allTouches] allObjects] objectAtIndex:0];
//        CGPoint point = [touch locationInView:self.view];
//        if (CGRectContainsPoint(maleDriverLabel.frame, point)) {
//            maleDriverLabel.hidden = YES;
//            enterNameField.hidden = NO;
//            titleLabel.text = @"Name Driver";
//        }         
//    }
//}

    
#pragma mark
#pragma TextField Delegate
    
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self performSelector:@selector(blankBackgroundTapped:) withObject:nil];
    [[GameManager sharedGameManager].m_targetDic setValue:textField.text forKey:@"user_name"];
    
    NSLog(@"User NAME:%@",[[[GameManager sharedGameManager]m_targetDic]valueForKey:@"user_name"]);
    
    return YES;
}

#pragma mark
#pragma Actions

- (void)blankBackgroundTapped:(UIControl *)sender
{
    [enterNameField resignFirstResponder];
    if ([enterNameField.text length] > 140) {
        enterNameField.text = @"";
        NSLog(@"more text");
        return;
    }
    [GameManager sharedGameManager].m_DriverName = enterNameField.text;
    //nextButton.enabled = YES;
}
- (void)keyboardWillAnimate:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    if([notification name] == UIKeyboardWillShowNotification)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - keyboardBounds.size.height, self.view.frame.size.width, self.view.frame.size.height)];        
    }
    else if([notification name] == UIKeyboardWillHideNotification)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + keyboardBounds.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    }
    [UIView commitAnimations];
}
@end
