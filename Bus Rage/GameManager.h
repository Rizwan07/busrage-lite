//
//  GameManager.h
//  Electric Bird
//
//  Created by Steve Jobs on 12-2-27.
//  Copyright (c) 2012年 Apple inc. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

typedef enum apiCall {
    kAPILogout,
    kAPIGraphUserPermissionsDelete,
    kDialogPermissionsExtended,
    kDialogRequestsSendToMany,
    kAPIGetAppUsersFriendsNotUsing,
    kAPIGetAppUsersFriendsUsing,
    kAPIFriendsForDialogRequests,
    kDialogRequestsSendToSelect,
    kAPIFriendsForTargetDialogRequests,
    kDialogRequestsSendToTarget,
    kDialogFeedUser,
    kAPIFriendsForDialogFeed,
    kDialogFeedFriend,
    kAPIGraphUserPermissions,
    kAPIGraphMe,
    kAPIGraphUserFriends,
    kDialogPermissionsCheckin,
    kDialogPermissionsCheckinForRecent,
    kDialogPermissionsCheckinForPlaces,
    kAPIGraphSearchPlace,
    kAPIGraphUserCheckins,
    kAPIGraphUserPhotosPost,
    kAPIGraphUserVideosPost,
} apiCall;

@interface GameManager : NSObject
{
    BOOL m_bEnabledMusic;
    BOOL m_bEnabledSound;    
    BOOL m_bPlayMusic;
    
    //SelectCharacter
    BOOL m_bMale; 
    NSString *m_DriverName;
    
    //SelectLevel
    int m_levelNum;
    
    //SelectTarget
    NSMutableArray *m_TargetArray;//bool
    NSMutableArray *m_TargetNameArray;//name text
    int targetNum1;
    int targetNum2;
    int targetNum3;
    int targetNum4;
    int targetNum5;
    int targetNum6;
    int targetNum7;
    UIImage *fbTargetImage1;
    UIImage *fbTargetImage2;
    UIImage *fbTargetImage3;
    UIImage *fbTargetImage4;
    UIImage *fbTargetImage5;
    UIImage *fbTargetImage6;
    UIImage *fbTargetImage7;
    
    NSMutableArray *m_TargetfacebookImageObjectIDArray;//id_list
    NSMutableArray *m_TargetfacebookImageArray;
    int m_targetNum;
        
    BOOL bNetWork_Con;
    
    int m_GameScore;
    int m_target1HitNum, m_target2HitNum, m_target3HitNum;
    int m_target4HitNum;//, m_target5HitNum, m_target6HitNum, m_target7HitNum;
    int m_loopNum;
    int m_currentLevelTargetNum;
    
    int m_currentPassedLevel;
    
    NSString *challengeId;
    NSMutableDictionary *m_targetDic;
}

@property (assign) BOOL m_bEnabledMusic;
@property (assign) BOOL m_bEnabledSound;
@property (assign) BOOL m_bPlayMusic;

@property (assign) BOOL m_bMale;
@property (nonatomic, retain) NSString *m_DriverName;
@property (assign) int m_levelNum;

@property (nonatomic, retain) NSMutableArray *m_TargetArray;
@property (nonatomic, retain) NSMutableArray *m_TargetNameArray;
@property (assign) int targetNum1;
@property (assign) int targetNum2;
@property (assign) int targetNum3;
@property (assign) int targetNum4;
//@property (assign) int targetNum5;
//@property (assign) int targetNum6;
//@property (assign) int targetNum7;
@property (nonatomic, retain) UIImage *fbTargetImage1;
@property (nonatomic, retain) UIImage *fbTargetImage2;
@property (nonatomic, retain) UIImage *fbTargetImage3;
@property (nonatomic, retain) UIImage *fbTargetImage4;
//@property (nonatomic, retain) UIImage *fbTargetImage5;
//@property (nonatomic, retain) UIImage *fbTargetImage6;
//@property (nonatomic, retain) UIImage *fbTargetImage7;
@property (nonatomic, retain) NSMutableArray *m_TargetfacebookImageObjectIDArray;
@property (nonatomic, retain) NSMutableArray *m_TargetfacebookImageArray;
@property (assign) int m_targetNum;
@property (assign) int m_loopNum;
@property (assign) BOOL isSelectAll;

@property (assign) int m_target1HitNum, m_target2HitNum, m_target3HitNum, m_target4HitNum;//, m_target5HitNum, m_target6HitNum, m_target7HitNum;

@property (assign) BOOL bNetWork_Con;
@property (assign) int m_GameScore;
@property (assign) int m_currentLevelTargetNum;

@property (assign) int m_currentPassedLevel;

@property (nonatomic, retain) NSString *challengeId;

@property (nonatomic, retain) NSMutableDictionary *m_targetDic;

+ (GameManager*) sharedGameManager;
+ (id) alloc;

- (NSString *)createEditableCopyOfFileIfNeeded:(NSString *)_filename;
- (void)saveCompleteLevel;
- (void)loadCompleteLevel;
@end
