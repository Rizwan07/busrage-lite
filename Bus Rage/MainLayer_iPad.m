//
//  MainLayer_iPad.m
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "MainLayer_iPad.h"
#import <mach/mach.h>
#import "CRock.h"
#import "SV4Obstacle.h"

#define SHOW_LIMIT 300

@implementation MainLayer_iPad
- (void) print_free_memory 
{//kgh
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);
    
    vm_statistics_data_t vm_stat;
    
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
        NSLog(@"Failed to fetch vm statistics");
    
    /* Stats in bytes */
    natural_t mem_used = (vm_stat.active_count + vm_stat.inactive_count + vm_stat.wire_count) * pagesize;
    natural_t mem_free = vm_stat.free_count * pagesize;
    natural_t mem_total = mem_used + mem_free;
    int iUsed = round(mem_used/100000);
    int iFree = round(mem_free/100000);
    int iTotal = round(mem_total/100000);
    NSLog(@"used: %d free: %d total: %d", iUsed, iFree, iTotal);    
}
+(CCScene *) scene:(RootViewController *)rootViewController 
{
    CCScene *scene = [CCScene node];
    MainLayer_iPad *layer = [MainLayer_iPad node];
    [scene addChild: layer];	
    layer->_rootViewController = rootViewController;
    
    return scene;
    
}

- (void) setiPadCoordinates {
    iPadX = 1024.0/480.0;
    iPadY = 768.0/320.0;
}

// on "init" you need to initialize your instance
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super initWithColor:ccc4(255, 255, 255, 255)])) {		
        
        [self setiPadCoordinates];
        
        srand(time(NULL) % 257);
        m_levelNum = [GameManager sharedGameManager].m_levelNum;
        m_runDistance = 0;
        m_bStart = NO;
        
		// ask director the the window size
		CGSize size = [[CCDirector sharedDirector] winSize];        
        
        m_ReadySprite1 = [CCSprite spriteWithFile:@"Get Ready.png"];
        m_ReadySprite2 = [CCSprite spriteWithFile:@"Get Set.png"];
        m_ReadySprite3 = [CCSprite spriteWithFile:@"Bus Rage.png"];
        m_ReadySprite1.position = m_ReadySprite2.position = m_ReadySprite3.position = ccp(240*iPadX, 160*iPadY);
        m_ReadySprite1.visible = m_ReadySprite2.visible = m_ReadySprite3.visible = NO;
        [self addChild:m_ReadySprite1 z:10];
        [self addChild:m_ReadySprite2 z:10];
        [self addChild:m_ReadySprite3 z:10];        
        
        CCLabelTTF *lbl = [CCLabelTTF labelWithString:@"You missed the target in current level. Now play again" fontName:@"Marker Felt" fontSize:25];
        CCMenuItem* missedTarget = [CCMenuItemLabel itemWithLabel:lbl];
        m_menuMissedTarget = [CCMenu menuWithItems:missedTarget, nil];
        m_menuMissedTarget.visible = NO;
        m_menuMissedTarget.position = ccp(240*iPadX, 220*iPadY);
        m_menuMissedTarget.color = ccc3(0,0,0);
        [self addChild:m_menuMissedTarget z:130];
        
        CCMenuItem* btnGameOver = [CCMenuItemImage itemFromNormalImage:@"Restart.png" selectedImage:nil target:self selector:@selector(btnGameOverClick)];        
        m_menuGameOver = [CCMenu menuWithItems:btnGameOver, nil];
        m_menuGameOver.position = ccp(240*iPadX, 160*iPadY);
        [self addChild:m_menuGameOver z:130];
        
        m_ShowMessageTargetDead = [CCSprite spriteWithFile:@"target-is-dead.png"];
        m_ShowMessageTargetInternalOrganDamage = [CCSprite spriteWithFile:@"internal-organ-damage.png"];
        m_ShowMessageTargetArmBroken = [CCSprite spriteWithFile:@"arm-broken.png"];
        m_ShowMessageTargetLegBroken = [CCSprite spriteWithFile:@"leg-broken.png"];
        m_ShowMessageTargetBackBroken = [CCSprite spriteWithFile:@"back-broken.png"];
        m_ShowMessageTargetComa = [CCSprite spriteWithFile:@"in-a-coma.png"];
        m_ShowMessageTargetDead.position = m_ShowMessageTargetInternalOrganDamage.position = m_ShowMessageTargetArmBroken.position = m_ShowMessageTargetLegBroken.position = m_ShowMessageTargetBackBroken.position = m_ShowMessageTargetComa.position = ccp(240*iPadX, 160*iPadY);
        m_ShowMessageTargetDead.visible = m_ShowMessageTargetInternalOrganDamage.visible = m_ShowMessageTargetArmBroken.visible = m_ShowMessageTargetLegBroken.visible = m_ShowMessageTargetBackBroken.visible = m_ShowMessageTargetComa.visible = NO;
        m_ShowStatus = 0;
        [self addChild:m_ShowMessageTargetDead z:131];
        [self addChild:m_ShowMessageTargetInternalOrganDamage z:131];
        [self addChild:m_ShowMessageTargetArmBroken z:131];
        [self addChild:m_ShowMessageTargetLegBroken z:131];
        [self addChild:m_ShowMessageTargetBackBroken z:131];
        [self addChild:m_ShowMessageTargetComa z:131];
        
        CCLabelTTF* labelYes = [CCLabelTTF labelWithString:@"YES" fontName:@"Marker Felt" fontSize:45];
        CCMenuItem* btnSuccessFaceBook = [CCMenuItemLabel itemWithLabel:labelYes target:self selector:@selector(yesFaceBookClick)];
        btnSuccessFaceBook.position = ccp(342, 216+100);
        
        CCLabelTTF* labelNo = [CCLabelTTF labelWithString:@"NO" fontName:@"Marker Felt" fontSize:45];
        CCMenuItem* btnSuccessNoFaceBook = [CCMenuItemLabel itemWithLabel:labelNo target:self selector:@selector(noFaceBookClick)];  
        btnSuccessNoFaceBook.position = ccp(682, 216+100);
        
        CCLabelTTF* labelMessage1 = [CCLabelTTF labelWithString:@"Congratulations!" fontName:@"Marker Felt" fontSize:40];
        CCMenuItem  *messageLabel1 = [CCMenuItemLabel itemWithLabel:labelMessage1];
        messageLabel1.position = ccp(512, 480);
        //
        CCLabelTTF* labelMessage2 = [CCLabelTTF labelWithString:@"Do you want to post to Facebook?" fontName:@"Marker Felt" fontSize:30];     
        CCMenuItem  *messageLabel2 = [CCMenuItemLabel itemWithLabel:labelMessage2];
        messageLabel2.position = ccp(512, 384+30);
        
        m_menuSuccess = [CCMenu menuWithItems:btnSuccessFaceBook, btnSuccessNoFaceBook, messageLabel1, messageLabel2, nil];
        labelMessage1.color = labelMessage2.color = labelYes.color = labelNo.color = ccc3(0,0,0);
        
        m_menuSuccess.position = ccp(0, 0);
        //[self addChild:m_menuSuccess];
        [self addChild:m_menuSuccess z:130];
        m_menuGameOver.visible = m_menuSuccess.visible = NO;
        
        self.isAccelerometerEnabled = YES;
        [[UIAccelerometer sharedAccelerometer] setUpdateInterval:1/60];
        [self setBackgroud];
        //NSLog(@"setBackgroud_end");
        m_BusSpeed = 0; // 2m/s ... 10m/s
        m_BusSpeedFloat = 0.0f;
        
        m_bSteering = NO;      
        m_steering_angle = 0;
        
        
        CCSprite* score_panelSprite = [CCSprite spriteWithFile:@"Score_panel_ipad.png"];
        score_panelSprite.position = ccp(size.width/2, size.height/2);
        [self addChild:score_panelSprite z:10];
        //[self addChild:score_panelSprite z:100];
        
        m_labelSpeed = [CCLabelTTF labelWithString:@"0 mile/h" fontName:@"Arial" fontSize:22];     
        [m_labelSpeed setColor:ccc3(0,0,0)];
        m_labelSpeed.position = ccp(400*iPadX, 310*iPadY);
        m_labelScore = [CCLabelTTF labelWithString:@"SCORE:0" fontName:@"Arial" fontSize:22];
        [m_labelScore setColor:ccc3(0,0,0)];
        m_labelScore.position = ccp(400*iPadX, 290*iPadY);
        m_labelTime = [CCLabelTTF labelWithString:@"TIME:90" fontName:@"Arial" fontSize:22];
        [m_labelTime setColor:ccc3(0,0,0)];
        m_labelTime.position = ccp(330*iPadX, 270*iPadY);
        m_labelMiss = [CCLabelTTF labelWithString:@"T_MISS:0" fontName:@"Arial" fontSize:22];
        [m_labelMiss setColor:ccc3(0,0,0)];
        m_labelMiss.position = ccp(400*iPadX, 270*iPadY);        
        [score_panelSprite addChild:m_labelSpeed z:0];
        [score_panelSprite addChild:m_labelScore z:0];
        [score_panelSprite addChild:m_labelTime z:0];
        [score_panelSprite addChild:m_labelMiss z:0];
        m_labelMiss.visible = NO;
        [GameManager sharedGameManager].m_GameScore = 0;
        
        //m_Bus_angle = m_Bus_angle_old = m_Bus_Move = 0.0; 
        m_Bus_angle_Old_Right = YES;
        //        busSprite.rotation = m_Bus_angle;//14...-10
        
        m_Bus = [[CBus alloc] init];
        [self addChild:m_Bus z:tagBus];
        m_Bus.position = ccp(302*iPadX, 50*iPadY);//m_Bus.position = ccp(638, 320+79);
        
        //target and passenger distance setting
        [self setTargetPassengerDistance];
        
        //m_CollideCountLeft = m_CollideCountRight = 0;
        m_bCollideLeft = m_bCollideRight = NO;
        //m_bBusFlip = NO;
        m_loopNum = [GameManager sharedGameManager].m_loopNum = 0;
        
        m_BusHitAreaSprite = [CCSprite spriteWithFile:@"bus_hit_ipad.png"];
        m_BusHitAreaSprite.position = ccp(302*iPadX + 47*iPadX, 50*iPadY + 14*iPadY);
        //m_BusHitAreaSprite.position = ccp(302+47, 50+14);
        [self addChild:m_BusHitAreaSprite z:tagBus + 1];
        m_BusHitAreaSprite.visible = NO;
        
        _rootViewController.m_bBusTargetHit = NO;
        [GameManager sharedGameManager].m_target1HitNum = [GameManager sharedGameManager].m_target2HitNum = [GameManager sharedGameManager].m_target3HitNum = [GameManager sharedGameManager].m_target4HitNum = 0;//[GameManager sharedGameManager].m_target5HitNum = [GameManager sharedGameManager].m_target6HitNum = 0;
        
        //target's label or image add--------------------------------------------------------------------------
        
        NSString *tmpString;
        CTarget *target;
        NSString *objectId;
        UIImage *tmpUIImage;
        int levelNum = [GameManager sharedGameManager].m_levelNum;
        //t1               
        
        tmpString = [[GameManager sharedGameManager].m_TargetNameArray objectAtIndex:[GameManager sharedGameManager].targetNum1 - 1];        
        m_target1Label = [CCLabelTTF labelWithString:tmpString fontName:@"Arial" fontSize:12];     
        [m_target1Label setColor:ccc3(0,0,0)];
        m_target1Label.position = ccp(0, 50);
        if ([[GameManager sharedGameManager] isSelectAll]) {
            target = [m_targetArrChooseForAll objectAtIndex:0];
        }
        else {
            target = [m_targetArray objectAtIndex:[GameManager sharedGameManager].targetNum1 - 1];
        }
        objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum1 - 1];
        
        CCSprite *indexTSprite = [CCSprite spriteWithFile:@"Target_icon.png"]; 
        indexTSprite.position = ccp(0, 70);
        [target addChild:indexTSprite z:1]; 
        
        if (![objectId isEqualToString:@""]) {
            tmpUIImage = [GameManager sharedGameManager].fbTargetImage1;
            CCSprite *m_target1Sprite = [CCSprite spriteWithCGImage:tmpUIImage.CGImage key:@"t_spriteKey1"];
            m_target1Sprite.position = CGPointMake(0, 54);
            m_target1Sprite.scale = 0.5;
            [target addChild:m_target1Sprite z:1];
        } else {
            [target addChild:m_target1Label z:1];
        }   
        
        //t2
//        if (levelNum < 7) {
        
            tmpString = [[GameManager sharedGameManager].m_TargetNameArray objectAtIndex:[GameManager sharedGameManager].targetNum2 - 1];        
            m_target2Label = [CCLabelTTF labelWithString:tmpString fontName:@"Arial" fontSize:12];     
            [m_target2Label setColor:ccc3(0,0,0)];
            m_target2Label.position = ccp(0, 50);
        if ([[GameManager sharedGameManager] isSelectAll]) {
            target = [m_targetArrChooseForAll objectAtIndex:1];
        }
        else {
            target = [m_targetArray objectAtIndex:[GameManager sharedGameManager].targetNum2 - 1];
        }
            objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum2 - 1];
            
            indexTSprite = [CCSprite spriteWithFile:@"Target_icon.png"];
            indexTSprite.position = ccp(0, 70);
            [target addChild:indexTSprite z:1];
            
            if (![objectId isEqualToString:@""]) {
                tmpUIImage = [GameManager sharedGameManager].fbTargetImage2;
                CCSprite *m_target2Sprite = [CCSprite spriteWithCGImage:tmpUIImage.CGImage key:@"t_spriteKey2"];
                m_target2Sprite.position = CGPointMake(0, 54);
                m_target2Sprite.scale = 0.5;
                [target addChild:m_target2Sprite z:1];
            } else {
                [target addChild:m_target2Label z:1];
            }             
//        }
        //t3
        if (levelNum == 3) {
            
            tmpString = [[GameManager sharedGameManager].m_TargetNameArray objectAtIndex:[GameManager sharedGameManager].targetNum3 - 1];        
            m_target3Label = [CCLabelTTF labelWithString:tmpString fontName:@"Arial" fontSize:12];     
            [m_target3Label setColor:ccc3(0,0,0)];
            m_target3Label.position = ccp(0, 50);
            if ([[GameManager sharedGameManager] isSelectAll]) {
                target = [m_targetArrChooseForAll objectAtIndex:2];
            }
            else {
                target = [m_targetArray objectAtIndex:[GameManager sharedGameManager].targetNum3 - 1];
            }
            objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum3 - 1];
            
            CCSprite *indexTSprite = [CCSprite spriteWithFile:@"Target_icon.png"]; 
            indexTSprite.position = ccp(0, 70);
            [target addChild:indexTSprite z:1]; 
            
            if (![objectId isEqualToString:@""]) {
                tmpUIImage = [GameManager sharedGameManager].fbTargetImage3;
                CCSprite *m_target3Sprite = [CCSprite spriteWithCGImage:tmpUIImage.CGImage key:@"t_spriteKey3"];
                m_target3Sprite.position = CGPointMake(0, 54);
                m_target3Sprite.scale = 0.5;
                [target addChild:m_target3Sprite z:1];
            } else {
                [target addChild:m_target3Label z:1];
            }            
        }
        if (levelNum == 6 || levelNum == 7) {
            
            tmpString = [[GameManager sharedGameManager].m_TargetNameArray objectAtIndex:[GameManager sharedGameManager].targetNum3 - 1];
            m_target3Label = [CCLabelTTF labelWithString:tmpString fontName:@"Arial" fontSize:12];
            [m_target3Label setColor:ccc3(0,0,0)];
            m_target3Label.position = ccp(0, 50);
            if ([[GameManager sharedGameManager] isSelectAll]) {
                target = [m_targetArrChooseForAll objectAtIndex:2];
            }
            else {
                target = [m_targetArray objectAtIndex:[GameManager sharedGameManager].targetNum3 - 1];
            }
            objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum3 - 1];
            
            CCSprite *indexTSprite = [CCSprite spriteWithFile:@"Target_icon.png"];
            indexTSprite.position = ccp(0, 70);
            [target addChild:indexTSprite z:1];
            
            if (![objectId isEqualToString:@""]) {
                tmpUIImage = [GameManager sharedGameManager].fbTargetImage3;
                CCSprite *m_target3Sprite = [CCSprite spriteWithCGImage:tmpUIImage.CGImage key:@"t_spriteKey3"];
                m_target3Sprite.position = CGPointMake(0, 54);
                m_target3Sprite.scale = 0.5;
                [target addChild:m_target3Sprite z:1];
            } else {
                [target addChild:m_target3Label z:1];
            }
            
            tmpString = [[GameManager sharedGameManager].m_TargetNameArray objectAtIndex:[GameManager sharedGameManager].targetNum4 - 1];
            m_target4Label = [CCLabelTTF labelWithString:tmpString fontName:@"Arial" fontSize:12];
            [m_target4Label setColor:ccc3(0,0,0)];
            m_target4Label.position = ccp(0, 50);
            if ([[GameManager sharedGameManager] isSelectAll]) {
                target = [m_targetArrChooseForAll objectAtIndex:3];
            }
            else {
                target = [m_targetArray objectAtIndex:[GameManager sharedGameManager].targetNum4 - 1];
            }
            objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum4 - 1];
            
            CCSprite *indexT4Sprite = [CCSprite spriteWithFile:@"Target_icon.png"];
            indexT4Sprite.position = ccp(0, 70);
            [target addChild:indexT4Sprite z:1];
            
            if (![objectId isEqualToString:@""]) {
                tmpUIImage = [GameManager sharedGameManager].fbTargetImage4;
                CCSprite *m_target4Sprite = [CCSprite spriteWithCGImage:tmpUIImage.CGImage key:@"t_spriteKey4"];
                m_target4Sprite.position = CGPointMake(0, 54);
                m_target4Sprite.scale = 0.5;
                [target addChild:m_target4Sprite z:1];
            } else {
                [target addChild:m_target4Label z:1];
            }
        }
//        if (levelNum == 7) {
//            
//            tmpString = [[GameManager sharedGameManager].m_TargetNameArray objectAtIndex:[GameManager sharedGameManager].targetNum5 - 1];
//            m_target5Label = [CCLabelTTF labelWithString:tmpString fontName:@"Arial" fontSize:12];
//            [m_target5Label setColor:ccc3(0,0,0)];
//            m_target5Label.position = ccp(0, 50);
//            target = [m_targetArray objectAtIndex:[GameManager sharedGameManager].targetNum5 - 1];
//            objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum5 - 1];
//            
//            CCSprite *indexTSprite = [CCSprite spriteWithFile:@"Target_icon.png"];
//            indexTSprite.position = ccp(0, 70);
//            [target addChild:indexTSprite z:1];
//            
//            if (![objectId isEqualToString:@""]) {
//                tmpUIImage = [GameManager sharedGameManager].fbTargetImage5;
//                CCSprite *m_target5Sprite = [CCSprite spriteWithCGImage:tmpUIImage.CGImage key:@"t_spriteKey5"];
//                m_target5Sprite.position = CGPointMake(0, 54);
//                m_target5Sprite.scale = 0.5;
//                [target addChild:m_target5Sprite z:1];
//            } else {
//                [target addChild:m_target5Label z:1];
//            }
//            
//            tmpString = [[GameManager sharedGameManager].m_TargetNameArray objectAtIndex:[GameManager sharedGameManager].targetNum6 - 1];
//            m_target6Label = [CCLabelTTF labelWithString:tmpString fontName:@"Arial" fontSize:12];
//            [m_target6Label setColor:ccc3(0,0,0)];
//            m_target6Label.position = ccp(0, 50);
//            target = [m_targetArray objectAtIndex:[GameManager sharedGameManager].targetNum6 - 1];
//            objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum6 - 1];
//            
//            CCSprite *indexT6Sprite = [CCSprite spriteWithFile:@"Target_icon.png"];
//            indexT6Sprite.position = ccp(0, 70);
//            [target addChild:indexT6Sprite z:1];
//            
//            if (![objectId isEqualToString:@""]) {
//                tmpUIImage = [GameManager sharedGameManager].fbTargetImage4;
//                CCSprite *m_target6Sprite = [CCSprite spriteWithCGImage:tmpUIImage.CGImage key:@"t_spriteKey6"];
//                m_target6Sprite.position = CGPointMake(0, 54);
//                m_target6Sprite.scale = 0.5;
//                [target addChild:m_target6Sprite z:1];
//            } else {
//                [target addChild:m_target6Label z:1];
//            }
//            
//        }
        
        m_MissT = 0;
        [self setTime];
        //add obstacles
        //level 2,3
        m_RockArray = [[NSMutableArray alloc] init];
        CRock *rock = [[CRock alloc] init:1 forIpad:YES];
        rock.visible = NO;
        [self addChild:rock z:tagBus-1];
        rock.m_bBusHit = NO;
        rock.m_distance = 800;
        rock.m_road_width = 3.5;
        [m_RockArray addObject:rock];
        [rock release];
        
        rock = [[CRock alloc] init:2 forIpad:YES];
        rock.visible = NO;
        [self addChild:rock z:tagBus-1];
        rock.m_bBusHit = NO;
        rock.m_distance = 1100;
        rock.m_road_width = 6.0;
        [m_RockArray addObject:rock];
        [rock release];
        
        rock = [[CRock alloc] init:1 forIpad:YES];
        rock.visible = NO;
        [self addChild:rock z:tagBus-1];
        rock.m_bBusHit = NO;
        rock.m_distance = 1800;
        rock.m_road_width = 3.5;
        [m_RockArray addObject:rock];
        [rock release];
        
        rock = [[CRock alloc] init:2 forIpad:YES];
        rock.visible = NO;
        [self addChild:rock z:tagBus-1];
        rock.m_bBusHit = NO;
        rock.m_distance = 2600;
        rock.m_road_width = 5.0;
        [m_RockArray addObject:rock];
        [rock release];
        
        //level 4
        m_SV4ObstacleArray = [[NSMutableArray alloc] init];
        SV4Obstacle *sv4Obstacle = [[SV4Obstacle alloc] init:m_levelNum==8?9:1];
        sv4Obstacle.visible = NO;
        [self addChild:sv4Obstacle z:tagBus-1];
        sv4Obstacle.m_bBusHit = NO;
        sv4Obstacle.m_distance = 500;
        sv4Obstacle.m_road_width = 2.0;
        [m_SV4ObstacleArray addObject:sv4Obstacle];
        [sv4Obstacle release];
        
        sv4Obstacle = [[SV4Obstacle alloc] init:m_levelNum==8?8:2];
        sv4Obstacle.visible = NO;
        [self addChild:sv4Obstacle z:tagBus-1];
        sv4Obstacle.m_bBusHit = NO;
        sv4Obstacle.m_distance = 1000;
        sv4Obstacle.m_road_width = 5.6;
        [m_SV4ObstacleArray addObject:sv4Obstacle];
        [sv4Obstacle release];
        
        sv4Obstacle = [[SV4Obstacle alloc] init:m_levelNum==8?9:3];
        sv4Obstacle.visible = NO;
        [self addChild:sv4Obstacle z:tagBus-1];
        sv4Obstacle.m_bBusHit = NO;
        sv4Obstacle.m_distance = 1500;
        sv4Obstacle.m_road_width = 3.0;
        [m_SV4ObstacleArray addObject:sv4Obstacle];
        [sv4Obstacle release];
        
        sv4Obstacle = [[SV4Obstacle alloc] init:m_levelNum==8?8:4];
        sv4Obstacle.visible = NO;
        [self addChild:sv4Obstacle z:tagBus-1];
        sv4Obstacle.m_bBusHit = NO;
        sv4Obstacle.m_distance = 2000;
        sv4Obstacle.m_road_width = 5.6;
        [m_SV4ObstacleArray addObject:sv4Obstacle];//use at level 5
        [sv4Obstacle release];
        
        sv4Obstacle = [[SV4Obstacle alloc] init:m_levelNum==8?9:5];
        sv4Obstacle.visible = NO;
        [self addChild:sv4Obstacle z:tagBus-1];
        sv4Obstacle.m_bBusHit = NO;
        sv4Obstacle.m_distance = 2500;
        sv4Obstacle.m_road_width = 2.5;
        [m_SV4ObstacleArray addObject:sv4Obstacle];  
        [sv4Obstacle release];
        
        sv4Obstacle = [[SV4Obstacle alloc] init:m_levelNum==8?8:6];
        sv4Obstacle.visible = NO;
        [self addChild:sv4Obstacle z:tagBus-1];
        sv4Obstacle.m_bBusHit = NO;
        sv4Obstacle.m_distance = 3000;
        sv4Obstacle.m_road_width = 5.6;
        [m_SV4ObstacleArray addObject:sv4Obstacle]; 
        [sv4Obstacle release];
        
        sv4Obstacle = [[SV4Obstacle alloc] init:m_levelNum==8?9:7];
        sv4Obstacle.visible = NO;
        [self addChild:sv4Obstacle z:tagBus-1];
        sv4Obstacle.m_bBusHit = NO;
        sv4Obstacle.m_distance = 3500;
        sv4Obstacle.m_road_width = 3.5;
        [m_SV4ObstacleArray addObject:sv4Obstacle];      
        [sv4Obstacle release];
        
        m_girlWalk = [[GirlWalking alloc] init];
        m_girlWalk.visible = NO;
        [self addChild:m_girlWalk z:tagBus-1];
        m_girlWalk.m_bBusHit = NO;
        m_girlWalk.m_distance = 4000;
        m_girlWalk.m_road_width = 0.0;
        [m_girlWalk setAnimWalk];
        
        //level 5
        m_scBoy = [[SCBoy alloc] init];
        m_scBoy.visible = NO;
        [self addChild:m_scBoy z:tagBus-1];
        m_scBoy.m_bBusHit = NO;
        m_scBoy.m_distance = 1000;
        m_scBoy.m_road_width = 0.0;
        [m_scBoy setAnimWalk];
        
        m_scGirl = [[SCGirl alloc] init];
        m_scGirl.visible = NO;
        [self addChild:m_scGirl z:tagBus-1];
        m_scGirl.m_bBusHit = NO;
        m_scGirl.m_distance = 1600;
        m_scGirl.m_road_width = 0.0;
        [m_scGirl setAnimWalk];
        
        m_scTeacher = [[SCTeacher alloc] init];
        m_scTeacher.visible = NO;
        [self addChild:m_scTeacher z:tagBus-1];
        m_scTeacher.m_bBusHit = NO;
        m_scTeacher.m_distance = 2600;
        m_scTeacher.m_road_width = 0.0;
        [m_scTeacher setAnimWalk];
        
        m_timecount = 0;
        _rootViewController.m_bStop = NO;
        [self schedule:@selector(updateGamePlay:)];
        
        //[self connectToServer];
	}    
	return self;
}

- (void) replayClick
{
    m_runDistance = 0;
    m_BusSpeed = 0;
    m_BusSpeedFloat = 0.0;
    m_loopNum = 0;
    m_Bus.position = ccp(302*iPadX, 50*iPadY);
    //m_bFlit = NO;
    
}

- (void) crashed
{
    [self bus_break];
    if ([GameManager sharedGameManager].m_currentLevelTargetNum == 0) {//success
        [_rootViewController challenge];
        m_menuSuccess.visible = YES;
        _rootViewController.m_bStop = YES;
        
        if (m_levelNum+1 > [[NSUserDefaults standardUserDefaults] integerForKey:@"unlockLevel"]) {
            
            [[NSUserDefaults standardUserDefaults] setInteger:m_levelNum+1 forKey:@"unlockLevel"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
//        if ([GameManager sharedGameManager].m_currentPassedLevel < 4)
//            [GameManager sharedGameManager].m_currentPassedLevel = m_levelNum;
//        [[GameManager sharedGameManager] saveCompleteLevel];
    }
    else{
        m_menuGameOver.visible = YES;
        _rootViewController.m_bStop = YES;
        if (m_levelNum == 8) {
            //[self videoPlayFunEnd];
        }
    }
    
    
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{	
	// don't forget to call "super dealloc"  
    [m_targetArray removeAllObjects];
    [m_targetArray release];
    [m_targetArrChooseForAll removeAllObjects];
    [m_targetArrChooseForAll release];
    [m_Bus release];
    
    [m_RockArray removeAllObjects];
    [m_RockArray release];
    
    [m_SV4ObstacleArray removeAllObjects];
    [m_SV4ObstacleArray release];
    
    [m_girlWalk release]; m_girlWalk = nil;
    [m_scBoy release]; m_scBoy = nil;
    [m_scGirl release]; m_scGirl = nil;
    [m_scTeacher release]; m_scTeacher = nil;
	[super dealloc];
}
- (void) getStart {
    _rootViewController.m_bStop = NO;
    m_bStart = YES;
    m_ReadySprite3.visible = NO;
}
- (void) getRage {
    m_ReadySprite1.visible = m_ReadySprite2.visible = m_ReadySprite3.visible = NO;
    m_ReadySprite3.visible = YES;
    [self performSelector:@selector(getStart) withObject:self afterDelay:1.5];
}
- (void) getReady {
    m_ReadySprite1.visible = m_ReadySprite2.visible = m_ReadySprite3.visible = NO;
    m_ReadySprite2.visible = YES;
    [self performSelector:@selector(getRage) withObject:self afterDelay:1.5];
}
- (void) readyGame
{
    m_ReadySprite1.visible = m_ReadySprite2.visible = m_ReadySprite3.visible = NO;
    m_ReadySprite1.visible = YES;
    [self performSelector:@selector(getReady) withObject:self afterDelay:1.5];
}
- (void) showEnd
{
    m_ShowMessageTargetDead.visible = m_ShowMessageTargetInternalOrganDamage.visible = m_ShowMessageTargetArmBroken.visible = m_ShowMessageTargetLegBroken.visible = m_ShowMessageTargetBackBroken.visible = m_ShowMessageTargetComa.visible = NO;
    _rootViewController.m_bBusTargetHit = NO;
}
- (void)updateGamePlay:(ccTime)dt
{
    if (_rootViewController.m_bStop)
        return;
    if (_rootViewController.m_bMessageShow) {
        _rootViewController.m_bMessageShow = NO;
        switch (m_ShowStatus) {
            case 1:
                m_ShowMessageTargetDead.visible = YES;
                break;
            case 2:
                m_ShowMessageTargetInternalOrganDamage.visible = YES;
                break;
            case 3:
                m_ShowMessageTargetArmBroken.visible = YES;
                break;
            case 4:
                m_ShowMessageTargetLegBroken.visible = YES;
                break;
            case 5:
                m_ShowMessageTargetBackBroken.visible = YES;
                break;
            case 6:
                m_ShowMessageTargetComa.visible = YES;
                break;
            default:
                break;
        }
        [self performSelector:@selector(showEnd) withObject:self afterDelay:2.0];
    }
    if (_rootViewController.m_bBusTargetHit)
        return;    
    
    if (!m_bStart && m_timecount == 50) {
        _rootViewController.m_bStop = YES;
        //m_bStart = YES;
        [self readyGame];
    }
    
    m_timecount++;
    if (m_timecount > 40000)
        m_timecount = 0;  
    
    if (m_timecount % 3 == 0) {
        m_runDistance += m_BusSpeed/2;
        //[self runMove:m_runDistance speed:m_BusSpeed];

    }    
    
    if (m_timecount % 6 == 0) {
        [self runMove:m_runDistance speed:m_BusSpeed];
    }
    
    if (m_runDistance > 1200) {//if (m_runDistance > 40000)
        m_runDistance = 0;  
        m_loopNum++;
        [GameManager sharedGameManager].m_loopNum = m_loopNum;
    }
    
    
    if (m_timecount % 5 == 0) {         
        if (_rootViewController.m_bAccelerator) {
            if (m_BusSpeedFloat < 10.00)
                m_BusSpeedFloat += 0.2;
        } else {
            if (m_BusSpeedFloat > 0.01)
                m_BusSpeedFloat -= 0.1;
        }
    }       
    
    if (m_timecount % 60 == 0 && m_Time > 0) {
        m_Time--;
        if (m_Time == 0 && m_bTimeClock) {
            m_menuGameOver.visible = YES;
            _rootViewController.m_bStop = YES;
        }
    }
    if (_rootViewController.m_bBrake && m_BusSpeedFloat > 0.2) {
        m_BusSpeedFloat -= 0.2; 
    }
    m_BusSpeed = ((int)((m_BusSpeedFloat + 1) / 2)) * 2;
    //NSLog(@"%d", m_BusSpeed);
    [m_labelSpeed setString:[NSString stringWithFormat:@"SPEED:%d mile/h", (unsigned int)(m_BusSpeedFloat * 4)]];
    [m_labelScore setString:[NSString stringWithFormat:@"SCORE:%d", [GameManager sharedGameManager].m_GameScore]];
    [m_labelTime setString:[NSString stringWithFormat:@"TIME:%d", m_Time]];
    [m_labelMiss setString:[NSString stringWithFormat:@"T_MISS:%d", m_MissT]];
    if (m_timecount % 10 == 0) {
        if (m_BusSpeed >= 2) {
            m_bSteering = YES;
            if (!m_Bus.m_bRunState) {
                m_Bus.m_bRunState = YES;
                [m_Bus setAnimRun];
            }
        } else {
            m_bSteering = NO;
            if (m_Bus.m_bRunState) {
                m_Bus.m_bRunState = NO;
                [m_Bus stopAnimRun];
            }
        }
    }     
    
    float minF = 227.0*iPadX;//257
    float maxF = 348.0*iPadX;//328
    if (m_bSteering) {
        //direct change
        CGPoint pos = m_Bus.position;
        float angle = m_Bus.rotation;
        if (m_steering_angle > 10 && pos.x < maxF) {
            m_Bus_angle_Old_Right = YES;
            pos.x += 2.0;
            pos.y -= 0.2;
            angle -= 0.06;            
        }
        if (m_steering_angle < -10 && pos.x > minF) {
            m_Bus_angle_Old_Right = NO;
            pos.x -= 2.0;
            pos.y += 0.2;
            angle += 0.06;
        }        
        m_Bus.position = pos;
        m_Bus.rotation = angle;       
        
    }
    
    if (m_timecount % 8 == 0) {        [self targetsWalk];
    }
    if (m_timecount % 3 == 0) {
        [self obstacleAnim_walk];
    }
    [self checkBusTargetHit];
    [self checkBusObstaclesHit];
    [self lifeNoHitTargetsReplace];
    
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    switch (m_levelNum) {
        case 1:  
            [cs_gameLayer runMove:runDistance speed:m_BusSpeed];
            break;
        case 2: 
            [mv_gameLayer runMove:runDistance speed:m_BusSpeed];
            break; 
        case 3:  
            [or_gameLayer runMove:runDistance speed:m_BusSpeed];
            break;
        case 4:   
            [sv_gameLayer runMove:runDistance speed:m_BusSpeed];            
            break; 
        case 5: 
            [sc_gameLayer runMove:runDistance speed:m_BusSpeed];
            break;
        case 6: 
            [sa_gameLayer runMove:runDistance speed:m_BusSpeed];
            break;
        case 7:
            [ca_gameLayer runMove:runDistance speed:m_BusSpeed];
            break;
        case 8:
            [fb_gameLayer runMove:runDistance speed:m_BusSpeed];
            break;
        default:
            [cs_gameLayer runMove:runDistance speed:m_BusSpeed];
            break;
    }
    [self showTargets:runDistance];
    [self showObstacles:runDistance];
}


- (void) setBackgroud //sky and groud at level 1...5
{    
    [self print_free_memory];
    switch (m_levelNum) {
        case 1:  
            cs_gameLayer = [CS_GameLayer_ipad node];
            [self addChild:cs_gameLayer z:0];
            break;
        case 2:            
            mv_gameLayer = [MV_GameLayer_ipad node];
            [self addChild:mv_gameLayer z:0];
            break;
        case 3:            
            or_gameLayer = [OR_GameLayer_ipad node];
            [self addChild:or_gameLayer z:0];
            break;
        case 4: 
            sv_gameLayer = [SV_GameLayer_ipad node];
            [self addChild:sv_gameLayer z:0];            
            break;
        case 5:
            sc_gameLayer = [SC_GameLayer_ipad node];
            [self addChild:sc_gameLayer z:0];
            break;
        case 6:   
            sa_gameLayer = [SA_GameLayer_ipad node];
            [sa_gameLayer setMainLayer:self];
            [self addChild:sa_gameLayer z:0];
            break;
        case 7:
            ca_gameLayer = [CA_GameLayer_ipad node];
            [ca_gameLayer setMainLayer:self];
            [self addChild:ca_gameLayer z:0];
            break;
        case 8:
            fb_gameLayer = [FB_GameLayer_ipad node];
            [fb_gameLayer setMainLayer:self];
            [self addChild:fb_gameLayer z:0];
            break;
        default:
            cs_gameLayer = [CS_GameLayer_ipad node];
            [self addChild:cs_gameLayer z:0];
            break;
    }
    [self print_free_memory];    
    
}

- (void) showTargets:(int)runDistance
{
    CTarget *target;
    float f_d1, f_d2, f_d3;
    if ([[GameManager sharedGameManager] isSelectAll]) {
        for (int i = 0; i < [m_targetArrChooseForAll  count]; i++)
        {
            int diff_BusTarget = -1;
            target = [m_targetArrChooseForAll objectAtIndex:i];
            
            diff_BusTarget = target.m_distance - (m_loopNum * 1200 + runDistance);
            
            target.visible = NO;
            if (diff_BusTarget > 0 && diff_BusTarget < SHOW_LIMIT && !target.m_bBusHit) {
                target.visible = YES;
                f_d1 = 52.0 - (52.0-14.0)/6*target.m_road_width;
                f_d2 = 110.0 + (380.0-110.0)/6*target.m_road_width;
                f_d3 = 418.0 + (463.0-418.0)/6*target.m_road_width;
                //x
                float ss;
                float sum;
                ss = f_d1;//57.0;
                sum = f_d2;//80.0;
                for (int i = 0; i < 50; i ++)
                {
                    if (i  == diff_BusTarget / 10) {
                        sum += ss * 0.831 / 10 * (diff_BusTarget % 10);
                        break;
                    }
                    sum += ss;
                    ss = ss * 0.831;
                }
                
                //y
                float ss1 = 25.0;
                float sum1 = 0.0;
                for (int i = 0; i < 50; i ++)
                {
                    if (i == diff_BusTarget / 10) {
                        sum1 += ss1 * 0.831 / 10 * (diff_BusTarget % 10);
                        break;
                    }
                    sum1 += ss1;
                    ss1 = ss1 * 0.831;//0.840;
                }
                CGPoint cur_pos;
                cur_pos.x = sum*iPadX;
                cur_pos.y = (sum1 - 5)*iPadY;
                target.position = cur_pos;
                
                float dist1;
                float dist2;
                dist1 = ccpDistance(CGPointMake(f_d2*iPadX, 0), CGPointMake(f_d3*iPadX, 148*iPadY));//a
                dist2 = ccpDistance(cur_pos, CGPointMake(f_d3*iPadX, 148*iPadY));//b
                target.scale = 0.5 * (dist2 + 0.5 * dist1) / 0.5 / dist1; //dist2/(dist1*0.3); //(dist2 + 0.8 * dist1)/dist1;
                //target.scale *= 1.5;
                target.scale *= 2.0;
            }
        }
        
    }
    else {
        for (int i = 0; i < [m_targetArray  count]; i++)
        {
            if (i+1 != [GameManager sharedGameManager].targetNum1 && i+1 != [GameManager sharedGameManager].targetNum2 && i+1 != [GameManager sharedGameManager].targetNum3 && i+1 != [GameManager sharedGameManager].targetNum4) {// && i+1 != [GameManager sharedGameManager].targetNum5 && i+1 != [GameManager sharedGameManager].targetNum6 && i+1 != [GameManager sharedGameManager].targetNum7) {
                continue;
            }
            int diff_BusTarget = -1;        
            target = [m_targetArray objectAtIndex:i];        
            
            diff_BusTarget = target.m_distance - (m_loopNum * 1200 + runDistance);
            
            target.visible = NO;
            if (diff_BusTarget > 0 && diff_BusTarget < SHOW_LIMIT && !target.m_bBusHit) {
                target.visible = YES;
                f_d1 = 52.0 - (52.0-14.0)/6*target.m_road_width;
                f_d2 = 110.0 + (380.0-110.0)/6*target.m_road_width;
                f_d3 = 418.0 + (463.0-418.0)/6*target.m_road_width;
                //x
                float ss;
                float sum;
                ss = f_d1;//57.0;
                sum = f_d2;//80.0;
                for (int i = 0; i < 50; i ++)
                {                        
                    if (i  == diff_BusTarget / 10) {
                        sum += ss * 0.831 / 10 * (diff_BusTarget % 10);            
                        break;
                    }
                    sum += ss; 
                    ss = ss * 0.831;             
                }
                
                //y
                float ss1 = 25.0;
                float sum1 = 0.0;
                for (int i = 0; i < 50; i ++)
                {               
                    if (i == diff_BusTarget / 10) {
                        sum1 += ss1 * 0.831 / 10 * (diff_BusTarget % 10);
                        break;
                    }   
                    sum1 += ss1;
                    ss1 = ss1 * 0.831;//0.840;             
                }
                CGPoint cur_pos;
                cur_pos.x = sum*iPadX;
                cur_pos.y = (sum1 - 5)*iPadY;            
                target.position = cur_pos;
                
                float dist1;
                float dist2;
                dist1 = ccpDistance(CGPointMake(f_d2*iPadX, 0), CGPointMake(f_d3*iPadX, 148*iPadY));//a
                dist2 = ccpDistance(cur_pos, CGPointMake(f_d3*iPadX, 148*iPadY));//b
                target.scale = 0.5 * (dist2 + 0.5 * dist1) / 0.5 / dist1; //dist2/(dist1*0.3); //(dist2 + 0.8 * dist1)/dist1;
                //target.scale *= 1.5;
                target.scale *= 2.0;
            }
        }
    }
}
- (void) targetsWalk
{
    CTarget *target;
    if ([[GameManager sharedGameManager] isSelectAll]) {
        for (int i = 0; i < [m_targetArrChooseForAll count]; i++)//3 in 29
        {
            target = [m_targetArrChooseForAll objectAtIndex:i];
            
            if (target.m_distance - (m_loopNum * 1200 + m_runDistance) < -20) {
                //NSLog(@"%d, target.m_distance: %d", i, target.m_distance);
                continue;
            }
            target.m_distance--;
        }
    }
    else {
        for (int i = 0; i < [m_targetArray count]; i++)//3 in 29
        {   
            if (i+1 != [GameManager sharedGameManager].targetNum1 && i+1 != [GameManager sharedGameManager].targetNum2 && i+1 != [GameManager sharedGameManager].targetNum3 && i+1 != [GameManager sharedGameManager].targetNum4) {// && i+1 != [GameManager sharedGameManager].targetNum5 && i+1 != [GameManager sharedGameManager].targetNum6 && i+1 != [GameManager sharedGameManager].targetNum7) {
                continue;
            }
            target = [m_targetArray objectAtIndex:i];
            
            if (target.m_distance - (m_loopNum * 1200 + m_runDistance) < -20) {
                //NSLog(@"%d, target.m_distance: %d", i, target.m_distance);
                continue;
            }
            target.m_distance--;
        }
    }
}
- (void) obstacleAnim_walk
{
    if (m_girlWalk.visible) {
        m_girlWalk.m_road_width += 0.1;
    }
    if (m_scBoy.visible) {
        m_scBoy.m_road_width += 0.1;
    }
    if (m_scGirl.visible) {
        m_scGirl.m_road_width += 0.1;
    }
    if (m_scTeacher.visible) {
        m_scTeacher.m_road_width += 0.1;
    }
}

- (void) showObstacles:(int)runDistance {
    //level2
    switch ([GameManager sharedGameManager].m_levelNum) {
        case 2:
        case 3:
        {
            CRock *rock;
            int count=0;
            if ([GameManager sharedGameManager].m_levelNum == 2) {
                count = 2;
            }
            if ([GameManager sharedGameManager].m_levelNum == 3) {
                count = 4;
            }
            for (int i = 0; i < count; i++)
            {        
                int diff_BusRock = -1;        
                rock = [m_RockArray objectAtIndex:i];
                
                
                diff_BusRock = rock.m_distance - (m_loopNum * 1200 + runDistance);
                
                rock.visible = NO;
                if (diff_BusRock > 0 && diff_BusRock < SHOW_LIMIT && !rock.m_bBusHit) {
                    rock.visible = YES;
                    
                    //x
                    float ss;
                    float sum;
                    ss = 62.0;
                    sum = 50.0;
                    for (int i = 0; i < 50; i ++)
                    {                        
                        if (i  == diff_BusRock / 10) {
                            sum += ss * 0.831 / 10 * (diff_BusRock % 10);            
                            break;
                        }
                        sum += ss; 
                        ss = ss * 0.831;             
                    }
                    
                    //y
                    float ss1 = 25.0;
                    float sum1 = 0.0;
                    for (int i = 0; i < 50; i ++)
                    {               
                        if (i == diff_BusRock / 10) {
                            sum1 += ss1 * 0.831 / 10 * (diff_BusRock % 10);
                            break;
                        }   
                        sum1 += ss1;
                        ss1 = ss1 * 0.831;//0.840;             
                    }
                    CGPoint cur_pos;
                    cur_pos.x = sum;
                    cur_pos.y = sum1;
                    float xx = sum + rock.m_road_width / 10.0 * (480 - sum);
                    rock.position = CGPointMake(xx*iPadX, cur_pos.y*iPadY);
                    
                    float dist1;
                    float dist2;
                    dist1 = ccpDistance(CGPointMake(50, 0), CGPointMake(416, 148));//a
                    dist2 = ccpDistance(cur_pos, CGPointMake(416, 148));//b
                    rock.scale = (dist2 + 0.25 * dist1) / 0.25 / dist1;   
                    rock.scale = rock.scale * 0.1;
                }
            }     
        }
            break;
        case 4:
        {
            SV4Obstacle *sv4Obstacle;
            for (int i = 0; i < 5; i++)//for (int i = 0; i < [m_SV4ObstacleArray count]; i++)
            {        
                int diff_BusRock = -1;        
                sv4Obstacle = [m_SV4ObstacleArray objectAtIndex:i];                
                
                diff_BusRock = sv4Obstacle.m_distance - (m_loopNum * 1200 + runDistance);
                
                sv4Obstacle.visible = NO;
                if (diff_BusRock > 0 && diff_BusRock < SHOW_LIMIT && !sv4Obstacle.m_bBusHit) {
                    sv4Obstacle.visible = YES;                    
                    //x
                    float ss;
                    float sum;
                    ss = 62.0;
                    sum = 50.0;
                    for (int i = 0; i < 50; i ++)
                    {                        
                        if (i  == diff_BusRock / 10) {
                            sum += ss * 0.831 / 10 * (diff_BusRock % 10);            
                            break;
                        }
                        sum += ss; 
                        ss = ss * 0.831;             
                    }                    
                    //y
                    float ss1 = 25.0;
                    float sum1 = 0.0;
                    for (int i = 0; i < 50; i ++)
                    {               
                        if (i == diff_BusRock / 10) {
                            sum1 += ss1 * 0.831 / 10 * (diff_BusRock % 10);
                            break;
                        }   
                        sum1 += ss1;
                        ss1 = ss1 * 0.831;//0.840;             
                    }
                    CGPoint cur_pos;
                    cur_pos.x = sum;
                    cur_pos.y = sum1;
                    float xx = sum + sv4Obstacle.m_road_width / 10.0 * (480 - sum);
                    sv4Obstacle.position = CGPointMake(xx*iPadX, cur_pos.y*iPadY);
                    
                    float dist1;
                    float dist2;
                    dist1 = ccpDistance(CGPointMake(50, 0), CGPointMake(416, 148));//a
                    dist2 = ccpDistance(cur_pos, CGPointMake(416, 148));//b
                    sv4Obstacle.scale = (dist2 + 0.25 * dist1) / 0.25 / dist1;   
                    sv4Obstacle.scale = sv4Obstacle.scale * 0.8;
                }
            }
            //girlwalk show
            {
                int diff_BusRock = -1; 
                
                diff_BusRock = m_girlWalk.m_distance - (m_loopNum * 1200 + runDistance);
                
                m_girlWalk.visible = NO;
                if (diff_BusRock > 0 && diff_BusRock < SHOW_LIMIT && !m_girlWalk.m_bBusHit) {
                    m_girlWalk.visible = YES;                    
                    //x
                    float ss;
                    float sum;
                    ss = 62.0;
                    sum = 50.0;
                    for (int i = 0; i < 50; i ++)
                    {                        
                        if (i  == diff_BusRock / 10) {
                            sum += ss * 0.831 / 10 * (diff_BusRock % 10);
                            break;
                        }
                        sum += ss; 
                        ss = ss * 0.831;             
                    }                    
                    //y
                    float ss1 = 25.0;
                    float sum1 = 0.0;
                    for (int i = 0; i < 50; i ++)
                    {               
                        if (i == diff_BusRock / 10) {
                            sum1 += ss1 * 0.831 / 10 * (diff_BusRock % 10);
                            break;
                        }   
                        sum1 += ss1;
                        ss1 = ss1 * 0.831;//0.840;             
                    }
                    CGPoint cur_pos;
                    cur_pos.x = sum;
                    cur_pos.y = sum1;
                    float xx = sum + m_girlWalk.m_road_width / 10.0 * (480 - sum);
                    m_girlWalk.position = CGPointMake(xx*iPadX, cur_pos.y*iPadY);
                    
                    float dist1;
                    float dist2;
                    dist1 = ccpDistance(CGPointMake(50, 0), CGPointMake(416, 148));//a
                    dist2 = ccpDistance(cur_pos, CGPointMake(416, 148));//b
                    m_girlWalk.scale = (dist2 + 0.25 * dist1) / 0.25 / dist1;   
                    m_girlWalk.scale = m_girlWalk.scale * 0.6;
                }
            }
        }
            break;
        case 5:
        {
            SV4Obstacle *sv4Obstacle;
            for (int i = 3; i < 4; i++)//1 object
            {        
                int diff_BusRock = -1;        
                sv4Obstacle = [m_SV4ObstacleArray objectAtIndex:i];                
                
                diff_BusRock = sv4Obstacle.m_distance - (m_loopNum * 1200 + runDistance);
                
                sv4Obstacle.visible = NO;
                if (diff_BusRock > 0 && diff_BusRock < SHOW_LIMIT && !sv4Obstacle.m_bBusHit) {
                    sv4Obstacle.visible = YES;                    
                    //x
                    float ss;
                    float sum;
                    ss = 62.0;
                    sum = 50.0;
                    for (int i = 0; i < 50; i ++)
                    {                        
                        if (i  == diff_BusRock / 10) {
                            sum += ss * 0.831 / 10 * (diff_BusRock % 10);            
                            break;
                        }
                        sum += ss; 
                        ss = ss * 0.831;             
                    }                    
                    //y
                    float ss1 = 25.0;
                    float sum1 = 0.0;
                    for (int i = 0; i < 50; i ++)
                    {               
                        if (i == diff_BusRock / 10) {
                            sum1 += ss1 * 0.831 / 10 * (diff_BusRock % 10);
                            break;
                        }   
                        sum1 += ss1;
                        ss1 = ss1 * 0.831;//0.840;             
                    }
                    CGPoint cur_pos;
                    cur_pos.x = sum;
                    cur_pos.y = sum1;
                    float xx = sum + sv4Obstacle.m_road_width / 10.0 * (480 - sum);
                    sv4Obstacle.position = CGPointMake(xx*iPadX, cur_pos.y*iPadY);
                    
                    float dist1;
                    float dist2;
                    dist1 = ccpDistance(CGPointMake(50, 0), CGPointMake(416, 148));//a
                    dist2 = ccpDistance(cur_pos, CGPointMake(416, 148));//b
                    sv4Obstacle.scale = (dist2 + 0.25 * dist1) / 0.25 / dist1;   
                    sv4Obstacle.scale = sv4Obstacle.scale * 0.8;
                }
            }
            [self SV5AnimShow:runDistance];            
            break;
        }
        case 6:
        {
            SV4Obstacle *sv4Obstacle;
            for (int i = 0; i < 7; i++)//1 object
            {        
                int diff_BusRock = -1;        
                sv4Obstacle = [m_SV4ObstacleArray objectAtIndex:i];                
                
                diff_BusRock = sv4Obstacle.m_distance - (m_loopNum * 1200 + runDistance);
                
                sv4Obstacle.visible = NO;
                if (diff_BusRock > 0 && diff_BusRock < SHOW_LIMIT && !sv4Obstacle.m_bBusHit) {
                    sv4Obstacle.visible = YES;                    
                    //x
                    float ss;
                    float sum;
                    ss = 62.0;
                    sum = 50.0;
                    for (int i = 0; i < 50; i ++)
                    {                        
                        if (i  == diff_BusRock / 10) {
                            sum += ss * 0.831 / 10 * (diff_BusRock % 10);            
                            break;
                        }
                        sum += ss; 
                        ss = ss * 0.831;             
                    }                    
                    //y
                    float ss1 = 25.0;
                    float sum1 = 0.0;
                    for (int i = 0; i < 50; i ++)
                    {               
                        if (i == diff_BusRock / 10) {
                            sum1 += ss1 * 0.831 / 10 * (diff_BusRock % 10);
                            break;
                        }   
                        sum1 += ss1;
                        ss1 = ss1 * 0.831;//0.840;             
                    }
                    CGPoint cur_pos;
                    cur_pos.x = sum;
                    cur_pos.y = sum1;
                    float xx = sum + sv4Obstacle.m_road_width / 10.0 * (480 - sum);
                    sv4Obstacle.position = CGPointMake(xx*iPadX, cur_pos.y*iPadY);
                    
                    float dist1;
                    float dist2;
                    dist1 = ccpDistance(CGPointMake(50, 0), CGPointMake(416, 148));//a
                    dist2 = ccpDistance(cur_pos, CGPointMake(416, 148));//b
                    sv4Obstacle.scale = (dist2 + 0.25 * dist1) / 0.25 / dist1;   
                    sv4Obstacle.scale = sv4Obstacle.scale * 0.8;
                }
            }  
            
        }            
            break;
        case 7:
        {
            SV4Obstacle *sv4Obstacle;
            for (int i = 0; i < 7; i++)//1 object
            {        
                int diff_BusRock = -1;        
                sv4Obstacle = [m_SV4ObstacleArray objectAtIndex:i];                
                
                diff_BusRock = sv4Obstacle.m_distance - (m_loopNum * 1200 + runDistance);
                
                sv4Obstacle.visible = NO;
                if (diff_BusRock > 0 && diff_BusRock < SHOW_LIMIT && !sv4Obstacle.m_bBusHit) {
                    sv4Obstacle.visible = YES;                    
                    //x
                    float ss;
                    float sum;
                    ss = 62.0;
                    sum = 50.0;
                    for (int i = 0; i < 50; i ++)
                    {                        
                        if (i  == diff_BusRock / 10) {
                            sum += ss * 0.831 / 10 * (diff_BusRock % 10);            
                            break;
                        }
                        sum += ss; 
                        ss = ss * 0.831;             
                    }                    
                    //y
                    float ss1 = 25.0;
                    float sum1 = 0.0;
                    for (int i = 0; i < 50; i ++)
                    {               
                        if (i == diff_BusRock / 10) {
                            sum1 += ss1 * 0.831 / 10 * (diff_BusRock % 10);
                            break;
                        }   
                        sum1 += ss1;
                        ss1 = ss1 * 0.831;//0.840;             
                    }
                    CGPoint cur_pos;
                    cur_pos.x = sum;
                    cur_pos.y = sum1;
                    float xx = sum + sv4Obstacle.m_road_width / 10.0 * (480 - sum);
                    sv4Obstacle.position = CGPointMake(xx*iPadX, cur_pos.y*iPadY);
                    
                    float dist1;
                    float dist2;
                    dist1 = ccpDistance(CGPointMake(50, 0), CGPointMake(416, 148));//a
                    dist2 = ccpDistance(cur_pos, CGPointMake(416, 148));//b
                    sv4Obstacle.scale = (dist2 + 0.25 * dist1) / 0.25 / dist1;   
                    sv4Obstacle.scale = sv4Obstacle.scale * 0.8;
                }
            }                        
        }
            break;
        case 8:
        {
            SV4Obstacle *sv4Obstacle;
            for (int i = 0; i < 7; i++)//1 object
            {
                int diff_BusRock = -1;
                sv4Obstacle = [m_SV4ObstacleArray objectAtIndex:i];
                
                diff_BusRock = sv4Obstacle.m_distance - (m_loopNum * 1200 + runDistance);
                
                sv4Obstacle.visible = NO;
                if (diff_BusRock > 0 && diff_BusRock < SHOW_LIMIT && !sv4Obstacle.m_bBusHit) {
                    sv4Obstacle.visible = YES;
                    //x
                    float ss;
                    float sum;
                    ss = 62.0;
                    sum = 50.0;
                    for (int i = 0; i < 50; i ++)
                    {
                        if (i  == diff_BusRock / 10) {
                            sum += ss * 0.831 / 10 * (diff_BusRock % 10);
                            break;
                        }
                        sum += ss;
                        ss = ss * 0.831;
                    }
                    //y
                    float ss1 = 25.0;
                    float sum1 = 0.0;
                    for (int i = 0; i < 50; i ++)
                    {
                        if (i == diff_BusRock / 10) {
                            sum1 += ss1 * 0.831 / 10 * (diff_BusRock % 10);
                            break;
                        }
                        sum1 += ss1;
                        ss1 = ss1 * 0.831;//0.840;
                    }
                    CGPoint cur_pos;
                    cur_pos.y = sum1;
                    cur_pos.x = sum;
                    float xx = sum + sv4Obstacle.m_road_width / 10.0 * (480 - sum);
                    sv4Obstacle.position = CGPointMake(xx*iPadX, cur_pos.y*iPadY);
                    
                    float dist1;
                    float dist2;
                    dist1 = ccpDistance(CGPointMake(50, 0), CGPointMake(416, 148));//a
                    dist2 = ccpDistance(cur_pos, CGPointMake(416, 148));//b
                    sv4Obstacle.scale = (dist2 + 0.25 * dist1) / 0.25 / dist1;
                    sv4Obstacle.scale = sv4Obstacle.scale * 0.25;
                }
            }
        }
            break;
        default:
            break;
    }
    
}
- (void) checkBusObstaclesHit
{
    //level 2
    switch ([GameManager sharedGameManager].m_levelNum) {
        case 2:
        case 3:  
        {
            CRock *rock;
            CGRect busRect = CGRectMake(m_BusHitAreaSprite.position.x - (52*iPadX)/2, m_BusHitAreaSprite.position.y - (16*iPadY)/2, 52*iPadX, 16*iPadY);
            int count=0;
            if ([GameManager sharedGameManager].m_levelNum == 2) {
                count = 2;
            }
            if ([GameManager sharedGameManager].m_levelNum == 3) {
                count = 4;
            }
            for (int i = 0; i < count; i++)
            { 
                rock = [m_RockArray objectAtIndex:i];
                if (rock.m_bBusHit) {
                    continue;
                }
                float obstacle_width = 80 * rock.scale;
                float obstacle_height = 48 * rock.scale;
                CGRect obstacleRect = CGRectMake(rock.position.x - obstacle_width/2, rock.position.y - obstacle_height/2, obstacle_width, obstacle_height);
                if (CGRectIntersectsRect(busRect, obstacleRect)) {
                    rock.m_bBusHit = YES;
                    [GameManager sharedGameManager].m_GameScore -= 5;
                    [self bus_break];
                }
            }
        }
            break;
        case 6:
        case 7:
        case 4:
        case 8:
        {
            SV4Obstacle *rock;
            CGRect busRect = CGRectMake(m_BusHitAreaSprite.position.x - (52*iPadX)/2, m_BusHitAreaSprite.position.y - (16*iPadY)/2, 52*iPadX, 16*iPadY);
            
            for (int i = 0; i < 7; i++)
            { 
                rock = [m_SV4ObstacleArray objectAtIndex:i];
                if (rock.m_bBusHit) {
                    continue;
                }
                float obstacle_width = 40 * rock.scale;
                float obstacle_height = 40 * rock.scale;
                CGRect obstacleRect = CGRectMake(rock.position.x - obstacle_width/2, rock.position.y - obstacle_height/2, obstacle_width, obstacle_height);
                if (CGRectIntersectsRect(busRect, obstacleRect)) {
                    rock.m_bBusHit = YES;
                    [GameManager sharedGameManager].m_GameScore -= 5;
                    [self bus_break];
                }
            }
            if (!m_girlWalk.m_bBusHit) {
                float obstacle_width = 20 * m_girlWalk.scale;
                float obstacle_height = 40 * m_girlWalk.scale;
                CGRect obstacleRect = CGRectMake(m_girlWalk.position.x - obstacle_width/2, m_girlWalk.position.y - obstacle_height/2, obstacle_width, obstacle_height);
                if (CGRectIntersectsRect(busRect, obstacleRect)) {
                    m_girlWalk.m_bBusHit = YES;
                    [GameManager sharedGameManager].m_GameScore -= 5;
                    [self bus_break];
                }
            }
        }
            break;
        case 5:
        {
            SV4Obstacle *rock;
            CGRect busRect = CGRectMake(m_BusHitAreaSprite.position.x - (52*iPadX)/2, m_BusHitAreaSprite.position.y - (16*iPadY)/2, 52*iPadX, 16*iPadY);
            
            for (int i = 3; i < 4; i++)
            { 
                rock = [m_SV4ObstacleArray objectAtIndex:i];
                if (rock.m_bBusHit) {
                    continue;
                }
                float obstacle_width = 40 * rock.scale;
                float obstacle_height = 40 * rock.scale;
                CGRect obstacleRect = CGRectMake(rock.position.x - obstacle_width/2, rock.position.y - obstacle_height/2, obstacle_width, obstacle_height);
                if (CGRectIntersectsRect(busRect, obstacleRect)) {
                    rock.m_bBusHit = YES;
                    [GameManager sharedGameManager].m_GameScore -= 5;
                    [self bus_break];
                }
            }
            if (!m_girlWalk.m_bBusHit) {
                float obstacle_width = 20 * m_girlWalk.scale;
                float obstacle_height = 40 * m_girlWalk.scale;
                CGRect obstacleRect = CGRectMake(m_girlWalk.position.x - obstacle_width/2, m_girlWalk.position.y - obstacle_height/2, obstacle_width, obstacle_height);
                if (CGRectIntersectsRect(busRect, obstacleRect)) {
                    m_girlWalk.m_bBusHit = YES;
                    [GameManager sharedGameManager].m_GameScore -= 5;
                    [self bus_break];
                }
            }
            if (!m_scBoy.m_bBusHit) {
                float obstacle_width = 20 * m_scBoy.scale;
                float obstacle_height = 40 * m_scBoy.scale;
                CGRect obstacleRect = CGRectMake(m_scBoy.position.x - obstacle_width/2, m_scBoy.position.y - obstacle_height/2, obstacle_width, obstacle_height);
                if (CGRectIntersectsRect(busRect, obstacleRect)) {
                    m_scBoy.m_bBusHit = YES;
                    [GameManager sharedGameManager].m_GameScore -= 5;
                    [self bus_break];
                }
            }
            if (!m_scGirl.m_bBusHit) {
                float obstacle_width = 20 * m_scGirl.scale;
                float obstacle_height = 40 * m_scGirl.scale;
                CGRect obstacleRect = CGRectMake(m_scGirl.position.x - obstacle_width/2, m_scGirl.position.y - obstacle_height/2, obstacle_width, obstacle_height);
                if (CGRectIntersectsRect(busRect, obstacleRect)) {
                    m_scGirl.m_bBusHit = YES;
                    [GameManager sharedGameManager].m_GameScore -= 5;
                    [self bus_break];
                }
            }
            if (!m_scTeacher.m_bBusHit) {
                float obstacle_width = 20 * m_scTeacher.scale;
                float obstacle_height = 40 * m_scTeacher.scale;
                CGRect obstacleRect = CGRectMake(m_scTeacher.position.x - obstacle_width/2, m_scTeacher.position.y - obstacle_height/2, obstacle_width, obstacle_height);
                if (CGRectIntersectsRect(busRect, obstacleRect)) {
                    m_scTeacher.m_bBusHit = YES;
                    [GameManager sharedGameManager].m_GameScore -= 5;
                    [self bus_break];
                }
            }
        }
            break;
        default:
            break;
    }
    
}
- (void) SV5AnimShow:(int)runDistance
{
    //girlwalk show
    int diff_BusRock = -1; 
    
    diff_BusRock = m_girlWalk.m_distance - (m_loopNum * 1200 + runDistance);
    
    m_girlWalk.visible = NO;
    if (diff_BusRock > 0 && diff_BusRock < SHOW_LIMIT && !m_girlWalk.m_bBusHit) {
        m_girlWalk.visible = YES;                    
        //x
        float ss;
        float sum;
        ss = 62.0;
        sum = 50.0;
        for (int i = 0; i < 50; i ++)
        {                        
            if (i  == diff_BusRock / 10) {
                sum += ss * 0.831 / 10 * (diff_BusRock % 10);            
                break;
            }
            sum += ss; 
            ss = ss * 0.831;             
        }                    
        //y
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {               
            if (i == diff_BusRock / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusRock % 10);
                break;
            }   
            sum1 += ss1;
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        float xx = sum + m_girlWalk.m_road_width / 10.0 * (480 - sum);
        m_girlWalk.position = CGPointMake(xx*iPadX, cur_pos.y*iPadY);
        
        float dist1;
        float dist2;
        dist1 = ccpDistance(CGPointMake(50, 0), CGPointMake(416, 148));//a
        dist2 = ccpDistance(cur_pos, CGPointMake(416, 148));//b
        m_girlWalk.scale = (dist2 + 0.25 * dist1) / 0.25 / dist1;   
        m_girlWalk.scale = m_girlWalk.scale * 0.5;
    }
    // scBoy show
    diff_BusRock = -1;
    diff_BusRock = m_scBoy.m_distance - (m_loopNum * 1200 + runDistance);
    
    m_scBoy.visible = NO;
    if (diff_BusRock > 0 && diff_BusRock < SHOW_LIMIT && !m_scBoy.m_bBusHit) {
        m_scBoy.visible = YES;                    
        //x
        float ss;
        float sum;
        ss = 62.0;
        sum = 50.0;
        for (int i = 0; i < 50; i ++)
        {                        
            if (i  == diff_BusRock / 10) {
                sum += ss * 0.831 / 10 * (diff_BusRock % 10);            
                break;
            }
            sum += ss; 
            ss = ss * 0.831;             
        }                    
        //y
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {               
            if (i == diff_BusRock / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusRock % 10);
                break;
            }   
            sum1 += ss1;
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        float xx = sum + m_scBoy.m_road_width / 10.0 * (480 - sum);
        m_scBoy.position = CGPointMake(xx*iPadX, cur_pos.y*iPadY);
        
        float dist1;
        float dist2;
        dist1 = ccpDistance(CGPointMake(50, 0), CGPointMake(416, 148));//a
        dist2 = ccpDistance(cur_pos, CGPointMake(416, 148));//b
        m_scBoy.scale = (dist2 + 0.25 * dist1) / 0.25 / dist1;   
        m_scBoy.scale = m_scBoy.scale * 0.5;
    }
    // scGirl show
    diff_BusRock = -1;
    diff_BusRock = m_scGirl.m_distance - (m_loopNum * 1200 + runDistance);
    
    m_scGirl.visible = NO;
    if (diff_BusRock > 0 && diff_BusRock < SHOW_LIMIT && !m_scGirl.m_bBusHit) {
        m_scGirl.visible = YES;                    
        //x
        float ss;
        float sum;
        ss = 52.0;//62.0;
        sum = 110.0;//50.0;
        for (int i = 0; i < 50; i ++)
        {                        
            if (i  == diff_BusRock / 10) {
                sum += ss * 0.831 / 10 * (diff_BusRock % 10);            
                break;
            }
            sum += ss; 
            ss = ss * 0.831;             
        }                    
        //y
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {               
            if (i == diff_BusRock / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusRock % 10);
                break;
            }   
            sum1 += ss1;
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        float xx = sum + m_scGirl.m_road_width / 10.0 * (480 - sum);
        m_scGirl.position = CGPointMake(xx*iPadX, cur_pos.y*iPadY);
        
        float dist1;
        float dist2;
        dist1 = ccpDistance(CGPointMake(50, 0), CGPointMake(416, 148));//a
        dist2 = ccpDistance(cur_pos, CGPointMake(416, 148));//b
        m_scGirl.scale = (dist2 + 0.25 * dist1) / 0.25 / dist1;   
        m_scGirl.scale = m_scGirl.scale * 0.5;
    }
    // scTeach show
    diff_BusRock = -1;
    diff_BusRock = m_scTeacher.m_distance - (m_loopNum * 1200 + runDistance);
    
    m_scTeacher.visible = NO;
    if (diff_BusRock > 0 && diff_BusRock < SHOW_LIMIT && !m_scTeacher.m_bBusHit) {
        m_scTeacher.visible = YES;                    
        //x
        float ss;
        float sum;
        ss = 62.0;
        sum = 50.0;
        for (int i = 0; i < 50; i ++)
        {                        
            if (i  == diff_BusRock / 10) {
                sum += ss * 0.831 / 10 * (diff_BusRock % 10);            
                break;
            }
            sum += ss; 
            ss = ss * 0.831;             
        }                    
        //y
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {               
            if (i == diff_BusRock / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusRock % 10);
                break;
            }   
            sum1 += ss1;
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        float xx = sum + m_scTeacher.m_road_width / 10.0 * (480 - sum);
        m_scTeacher.position = CGPointMake(xx*iPadX, cur_pos.y*iPadY);
        
        float dist1;
        float dist2;
        dist1 = ccpDistance(CGPointMake(50, 0), CGPointMake(416, 148));//a
        dist2 = ccpDistance(cur_pos, CGPointMake(416, 148));//b
        m_scTeacher.scale = (dist2 + 0.25 * dist1) / 0.25 / dist1;
        m_scTeacher.scale = m_scTeacher.scale * 0.5;
    }
}

- (CGPoint) getCollectPos:(CGRect)busRect targetRect:(CGRect)targetRect
{
    CGPoint retPos = CGPointZero;
    //get x    
    for (CGFloat px = busRect.origin.x; px < busRect.origin.x + busRect.size.width; px += 0.1) {
        if (px > targetRect.origin.x && px < targetRect.origin.x + targetRect.size.width) {
            retPos.x = px;
            break;
        }
        //retPos.x = targetRect.origin.x;
    }
    //get y
    for (CGFloat py = busRect.origin.y; py < busRect.origin.y + busRect.size.height; py += 0.1) {
        if (py > targetRect.origin.y && py < targetRect.origin.y + targetRect.size.height) {
            retPos.y = py;
            break;
        }
        //retPos.y = targetRect.origin.y;
    }
    return retPos;
}
- (void) checkBusTargetHit
{    
    bool bHit = NO;
    bool bKill;
    if (_rootViewController.m_bBusTargetHit)
        return;
    //CGAffineTransform transform1 = CGAffineTransformMakeTranslation(-302, -50);
    //m_BusHitAreaSprite.position = CGPointMake(47, 14);
    CGAffineTransform transform1 = CGAffineTransformMakeRotation(-m_Bus.rotation/180 * M_PI);
    CGAffineTransform transform2 = CGAffineTransformMakeTranslation(302*iPadX, 50*iPadY);
    CGAffineTransform transform3 = CGAffineTransformMakeTranslation(m_Bus.position.x - 302*iPadX, m_Bus.position.y - 50*iPadY);
    CGPoint busPos = CGPointMake(47*iPadX, 14*iPadY);
    busPos = CGPointApplyAffineTransform(busPos, transform1);
    busPos = CGPointApplyAffineTransform(busPos, transform2);
    busPos = CGPointApplyAffineTransform(busPos, transform3);
    
    m_BusHitAreaSprite.position = busPos;
    
    CGRect busRect = CGRectMake((busPos.x - (52*iPadX)/2), (busPos.y - (16*iPadY)/2), 52*iPadX, 16*iPadY);
    //NSLog(@"busPos:%@ busRect:%@", NSStringFromCGPoint(busPos), NSStringFromCGRect(busRect));
    CGRect targetRect;
    CTarget *target;
    if ([[GameManager sharedGameManager] isSelectAll]) {
        for (int k = 0; k < [m_targetArrChooseForAll count]; k++)
        {
            int index=k+1;
            
            target = [m_targetArrChooseForAll objectAtIndex:k];
            if (target.visible == NO)
                continue;
            int diff = target.m_road_width * 2;//target.m_bLeftSide ? 0 : 20;
            targetRect = CGRectMake(
                                    target.position.x - (target.m_Sprite.contentSize.width/8 * target.scale),
                                    target.position.y + diff,//kgh_06_23
                                    target.m_Sprite.contentSize.width/4 * target.scale,
                                    target.m_Sprite.contentSize.height/4 * target.scale);
            if (CGRectIntersectsRect(busRect, targetRect)) {
                _rootViewController.m_bBusTargetHit = YES;
                m_BusSpeed = 0;
                m_BusSpeedFloat = 0.0f;
                //m_CollideCountLeft = m_CollideCountRight = 0;
                //NSLog(@"hit %@, %@ angle:%.2f", NSStringFromCGRect(busRect), NSStringFromCGRect(targetRect), m_Bus_angle);
                target.m_bBusHit = YES;
                target.m_distance -= 10;
                [self showTargets:m_runDistance];
                //srand(time(nil) % 256);
                if (rand() % 2 == 0) {//kill
                    if (index == 1) {
                        [GameManager sharedGameManager].m_target1HitNum = 3;
                        _rootViewController.target1ImageX.hidden = NO;
                    }
                    if (index == 2) {
                        [GameManager sharedGameManager].m_target2HitNum = 3;
                        _rootViewController.target2ImageX.hidden = NO;
                    }
                    if (index == 3) {
                        [GameManager sharedGameManager].m_target3HitNum = 3;
                        _rootViewController.target3ImageX.hidden = NO;
                    }
                    if (index == 4) {
                        [GameManager sharedGameManager].m_target4HitNum = 3;
                        _rootViewController.target4ImageX.hidden = NO;
                    }
                    bHit = YES;
                    bKill = YES;
                    //kill movie
                } else {
                    if (m_levelNum == 6 || m_levelNum == 7) {
                        bKill = YES;
                        bHit = YES;
                        if (index == 1) {
                            [GameManager sharedGameManager].m_target1HitNum = 3;
                            _rootViewController.target1ImageX.hidden = NO;
                        }
                        if (index == 2) {
                            [GameManager sharedGameManager].m_target2HitNum = 3;
                            _rootViewController.target2ImageX.hidden = NO;
                        }
                        if (index == 3) {
                            [GameManager sharedGameManager].m_target3HitNum = 3;
                            _rootViewController.target3ImageX.hidden = NO;
                        }
                        if (index == 4) {
                            [GameManager sharedGameManager].m_target4HitNum = 3;
                            _rootViewController.target4ImageX.hidden = NO;
                        }
                    }
                    else {
                        bKill = NO;
                        switch (index) {
                            case 1:
                                [GameManager sharedGameManager].m_target1HitNum++;
                                if ([GameManager sharedGameManager].m_target1HitNum > 2) {
                                    _rootViewController.target1ImageX.hidden = NO;
                                    bKill = YES;
                                }
                                break;
                            case 2:
                                [GameManager sharedGameManager].m_target2HitNum++;
                                if ([GameManager sharedGameManager].m_target2HitNum > 2) {
                                    _rootViewController.target2ImageX.hidden = NO;
                                    bKill = YES;
                                }
                                break;
                            case 3:
                                [GameManager sharedGameManager].m_target3HitNum++;
                                if ([GameManager sharedGameManager].m_target3HitNum > 2) {
                                    _rootViewController.target3ImageX.hidden = NO;
                                    bKill = YES;
                                }
                                break;
                            case 4:
                                [GameManager sharedGameManager].m_target4HitNum++;
                                if ([GameManager sharedGameManager].m_target4HitNum > 2) {
                                    _rootViewController.target4ImageX.hidden = NO;
                                    bKill = YES;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    if (bKill) {
                        bHit = YES;
                        bKill = YES;
                        //kill movie
                    } else {
                        bHit = YES;
                        bKill = NO;
                        target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 700;
                        target.m_bBusHit = NO;
                        
                    }
                    
                }
            }
        }
    }
    else {
        for (int i = 0; i < [m_targetArray count]; i++)
        {
            if (i+1 != [GameManager sharedGameManager].targetNum1 && i+1 != [GameManager sharedGameManager].targetNum2 && i+1 != [GameManager sharedGameManager].targetNum3 && i+1 != [GameManager sharedGameManager].targetNum4) {// && i+1 != [GameManager sharedGameManager].targetNum5 && i+1 != [GameManager sharedGameManager].targetNum6 && i+1 != [GameManager sharedGameManager].targetNum7) {
                continue;
            } 
            int index=0;
            if (i+1 == [GameManager sharedGameManager].targetNum1) {
                index = 1;
            }
            if (i+1 == [GameManager sharedGameManager].targetNum2) {
                index = 2;
            }
            if (i+1 == [GameManager sharedGameManager].targetNum3) {
                index = 3;
            }
            if (i+1 == [GameManager sharedGameManager].targetNum4) {
                index = 4;
            }
//        if (i+1 == [GameManager sharedGameManager].targetNum5) {
//            index = 5;
//        }
//        if (i+1 == [GameManager sharedGameManager].targetNum6) {
//            index = 6;
//        }
//        if (i+1 == [GameManager sharedGameManager].targetNum7) {
//            index = 7;
//        }
            target = [m_targetArray objectAtIndex:i];
            if (target.visible == NO)
                continue;
            int diff = target.m_road_width * 2;//target.m_bLeftSide ? 0 : 20;
            targetRect = CGRectMake(
                                    (target.position.x - (target.m_Sprite.contentSize.width/8 * target.scale)),
                                    (target.position.y + diff),//kgh_06_23
                                    (target.m_Sprite.contentSize.width/4 * target.scale), 
                                    (target.m_Sprite.contentSize.height/4 * target.scale));
            if (CGRectIntersectsRect(busRect, targetRect)) { 
                _rootViewController.m_bBusTargetHit = YES;
                m_BusSpeed = 0;  
                m_BusSpeedFloat = 0.0f;
                //m_CollideCountLeft = m_CollideCountRight = 0;
                //NSLog(@"hit %@, %@ angle:%.2f", NSStringFromCGRect(busRect), NSStringFromCGRect(targetRect), m_Bus_angle);
                target.m_bBusHit = YES;
                target.m_distance -= 10; 
                [self showTargets:m_runDistance];
                //srand(time(nil) % 256);
                if (rand() % 2 == 0) {//kill                
                    if (index == 1) {
                        [GameManager sharedGameManager].m_target1HitNum = 3;
                        _rootViewController.target1ImageX.hidden = NO;
                    }
                    if (index == 2) {
                        [GameManager sharedGameManager].m_target2HitNum = 3;
                        _rootViewController.target2ImageX.hidden = NO;
                    }
                    if (index == 3) {
                        [GameManager sharedGameManager].m_target3HitNum = 3;
                        _rootViewController.target3ImageX.hidden = NO;
                    }
                    if (index == 4) {
                        [GameManager sharedGameManager].m_target4HitNum = 3;
                        _rootViewController.target4ImageX.hidden = NO;
                    }
//                if (index == 5) {
//                    [GameManager sharedGameManager].m_target5HitNum = 3;
//                    _rootViewController.target5ImageX.hidden = NO;
//                }
//                if (index == 6) {
//                    [GameManager sharedGameManager].m_target6HitNum = 3;
//                    _rootViewController.target6ImageX.hidden = NO;
//                }
//                if (index == 7) {
//                    [GameManager sharedGameManager].m_target7HitNum = 3;
//                    _rootViewController.target7ImageX.hidden = NO;
//                }
//                [GameManager sharedGameManager].m_GameScore += 100;
//                [_rootViewController launchCutSceenPlayer:3];
//                [GameManager sharedGameManager].m_currentLevelTargetNum--;
                    bHit = YES;
                    bKill = YES;
                    //kill movie
                } else {
                    
                    if (m_levelNum == 6 || m_levelNum == 7) {
                        bKill = YES;
                        bHit = YES;
                        if (index == 1) {
                            [GameManager sharedGameManager].m_target1HitNum = 3;
                            _rootViewController.target1ImageX.hidden = NO;
                        }
                        if (index == 2) {
                            [GameManager sharedGameManager].m_target2HitNum = 3;
                            _rootViewController.target2ImageX.hidden = NO;
                        }
                        if (index == 3) {
                            [GameManager sharedGameManager].m_target3HitNum = 3;
                            _rootViewController.target3ImageX.hidden = NO;
                        }
                        if (index == 4) {
                            [GameManager sharedGameManager].m_target4HitNum = 3;
                            _rootViewController.target4ImageX.hidden = NO;
                        }
//                    if (index == 5) {
//                        [GameManager sharedGameManager].m_target5HitNum = 3;
//                        _rootViewController.target5ImageX.hidden = NO;
//                    }
//                    if (index == 6) {
//                        [GameManager sharedGameManager].m_target6HitNum = 3;
//                        _rootViewController.target6ImageX.hidden = NO;
//                    }
//                    if (index == 7) {
//                        [GameManager sharedGameManager].m_target7HitNum = 3;
//                        _rootViewController.target7ImageX.hidden = NO;
//                    }
                    }
                    else {
                    
                        bKill = NO;
                        switch (index) {
                            case 1:
                                [GameManager sharedGameManager].m_target1HitNum++;
                                if ([GameManager sharedGameManager].m_target1HitNum > 2) {
                                    _rootViewController.target1ImageX.hidden = NO;
                                    bKill = YES;
                                }
                                break;
                            case 2:
                                [GameManager sharedGameManager].m_target2HitNum++;
                                if ([GameManager sharedGameManager].m_target2HitNum > 2) {
                                    _rootViewController.target2ImageX.hidden = NO;
                                    bKill = YES;
                                }
                                break;
                            case 3:
                                [GameManager sharedGameManager].m_target3HitNum++;
                                if ([GameManager sharedGameManager].m_target3HitNum > 2) {
                                    _rootViewController.target3ImageX.hidden = NO;
                                    bKill = YES;
                                }
                            case 4:
                                [GameManager sharedGameManager].m_target4HitNum++;
                                if ([GameManager sharedGameManager].m_target4HitNum > 2) {
                                    _rootViewController.target4ImageX.hidden = NO;
                                    bKill = YES;
                                }
                                break;
//                        case 5:
//                            if (!bKill && m_levelNum == 7) {
//                                [GameManager sharedGameManager].m_target5HitNum++;
//                            }
//                            if ([GameManager sharedGameManager].m_target5HitNum == 3) {
//                                bKill = YES;
//                            }
//                        case 6:
//                            if (!bKill && m_levelNum == 7) {
//                                [GameManager sharedGameManager].m_target6HitNum++;
//                            }
//                            if ([GameManager sharedGameManager].m_target6HitNum == 3) {
//                                bKill = YES;
//                            }
//                    case 7:
//                        [GameManager sharedGameManager].m_target7HitNum++;
//                        if ([GameManager sharedGameManager].m_target7HitNum == 3) {
//                            bKill = YES;
//                        }
//                        break;
                            default:
                                break;
                        }
                    }
                    if (bKill) {
//                    [GameManager sharedGameManager].m_GameScore += 100;
//                    [_rootViewController launchCutSceenPlayer:3];
//                    [GameManager sharedGameManager].m_currentLevelTargetNum--;
                        bHit = YES;
                        bKill = YES;
                        //kill movie
                    } else {
//                    if (rand() % 2 == 0) {//legs = 80
//                        [GameManager sharedGameManager].m_GameScore += 80;
//                    } else {//legs = 70
//                        [GameManager sharedGameManager].m_GameScore += 70;
//                    } 
//                    [_rootViewController launchCutSceenPlayer:2];
                        bHit = YES;
                        bKill = NO;
                        target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 700;
                        target.m_bBusHit = NO;
                        
                    }
                    
                }           
            }
        }
    }
    if (bHit) {
        CGPoint pos = [self getCollectPos:busRect targetRect:targetRect];
        CCSprite *bangsSprite = [CCSprite spriteWithFile:@"target_BANGs.png"];
        [self addChild:bangsSprite z:tagBus + 1];
        bangsSprite.scale = 0.1;
        bangsSprite.position = pos;
        id s = [CCEaseBounceOut actionWithAction:[CCScaleTo actionWithDuration:1 scale:2]];
        id d = [CCDelayTime actionWithDuration:.3];
        id f = [CCFadeOut actionWithDuration:.5];
        id videoPlay = nil;
        id levelEndPlay = nil;
        if (bKill) {
            m_ShowStatus = 1;
            if (m_levelNum == 8 && [GameManager sharedGameManager].m_currentLevelTargetNum == 1) {
                levelEndPlay = [CCCallFuncN actionWithTarget:self selector:@selector(videoPlayFunEnd)];
            } else
                videoPlay = [CCCallFuncN actionWithTarget:self selector:@selector(videoPlayFunNoKill)];
            // levelEndPlay = videoPlay;
        } else {
            m_ShowStatus = rand() % 5 + 2;
            videoPlay = [CCCallFuncN actionWithTarget:self selector:@selector(videoPlayFunNoKill)];
        }
        if (m_levelNum == 8 && bKill && [GameManager sharedGameManager].m_currentLevelTargetNum == 1)
            [bangsSprite runAction:[CCSequence actions:s, d, f, levelEndPlay, nil]];
        else
            [bangsSprite runAction:[CCSequence actions:s, d, f, videoPlay, nil]];
    }
    //check success
    if ([GameManager sharedGameManager].m_currentLevelTargetNum == 0 && (m_levelNum != 6 && m_levelNum != 7)) {//success
        [_rootViewController challenge];
        m_menuSuccess.visible = YES;
        _rootViewController.m_bStop = YES;
        
        if (m_levelNum+1 > [[NSUserDefaults standardUserDefaults] integerForKey:@"unlockLevel"]) {
            
            [[NSUserDefaults standardUserDefaults] setInteger:m_levelNum+1 forKey:@"unlockLevel"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
        
//        if ([GameManager sharedGameManager].m_currentPassedLevel < 4)
//            [GameManager sharedGameManager].m_currentPassedLevel = m_levelNum;
//        [[GameManager sharedGameManager] saveCompleteLevel];
    }
}

- (void) videoPlayFunEnd
{
    [GameManager sharedGameManager].m_GameScore += 100;
    [_rootViewController launchSecuensesCutSceen];
    [GameManager sharedGameManager].m_currentLevelTargetNum--;
}

- (void) videoPlayFunKill
{
    [GameManager sharedGameManager].m_GameScore += 100;
    [_rootViewController launchCutSceenPlayer:3];
    [GameManager sharedGameManager].m_currentLevelTargetNum--;
}
//- (void) videoPlayFunNoKill
//{
//    if (rand() % 2 == 0) {//legs = 80
//        [GameManager sharedGameManager].m_GameScore += 80;
//    } else {//legs = 70
//        [GameManager sharedGameManager].m_GameScore += 70;
//    } 
//    [_rootViewController launchCutSceenPlayer:2];
//}
- (void) videoPlayFunNoKill
{
    if (m_ShowStatus == 1) {
        [GameManager sharedGameManager].m_GameScore += 100;
        [GameManager sharedGameManager].m_currentLevelTargetNum--;
    } else {
        if (rand() % 2 == 0) {//legs = 80
            [GameManager sharedGameManager].m_GameScore += 80;
        } else {//legs = 70
            [GameManager sharedGameManager].m_GameScore += 70;
        }        
    }
    [_rootViewController launchCutSceenPlayer:2];
}

- (void) setTargetPassengerDistance
{
    //level1
    if ([[GameManager sharedGameManager] isSelectAll]) {
        m_targetArrChooseForAll = [[NSMutableArray alloc] init];
        switch ([GameManager sharedGameManager].m_levelNum) {
            case 1:
            {
                for (int i = 0; i < 2; i++) {
                    
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = [GameManager sharedGameManager].targetNum1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArrChooseForAll addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i == 0) {
                        target.m_distance = 700;
                    }
                    if (i == 1) {
                        target.m_distance = 1400;
                    }
                }
            }
                break;
            case 2:
                for (int i = 0; i < 2; i++)
                {
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = [GameManager sharedGameManager].targetNum1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArrChooseForAll addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i == 0) {
                        target.m_distance = 700;
                    }
                    if (i == 1) {
                        target.m_distance = 1400;
                    }
                    
                }
                break;
            case 3:
                for (int i = 0; i < 3; i++)
                {
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = [GameManager sharedGameManager].targetNum1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArrChooseForAll addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i == 0) {
                        target.m_distance = 700;
                    }
                    if (i == 1) {
                        target.m_distance = 1400;
                    }
                    if (i == 2) {
                        target.m_distance = 2100;
                    }
                    
                }
                break;
            case 4:
                for (int i = 0; i < 2; i++)
                {
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = [GameManager sharedGameManager].targetNum1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArrChooseForAll addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i == 0) {
                        target.m_distance = 700;
                    }
                    if (i == 1) {
                        target.m_distance = 1400;
                    }
                    
                }
                break;
            case 5:
                for (int i = 0; i < 2; i++)
                {
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = [GameManager sharedGameManager].targetNum1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArrChooseForAll addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i == 0) {
                        target.m_distance = 700;
                    }
                    if (i == 1) {
                        target.m_distance = 1400;
                    }
                    
                }
                break;
            case 6:
                for (int i = 0; i < 4; i++)
                {
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = [GameManager sharedGameManager].targetNum1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArrChooseForAll addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i == 0) {
                        target.m_distance = 700;
                    }
                    if (i == 1) {
                        target.m_distance = 1400;
                    }
                    if (i == 2) {
                        target.m_distance = 2100;
                    }
                    if (i == 3) {
                        target.m_distance = 2800;
                    }
                }
                break;
            case 7:
                for (int i = 0; i < 4; i++)
                {
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = [GameManager sharedGameManager].targetNum1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArrChooseForAll addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i == 0) {
                        target.m_distance = 700;
                    }
                    if (i == 1) {
                        target.m_distance = 1400;
                    }
                    if (i == 2) {
                        target.m_distance = 2100;
                    }
                    if (i == 3) {
                        target.m_distance = 2800;
                    }
//                if (i+1 == [GameManager sharedGameManager].targetNum5) {
//                    target.m_distance = 3500;
//                }
//                if (i+1 == [GameManager sharedGameManager].targetNum6) {
//                    target.m_distance = 4200;
//                }
                    
                }
                break;
            case 8:
                for (int i = 0; i < 2; i++)
                {
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = [GameManager sharedGameManager].targetNum1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArrChooseForAll addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i == 0) {
                        target.m_distance = 800;
                    }
                    if (i == 1) {
                        target.m_distance = 1600;
                    }
                }
                break;
            default:
                break;
        }
        
    }
    else {
        m_targetArray = [[NSMutableArray alloc] init];
        switch ([GameManager sharedGameManager].m_levelNum) {
            case 1:
                for (int i = 0; i < 29; i++)
                {                   
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = i + 1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArray addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i+1 == [GameManager sharedGameManager].targetNum1) {
                        target.m_distance = 700;
                    }
                    if (i+1 == [GameManager sharedGameManager].targetNum2) {
                        target.m_distance = 1400;
                    }          
                    
                }            
                break;
            case 2:    
                for (int i = 0; i < 29; i++)
                {   
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = i + 1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArray addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i+1 == [GameManager sharedGameManager].targetNum1) {
                        target.m_distance = 700;
                    }
                    if (i+1 == [GameManager sharedGameManager].targetNum2) {
                        target.m_distance = 1400;
                    }          
                    
                }
                break; 
            case 3:            
                for (int i = 0; i < 29; i++)
                {  
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = i + 1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArray addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i+1 == [GameManager sharedGameManager].targetNum1) {
                        target.m_distance = 700;
                    }
                    if (i+1 == [GameManager sharedGameManager].targetNum2) {
                        target.m_distance = 1400;
                    }   
                    if (i+1 == [GameManager sharedGameManager].targetNum3) {
                        target.m_distance = 2100;
                    } 
                    
                }
                break;
            case 4:            
                for (int i = 0; i < 29; i++)
                {
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = i + 1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArray addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i+1 == [GameManager sharedGameManager].targetNum1) {
                        target.m_distance = 700;
                    }
                    if (i+1 == [GameManager sharedGameManager].targetNum2) {
                        target.m_distance = 1400;
                    }          
                    
                }
                break;
            case 5:   
                for (int i = 0; i < 29; i++)
                {   
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = i + 1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArray addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i+1 == [GameManager sharedGameManager].targetNum1) {
                        target.m_distance = 700;
                    }
                    if (i+1 == [GameManager sharedGameManager].targetNum2) {
                        target.m_distance = 1400;
                    }          
                    
                }
                break;
            case 6:          
                for (int i = 0; i < 29; i++)
                {
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = i + 1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArray addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i+1 == [GameManager sharedGameManager].targetNum1) {
                        target.m_distance = 700;
                    }
                    if (i+1 == [GameManager sharedGameManager].targetNum2) {
                        target.m_distance = 1400;
                    }   
                    if (i+1 == [GameManager sharedGameManager].targetNum3) {
                        target.m_distance = 2100;
                    } 
                    if (i+1 == [GameManager sharedGameManager].targetNum4) {
                        target.m_distance = 2800;
                    }
                }
                break;
            case 7:
                for (int i = 0; i < 29; i++)
                {
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = i + 1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArray addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i+1 == [GameManager sharedGameManager].targetNum1) {
                        target.m_distance = 700;
                    }
                    if (i+1 == [GameManager sharedGameManager].targetNum2) {
                        target.m_distance = 1400;
                    }
                    if (i+1 == [GameManager sharedGameManager].targetNum3) {
                        target.m_distance = 2100;
                    }
                    if (i+1 == [GameManager sharedGameManager].targetNum4) {
                        target.m_distance = 2800;
                    }
    //                if (i+1 == [GameManager sharedGameManager].targetNum5) {
    //                    target.m_distance = 3500;
    //                }
    //                if (i+1 == [GameManager sharedGameManager].targetNum6) {
    //                    target.m_distance = 4200;
    //                }
                    
                }
                break;
            case 8: 
                for (int i = 0; i < 29; i++)
                { 
                    CTarget *target = [[[CTarget alloc] init] autorelease];
                    target.m_selectNum = i + 1;
                    [target setAnimWalk];
                    target.m_distance = 0;
                    target.m_road_width = rand() % 7;
                    target.visible = NO;
                    [m_targetArray addObject:target];
                    
                    [self addChild:target z:tagTarget + 30 - i];
                    
                    if (i+1 == [GameManager sharedGameManager].targetNum1) {
                        target.m_distance = 800;
                    }
                    if (i+1 == [GameManager sharedGameManager].targetNum2) {
                        target.m_distance = 1600;
                    }
                    
                }
                break;
            default:
                break;
        }
    }
    
}

- (void)missedTargetInLevel6And7 {
    m_menuGameOver.visible = YES;
    _rootViewController.m_bStop = YES;
    m_menuMissedTarget.visible = YES;
    
}

- (void) lifeNoHitTargetsReplace
{
    if ([[GameManager sharedGameManager] isSelectAll]) {
        for (int i = 0; i < [m_targetArrChooseForAll count]; i++)
        {
            CTarget *target;
            target = [m_targetArrChooseForAll objectAtIndex:i];
            
            if (target.m_distance > 0 && target.m_distance < m_loopNum*1200+m_runDistance && !target.m_bBusHit) {
                
                if (m_levelNum == 6 || m_levelNum == 7) {
                    [self missedTargetInLevel6And7];
                }
                
                target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
                if (m_levelNum == 2 || m_levelNum == 6) {
                    target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
                }
                target.m_road_width = rand() % 7;
                m_MissT++;
            }
            
        }
    }
    else {
        for (int i = 0; i < 29; i++)
        { 
            CTarget *target;
            target = [m_targetArray objectAtIndex:i];
            
            if (i+1 == [GameManager sharedGameManager].targetNum1) {
                if (target.m_distance > 0 && target.m_distance < m_loopNum*1200+m_runDistance && !target.m_bBusHit) {
                    
                    if (m_levelNum == 6 || m_levelNum == 7) {
                        [self missedTargetInLevel6And7];
                    }
                    
                    target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
                    if (m_levelNum == 2 || m_levelNum == 6) {
                        target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
                    }
                    target.m_road_width = rand() % 7;
                    m_MissT++;
                }
            }
            if (i+1 == [GameManager sharedGameManager].targetNum2) {
                if (target.m_distance > 0 && target.m_distance < m_loopNum*1200+m_runDistance && !target.m_bBusHit) {
                    
                    if (m_levelNum == 6 || m_levelNum == 7) {
                        [self missedTargetInLevel6And7];
                    }
                    
                    target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
                    if (m_levelNum == 2 || m_levelNum == 6) {
                        target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
                    }
                    target.m_road_width = rand() % 7;
                    m_MissT++;
                }
            }  
            if (i+2 == [GameManager sharedGameManager].targetNum3) {
                if (target.m_distance > 0 && target.m_distance < m_loopNum*1200+m_runDistance && !target.m_bBusHit) {
                    
                    if (m_levelNum == 6 || m_levelNum == 7) {
                        [self missedTargetInLevel6And7];
                    }
                    
                    target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
                    if (m_levelNum == 2 || m_levelNum == 6) {
                        target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
                    }
                    target.m_road_width = rand() % 7;
                    m_MissT++;
                }
            }
            if (i+2 == [GameManager sharedGameManager].targetNum4) {
                if (target.m_distance > 0 && target.m_distance < m_loopNum*1200+m_runDistance && !target.m_bBusHit) {
                    
                    if (m_levelNum == 6 || m_levelNum == 7) {
                       [self missedTargetInLevel6And7];
                    }
                    
                    target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
                    if (m_levelNum == 2 || m_levelNum == 6) {
                        target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
                    }
                    target.m_road_width = rand() % 7;
                    m_MissT++;
                }
            }
//        if (i+2 == [GameManager sharedGameManager].targetNum5) {
//            if (target.m_distance > 0 && target.m_distance < m_loopNum*1200+m_runDistance && !target.m_bBusHit) {
//                
//                if (m_levelNum == 6 || m_levelNum == 7) {
//                    m_menuGameOver.visible = YES;
//                    _rootViewController.m_bStop = YES;
//                }
//                
//                target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
//                if (m_levelNum == 2 || m_levelNum == 6) {
//                    target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
//                }
//                target.m_road_width = rand() % 7;
//                m_MissT++;
//            }
//        }
//        if (i+2 == [GameManager sharedGameManager].targetNum6) {
//            if (target.m_distance > 0 && target.m_distance < m_loopNum*1200+m_runDistance && !target.m_bBusHit) {
//                
//                if (m_levelNum == 6 || m_levelNum == 7) {
//                    m_menuGameOver.visible = YES;
//                    _rootViewController.m_bStop = YES;
//                }
//                
//                target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
//                if (m_levelNum == 2 || m_levelNum == 6) {
//                    target.m_distance += [GameManager sharedGameManager].m_currentLevelTargetNum * 800;
//                }
//                target.m_road_width = rand() % 7;
//                m_MissT++;
//            }
//        }
            
        }
    }
    if (m_MissT >= 2) {
        //m_menuGameOver.visible = YES;
        //_rootViewController.m_bStop = YES;
        //m_MissT = 0;
    }
    
}
- (void) btnGameOverClick
{
    [self replayClick];
    m_menuGameOver.visible = NO;
    m_menuMissedTarget.visible = NO;
    _rootViewController.m_bStop = NO;
    //[[CCDirector sharedDirector] stopAnimation];
    [[CCDirector sharedDirector] replaceScene:[CCTransitionFade transitionWithDuration:.7 scene:[MainLayer_iPad scene:_rootViewController] withColor:ccBLACK]];
    
    CTarget *target;
    for (int i = 0; i < [m_targetArray count]; i++) {
        target = [m_targetArray objectAtIndex:i];
        target.m_bBusHit = NO;
    }
    for (int i = 0; i < [m_targetArrChooseForAll count]; i++) {
        target = [m_targetArrChooseForAll objectAtIndex:i];
        target.m_bBusHit = NO;
    }
    
    switch ([GameManager sharedGameManager].m_levelNum) {
        case 1:
        case 2:
        case 4:
        case 5:
        case 8:
            [GameManager sharedGameManager].m_currentLevelTargetNum = 2;
            break;
        case 3:
            [GameManager sharedGameManager].m_currentLevelTargetNum = 3;
            break;
        case 6:
            [GameManager sharedGameManager].m_currentLevelTargetNum = 4;
            break;
        case 7:
            [GameManager sharedGameManager].m_currentLevelTargetNum = 4;
            break;
        default:
            break;
    }
    [GameManager sharedGameManager].m_GameScore = 0;
}
- (void) yesFaceBookClick
{
    [self unschedule:@selector(updateGamePlay:)];
    [_rootViewController facebookLogin];
    [_rootViewController levelViewControllerToBack];
}
- (void) noFaceBookClick
{
    [self unschedule:@selector(updateGamePlay:)];
    [_rootViewController levelViewControllerToBack];
}
//- (void) btnSuccessClick
//{
//    [_rootViewController facebookLogin];
//    [_rootViewController levelViewControllerToBack];
//}
- (void) bus_break
{
    m_BusSpeed = 0;
    m_BusSpeedFloat = 0.0;
    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Bus_braking.mp3"];
    }
}
- (void) setTime
{
    m_bTimeClock = NO;
    m_labelTime.visible = NO;
    switch ([GameManager sharedGameManager].m_levelNum) {
        case 1://Time Clock:NO;
            m_Time = 50;
            break;
        case 2://Time Clock:NO;
            m_Time = 80;
            break;
        case 3://Time:YES(clocked)
            m_Time = 90;
            m_labelTime.visible = YES;
            m_bTimeClock = YES;
            break;
        case 4://Time Clock:NO;            
            m_Time = 90;
            break;
        case 5://Time:YES(clocked)
            m_Time = 40;
            m_labelTime.visible = YES;
            m_bTimeClock = YES;
            break;
        case 6://Time Clock:NO; 
            m_Time = 80;
            break; 
        case 7://Time:YES(clocked)
            m_Time = 60;
//            m_labelTime.visible = YES;
//            m_bTimeClock = YES;
            break;
        case 8://Time Clock:NO; 
            m_Time = 30;
            break;
        default:
            break;   
    }
}
#pragma mark - accelerometer
#define kPlayerSpeed 100
- (void) accelerometer:(UIAccelerometer *)accelerometer didAccelerate:(UIAcceleration *)acceleration {
    // use the running scene to grab the appropriate game layer by it's tag
    float limit = 0.25;//0.25;
    float destX;
    if(acceleration.x > limit) {  // tilting the device upwards
        destX = (acceleration.y * kPlayerSpeed);
    } else if (acceleration.x < -limit) {  // tilting the device downwards                
        destX = (acceleration.y * kPlayerSpeed);
    } else if(acceleration.y < -limit) {  // tilting the device to the right                
        destX = (acceleration.y * kPlayerSpeed);
    } else if (acceleration.y > limit) {  // tilting the device to the left
        destX = (acceleration.y * kPlayerSpeed);
    } else {
        destX = 0;
    }    
    m_steering_angle = destX;
    //NSLog(@"%.2f", destX);
}

- (void) connectToServer
{
    NSString *levelImage = [NSString stringWithFormat:@"http://www.twoservices.net/sites/default/files/images/Game Development/iPhone&iPad Game/level%d.png", m_levelNum];
    NSString *userImage;
    if ([GameManager sharedGameManager].m_bMale) {
        userImage = @"http://www.twoservices.net/sites/default/files/images/Game Development/iPhone&iPad Game/bus driver_male_correction_render.png";
    } else {
        userImage = @"http://www.twoservices.net/sites/default/files/images/Game Development/iPhone&iPad Game/bus driver_female_render.png";
    }
    
    UIDevice *myDevice = [UIDevice currentDevice];
    NSString *deviceUDID = [myDevice uniqueIdentifier];
    
    NSString *smsURL = @"http://developer.avenuesocial.com/azeemsal/busrage/application/json/score.php";
    NSMutableURLRequest *request = [[[NSMutableURLRequest alloc] init] autorelease];
    
    NSString *post = [NSString stringWithFormat:@"user_id=%@&level_img=%@&level=%d&score=%d&user_image=%@&user_name=%@", deviceUDID, levelImage, m_levelNum, [GameManager sharedGameManager].m_GameScore, userImage, [GameManager sharedGameManager].m_DriverName];
    NSLog(@"%@", post);
    NSData *postData = [post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:YES];
    NSString *postLength = [NSString stringWithFormat:@"%d", [postData length]];
    
    [request setURL:[NSURL URLWithString:smsURL]];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"Mozilla/4.0(compatible;)" forHTTPHeaderField:@"User-Agent"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    [NSURLConnection connectionWithRequest:request delegate:self];
}
- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
//    NSString *returnString = [[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding] autorelease];
//    NSLog(@"%@", returnString);
}
//- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
//{
//    NSHTTPCookie *cookie;
//    //int i = 0;
//    for (cookie in [[NSHTTPCookieStorage sharedHTTPCookieStorage]  cookies])
//    {
//        NSLog(@"%@", [cookie description]);
//    }
//}

@end
