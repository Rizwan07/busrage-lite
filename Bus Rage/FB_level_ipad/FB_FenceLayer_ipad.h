//
//  FB_FenceLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface FB_FenceLayer_ipad : CCLayer {
    CCSprite *roadFence1;
    CCSprite *roadFence2;
    CCSprite *roadFence3;
    CCSprite *roadFence4;
    int current_fence;
    int runCount;
    
    CCSprite *bush1Sprite_R;
    CCSprite *bush2Sprite_R;
    CCSprite *boardSprite;
    
    int m_Bush1_R[3];
    int m_Bush2_R[3];
    int m_Board[3];
    NSArray*    m_Objects;
    
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) showFence:(int)speed;
- (void) changeFenceSprite;

- (void) showBushes1_R:(int)runDistance;
- (void) showBushes2_R:(int)runDistance;
- (void) showBoard:(int)runDistance;

@end
