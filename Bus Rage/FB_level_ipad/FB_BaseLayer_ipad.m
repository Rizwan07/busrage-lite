//
//  FB_BaseLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import "FB_BaseLayer_ipad.h"


@implementation FB_BaseLayer_ipad
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        m_groundSprite = [CCSprite spriteWithFile:@"ground_FB_ipad.png"];
        m_groundSprite.position = ccp(480*2.1333333, 160*2.4);
        m_groundSprite.scale = 4;
        [self addChild:m_groundSprite z:0];
        
        CCSprite *skySprite = [CCSprite spriteWithFile:@"ground_sky_ipad.png"];
        skySprite.position = ccp(240*2.1333333, 235*2.4);
        skySprite.scale = 2;
        [self addChild:skySprite z:1];
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    CGPoint pos = m_groundSprite.position;
    
    if (speed > 0) {
        pos.y -= speed;
        pos.x -= speed*2;
        if (pos.y < -10 || pos.x < 0) {
            pos.y = 160*2.4; pos.x = 480*2.1333333;
        }
        m_groundSprite.position = pos;
    }
}

@end
