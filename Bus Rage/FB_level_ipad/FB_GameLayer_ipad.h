//
//  FB_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "FB_HousesLayer_ipad.h"
#import "FB_FenceLayer_ipad.h"
#import "FB_RoadLayer_ipad.h"
#import "FB_TreesLayer_ipad.h"
#import "FB_HillsLayer_ipad.h"
#import "FB_Horizon1Layer_ipad.h"
#import "FB_BaseLayer_ipad.h"
@class MainLayer_iPad;

@interface FB_GameLayer_ipad : CCLayer {
    FB_HousesLayer_ipad *fb_houseLayer;
    FB_FenceLayer_ipad *fb_fenceLayer;
    FB_RoadLayer_ipad *fb_roadLayer;
    FB_TreesLayer_ipad *fb_treeLayer;
    FB_HillsLayer_ipad *fb_hillLayer;
    FB_Horizon1Layer_ipad *fb_horizon1Layer;
    FB_BaseLayer_ipad *fb_baseLayer;
    CCSprite *cloud_dustSprite;
    MainLayer_iPad * mainLayer;
    int m_timecount;
}

- (void) runMove: (int)runDistance speed:(int)speed;
- (void) setMainLayer:(MainLayer_iPad*) mainLayer;


@end
