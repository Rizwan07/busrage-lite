//
//  SV_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SV_HousesLayer.h"
#import "SV_FenceLayer.h"
#import "SV_RoadLayer.h"
#import "SV_TreesLayer.h"
#import "SV_HillsLayer.h"
#import "SV_Horizon1Layer.h"
#import "SV_BaseLayer.h"
@class MainLayer;

@interface SV_GameLayer : CCLayer {
    SV_HousesLayer *sv_houseLayer;
    SV_FenceLayer *sv_fenceLayer;
    SV_RoadLayer *sv_roadLayer;
    SV_TreesLayer *sv_treeLayer;
    SV_HillsLayer *sv_hillLayer;
    SV_Horizon1Layer *sv_horizon1Layer;
    SV_BaseLayer *sv_baseLayer;
    CCSprite *cloud_dustSprite;
    MainLayer * mainLayer;
    int m_timecount;
    
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) setMainLayer:(MainLayer*) mainLayer;
@end
