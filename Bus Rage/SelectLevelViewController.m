//
//  SelectLevelViewController.m
//  Bus Rage
//
//  Created by Jin Tie on 5/9/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "SelectLevelViewController.h"
#import <mach/mach.h>
#import "AppDelegate.h"
#import "GameManager.h"
#import "MainLayer.h"

@implementation SelectLevelViewController

@synthesize selecttargetViewController = _selecttargetViewController;
@synthesize scrollView;
@synthesize imageView;
@synthesize textView1;
@synthesize textView2;

- (void) print_free_memory 
{//kgh
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);
    
    vm_statistics_data_t vm_stat;
    
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
        NSLog(@"Failed to fetch vm statistics");
    
    /* Stats in bytes */
    natural_t mem_used = (vm_stat.active_count + vm_stat.inactive_count + vm_stat.wire_count) * pagesize;
    natural_t mem_free = vm_stat.free_count * pagesize;
    natural_t mem_total = mem_used + mem_free;
    int iUsed = round(mem_used/100000);
    int iFree = round(mem_free/100000);
    int iTotal = round(mem_total/100000);
    NSLog(@"used: %d free: %d total: %d", iUsed, iFree, iTotal);    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

#pragma mark - View lifecycle

- (void)viewDidLoad
{    
    
    [super viewDidLoad];
    
    if (![[NSUserDefaults standardUserDefaults] integerForKey:@"unlockLevel"]) {
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"unlockLevel"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    // Do any additional setup after loading the view from its nib.
    m_LevelButtonArray = [[NSMutableArray alloc] init];
    int nLevel = 3;
    
    BOOL isIphone = [[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone;
    
    if (isIphone) {
        [scrollView setContentSize:CGSizeMake(86, nLevel * 80)];
    }
    else {
        [scrollView setContentSize:CGSizeMake(160, nLevel * 130)];
    }
    
    // 148, 100
    
    [GameManager sharedGameManager].m_levelNum = 1;//defaut level
    UIImage *disableImage = [UIImage imageNamed:@"level_lock.png"];
    for (int i = 0; i < nLevel; i++) {
        
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"level%i.png", i + 1]];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        if (isIphone) {
            [button setFrame:CGRectMake(6, 30 + i * 80, 74, 50)];
            //button = [[UIButton alloc] initWithFrame:CGRectMake(6, 30 + i * 80, 74, 50)];
        }
        else {
            [button setFrame:CGRectMake(6, 30 + i * 130, 148, 100)];
            //button = [[UIButton alloc] initWithFrame:CGRectMake(6, 30 + i * 130, 148, 100)];
        }
        [button addTarget:self action:@selector(picClicked:) forControlEvents:UIControlEventTouchUpInside];
        [button setImage:image forState:UIControlStateNormal];
        [button setImage:disableImage forState:UIControlStateDisabled];
        [button setBackgroundColor:[UIColor clearColor]];
        [button setBackgroundImage:[UIImage imageNamed:@"thumbnail_select_BG.png"] forState:UIControlStateNormal];
        //[button set
        if (i == 0) {
            [button setBackgroundImage:[UIImage imageNamed:@"thumbnail_cur_select_BG.png"] forState:UIControlStateNormal];
        }
        //[button setFrame:CGRectMake(11, 8+i*80, 53, 65)];        
        button.tag = i+1;
        [scrollView addSubview:button];
        [m_LevelButtonArray addObject:button];
        //[button release];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    //read file        
    //[[GameManager sharedGameManager] loadCompleteLevel];
    
    int level = [[NSUserDefaults standardUserDefaults] integerForKey:@"unlockLevel"];
    
    int nLevel = 3;
    UIButton *button;
    for (int i = 0; i < nLevel; i++) {
        button = [m_LevelButtonArray objectAtIndex:i];
        button.enabled = YES;
        //if (i > [GameManager sharedGameManager].m_currentPassedLevel) {
        if (i+1 > level) {
            button.enabled = NO;
        }
        
    }
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [m_LevelButtonArray removeAllObjects];
    [m_LevelButtonArray release]; m_LevelButtonArray = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backTapped:(id)sender {
    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Next_screen_swoosh.mp3"];
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)nextTapped:(id)sender {

    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Next_screen_swoosh.mp3"];
    }
//    if (_selecttargetViewController == nil) {
//        self.selecttargetViewController = [[[SelectTargetViewController alloc] initWithNibName:nil bundle:nil] autorelease];
//    }
    AppDelegate* mainDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
    //[self.navigationController pushViewController:_selecttargetViewController animated:YES];
    [self.navigationController pushViewController:mainDelegate.selecttargetViewController animated:YES];
    //NSLog(@"selectlevel_end");[self print_free_memory];
    
    //init
//    for (int i = 0; i < 29; i++)
//    {
//        [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray replaceObjectAtIndex:i withObject:@""];
//        [[GameManager sharedGameManager].m_TargetArray replaceObjectAtIndex:i withObject:@"0"];
//        [[GameManager sharedGameManager].m_TargetNameArray replaceObjectAtIndex:i withObject:@""];
//        
//        [[GameManager sharedGameManager].m_PassengerfacebookImageObjectIDArray replaceObjectAtIndex:i withObject:@""];
//        [[GameManager sharedGameManager].m_PassengerArray replaceObjectAtIndex:i withObject:@"0"];
//        [[GameManager sharedGameManager].m_PassengerNameArray replaceObjectAtIndex:i withObject:@""];
//    }
}
- (void) dealloc
{
    //[_rootViewController release];
    //_rootViewController = nil;
    [super dealloc];
}
- (void) picClicked:(id)sender {   
    
    UIButton* view = (UIButton*)sender;    
    imageView.image = [UIImage imageNamed:[NSString stringWithFormat:@"level%i.png", view.tag]];
    if ([GameManager sharedGameManager].m_levelNum != view.tag) {
        for (int i = 0; i < 29; i++)
        {
            [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray replaceObjectAtIndex:i withObject:@""];
            [[GameManager sharedGameManager].m_TargetArray replaceObjectAtIndex:i withObject:@"0"];
            [[GameManager sharedGameManager].m_TargetNameArray replaceObjectAtIndex:i withObject:@""];            
        }

        [GameManager sharedGameManager].targetNum1 = [GameManager sharedGameManager].targetNum2 = [GameManager sharedGameManager].targetNum3 =[GameManager sharedGameManager].targetNum4 = 0;//[GameManager sharedGameManager].targetNum5 =[GameManager sharedGameManager].targetNum6 =[GameManager sharedGameManager].targetNum7 = 0;
        
    }
    [GameManager sharedGameManager].m_levelNum = view.tag;
    int nLevel = 3;
    for (int i = 0; i < nLevel; i++) 
    {        
        UIButton* button = [m_LevelButtonArray objectAtIndex:i];
        [button setBackgroundImage:[UIImage imageNamed:@"thumbnail_select_BG.png"] forState:UIControlStateNormal];
        
    }
    [view setBackgroundImage:[UIImage imageNamed:@"thumbnail_cur_select_BG.png"] forState:UIControlStateNormal];
    switch (view.tag) {
        case 1:
            textView1.text = @"Level 1 \nCountryside";
            textView2.text = @"A Scenic and beautiful countryside drive offers an experience trough Lush Farm Fields and old wind mills."; 
            [GameManager sharedGameManager].m_targetNum = 2;
            break;
        case 2:
            textView1.text = @"Level 2 \nMountain View";
            textView2.text = @"A drive through the Northern Mountains, high hills and wide lakes greets you on this adventurous road. But Watch out for the rocks in your way!";
            [GameManager sharedGameManager].m_targetNum = 2;
            break;   
        case 3:
            textView1.text = @"Level 3 \nOffroad View";
            textView2.text = @"Sand Dunes and heat of the dessert is what awaits you in this scorching landscape, but worry not as the locals are rather “Friendly”";
            [GameManager sharedGameManager].m_targetNum = 3;
            break; 
        case 4:
            textView1.text = @"Level 4 \nStreet View";
            textView2.text = @"A drive on a suburban Road, Little traffic and nice sunny weather should make it quite a pleasing experience. Keep an eye out for the local hotdog stand, heard he has some mean hotdogs!";
            [GameManager sharedGameManager].m_targetNum = 2;
            break; 
        case 5:
            textView1.text = @"Level 5 \nSchool Side";
            textView2.text = @"A busy school street, Kids running around and teacher’s crossing over, you’ll have to be slow and careful on this one.";
            [GameManager sharedGameManager].m_targetNum = 2;
            break; 
        case 6:
            textView1.text = @"Level 6 \nStore Attack";
            textView2.text = @"That store guy really needs to learn a lesson, he is always selling cheap goods at high prices, Lets go give him a blow in the face!";
            [GameManager sharedGameManager].m_targetNum = 4;
            break; 
        case 7:
            textView1.text = @"Level 7 \nCar Dealer Attack";
            textView2.text = @"All you asked from that Car dealer was a new Bus but he didn’t only refuse, he kept all of your money too. Time for a little Payback!";
            [GameManager sharedGameManager].m_targetNum = 4;
            break; 
        case 8:
            textView1.text = @"Level 8 \nFerry Boat Attack";
            textView2.text = @"The cruise liner is ready to set sails, but they haven’t on-boarded the most important passenger -YOU. Charge Ahead at take your place on board!!!";
            [GameManager sharedGameManager].m_targetNum = 2;
            break; 
        default:
            break;
    }
    
}
@end
