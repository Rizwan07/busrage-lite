//
//  MV_BaseLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 5/26/12.
//  Copyright 2012 k. All rights reserved.
//

#import "MV_BaseLayer.h"


@implementation MV_BaseLayer
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        m_groundSprite = [CCSprite spriteWithFile:@"ground_MV.png"];
        m_groundSprite.position = ccp(480, 160);
        m_groundSprite.scale = 2;
        [self addChild:m_groundSprite z:0];
        
        CCSprite *skySprite = [CCSprite spriteWithFile:@"ground_sky.png"];
        skySprite.position = ccp(240, 235);
        skySprite.scale = 2;
        [self addChild:skySprite z:1];        
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{    
    CGPoint pos = m_groundSprite.position;
    
    if (speed > 0) {
        pos.y -= speed;
        pos.x -= speed*2;
        if (pos.y < -10 || pos.x < 0) {
            pos.y = 160; pos.x = 480;
        }
        m_groundSprite.position = pos;
    }
}
@end
