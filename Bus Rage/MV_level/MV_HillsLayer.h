//
//  MV_HillsLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface MV_HillsLayer : CCLayer {
    CCSprite *hill1Sprite;
    CCSprite *hill2Sprite;
    CCSprite *hill3Sprite;
}
- (void) runMove:(int)runDistance speed:(int)speed;
@end
