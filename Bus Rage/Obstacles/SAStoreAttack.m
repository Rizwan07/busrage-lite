//
//  SAStoreAttack.m
//  Bus Rage
//
//  Created by Jin Tie on 6/13/12.
//  Copyright 2012 k. All rights reserved.
//

#import "SAStoreAttack.h"


@implementation SAStoreAttack
//@synthesize m_Sprite;
@synthesize m_distance;
@synthesize m_bBusHit;
@synthesize m_road_width;

- (id) init {
    
    int num = 0;
    
//    self = [super init];
//    if (self) {
//        m_Sprite = [CCSprite spriteWithFile:@"Store Attack 1.png"];
//        
//        [self addChild:m_Sprite z:0];       
//    }
//    return self;
    
    self = [super init];
    if (self) {
        switch (num) {
            case 1:
                m_Sprite = [CCSprite spriteWithFile:@"Store Attack 1.png"];
                break;
            case 2:
                m_Sprite = [CCSprite spriteWithFile:@"Store Attack 2.png"];
                break; 
            case 3:
                m_Sprite = [CCSprite spriteWithFile:@"Store Attack 3.png"];
                break; 
            case 4:
                m_Sprite = [CCSprite spriteWithFile:@"Store Attack 4.png"];
                break; 
            default:
                m_Sprite = [CCSprite spriteWithFile:@"Store Attack 1.png"];
                break;
        }
        
        [self addChild:m_Sprite z:0];       
    }
    return self;    
}
- (void) dealloc
{       
    [super dealloc];
}
@end
