//
//  CRock.h
//  Bus Rage
//
//  Created by Jin Tie on 6/10/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CRock : CCSprite {
    CCSprite *m_Sprite;
    int m_distance;
    bool m_bBusHit;
    float m_road_width;//8 + 2 = 10m
}
//@property (nonatomic, retain) CCSprite *m_Sprite;
@property (assign) int m_distance;
@property (assign) bool m_bBusHit;
@property (assign) float m_road_width;

- (id) init:(int)num forIpad:(BOOL )isIpad;
@end
