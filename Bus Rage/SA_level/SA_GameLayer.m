//
//  SA_GameLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import "SA_GameLayer.h"
#import "MainLayer.h"

@implementation SA_GameLayer
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        CCSprite *horizon_fogSprite = [CCSprite spriteWithFile:@"horizon_fog_SA.png"];
        [self addChild:horizon_fogSprite z:tagRoad + 1];
        horizon_fogSprite.position = ccp(size.width/2, 150);
        
        CCSprite *horizon_farSprite = [CCSprite spriteWithFile:@"Horizon_far_SV.png"];
        [self addChild:horizon_farSprite z:tagHorizonFar];
        horizon_farSprite.position = ccp(size.width/2, 240);   
        
        cloud_dustSprite = [CCSprite spriteWithFile:@"Cloud_dust_horizon.png"];
        [self addChild:cloud_dustSprite z:tagHorizonFar + 3];
        cloud_dustSprite.position = ccp(-514, 220);
        
        sa_houseLayer = [SA_HousesLayer node];
        [self addChild:sa_houseLayer z:tagShops];        
        sa_fenceLayer = [SA_FenceLayer node];
        [self addChild:sa_fenceLayer z:tagFence];        
        sa_roadLayer = [SA_RoadLayer node];
        [self addChild:sa_roadLayer z:tagRoad];        
        sa_treeLayer = [SA_TreesLayer node];
        [self addChild:sa_treeLayer z:tagTrees];        
        sa_hillLayer = [SA_HillsLayer node];
        [self addChild:sa_hillLayer z:tagHills];        
        sa_horizon1Layer = [SA_Horizon1Layer node];        
        [self addChild:sa_horizon1Layer z:tagHorizon1];
        sa_baseLayer = [SA_BaseLayer node];
        [self addChild:sa_baseLayer z:tagBase];
        
        [self schedule:@selector(renderCloud:) interval: 10.0];
        [self schedule:@selector(renderBalloon:) interval: 70.0];  
        [self schedule:@selector(renderAirPlane:) interval:50.0];
        [self schedule:@selector(renderAirPlane:) interval:50.0];
        [self schedule:@selector(updateGamePlay:)];
	}
	return self;
}

- (void) renderAirPlane:(ccTime)dt {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    CCSprite *airPlaneSprite = [CCSprite spriteWithFile:@"Airplane_SA.png"];
    airPlaneSprite.scale = 0.4 + 0.01 * (arc4random()%10);
	[self addChild:airPlaneSprite z:tagHorizonFar+1];
    
	airPlaneSprite.position = ccp(-airPlaneSprite.contentSize.width/2 * airPlaneSprite.scale, 200 + (arc4random()% 4) * 20);
	
	int duration = 30 + arc4random() % 20;
	id actionMove = [CCMoveTo actionWithDuration:duration position:ccp(winSize.width + airPlaneSprite.contentSize.width/2 * airPlaneSprite.scale, airPlaneSprite.position.y)];
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(cloudMoveDone:)];
	[airPlaneSprite runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
}

- (void) dealloc
{
	[super dealloc];
}

- (void) setMainLayer:(MainLayer*) aMainLayer
{
    mainLayer = aMainLayer;
    [sa_fenceLayer setMainLayer:aMainLayer];
}

- (void) runMove:(int)runDistance speed:(int)speed
{
    [sa_houseLayer runMove:runDistance speed:speed];
    [sa_fenceLayer runMove:runDistance speed:speed];
    [sa_roadLayer runMove:runDistance speed:speed];
    [sa_treeLayer runMove:runDistance speed:speed];
    [sa_hillLayer runMove:runDistance speed:speed];
    [sa_horizon1Layer runMove:runDistance speed:speed];
    [sa_baseLayer runMove:runDistance speed:speed];
}
- (void) renderCloud: (ccTime)dt {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    int tagNum =  (arc4random()%2+1);
	NSString *strCloud = [NSString stringWithFormat:@"Cloud_%i_SC.png",tagNum];
    
    CCSprite *cloud = [CCSprite spriteWithFile:strCloud];
    cloud.scale = 0.4 + 0.01 * (arc4random()%10);
	[self addChild:cloud z:tagHorizonFar+tagNum];
    
	cloud.position = ccp(-cloud.contentSize.width/2 * cloud.scale, 250 + (arc4random()% 4) * 20);
	
	int duration = 30 + arc4random() % 20;
	id actionMove = [CCMoveTo actionWithDuration:duration position:ccp(winSize.width + cloud.contentSize.width/2 * cloud.scale, cloud.position.y)];
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(cloudMoveDone:)];
	[cloud runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
}
- (void) cloudMoveDone: (id) sender {
	CCSprite *cloud = (CCSprite *)sender;
	[self removeChild: cloud cleanup: YES];
}
- (void) renderBalloon: (ccTime)dt {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
	CCSprite *balloon = [CCSprite spriteWithFile:@"hot balloon_CS.png"];
    balloon.scale = 0.3 + 0.01 * (arc4random()%3);
    balloon.scale = balloon.scale * 4;
	[self addChild:balloon z:tagHorizonFar+4];
    
	balloon.position = ccp(-balloon.contentSize.width/2 * balloon.scale, 220 + (arc4random()% 2) * 50);
	
	int duration = 60 + arc4random() % 20;
	id actionMove = [CCMoveTo actionWithDuration:duration position:ccp(winSize.width + balloon.contentSize.width/2 * balloon.scale, balloon.position.y)];
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(balloonMoveDone:)];
	[balloon runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
}
- (void) balloonMoveDone: (id) sender {
	CCSprite *balloon = (CCSprite *)sender;
	[self removeChild:balloon cleanup: YES];
}
- (void) renderCloud_Dust: (ccTime)dt {
    CGSize winSize = [[CCDirector sharedDirector] winSize];
    
    int tagNum =  3;
	NSString *strCloud = @"Cloud_dust_horizon.png";
	CCSprite *cloud = [CCSprite spriteWithFile:strCloud];
	[self addChild:cloud z:tagHorizonFar+tagNum];
    
	cloud.position = ccp(-cloud.contentSize.width/2 + 480, 220);
	
	int duration = 33 + arc4random() % 20;
	id actionMove = [CCMoveTo actionWithDuration:duration position:ccp(winSize.width + cloud.contentSize.width/2 * cloud.scale, cloud.position.y)];
	id actionMoveDone = [CCCallFuncN actionWithTarget:self selector:@selector(cloud_DustMoveDone:)];
	[cloud runAction:[CCSequence actions:actionMove, actionMoveDone, nil]];
}
- (void) cloud_DustMoveDone: (id) sender {
	CCSprite *cloudDust = (CCSprite *)sender;
	[self removeChild:cloudDust cleanup: YES];
}

- (void)updateGamePlay:(ccTime)dt
{
    m_timecount++;
    if (m_timecount > 40000)
        m_timecount = 0;
    
    if (m_timecount % 2 == 0) {//cloud_dust move
        CGPoint pos = cloud_dustSprite.position;
        pos.x += 1;
        if (pos.x > 994)
            pos.x = -514;
        cloud_dustSprite.position = pos;
    }
}

@end
