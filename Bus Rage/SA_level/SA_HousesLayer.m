//
//  SA_HousesLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import "SA_HousesLayer.h"
#import "MainLayer.h"
#define SHOW_LIMIT 170

@implementation SA_HousesLayer
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        m_Objects = [NSArray arrayWithObjects://SC:0,1, 18,19,20, 21
                     
                     @"18, 40",//school1
                     @"19, 90",//school2
                     @"20, 140",//school3
                     @"21, 240",//basketball
                     @"18, 310",//school1
                     @"19, 360",//school2
                     @"20, 410",//school3
                     
                     @"0, 550",//house1
                     @"1, 600",//house2
                     @"18, 700",//school1
                     @"19, 760",//school2
                     @"20, 820",//school3
                     @"21, 920",//basketball
                     @"18, 1000",//school1
                     @"19, 1060",//school2
                     @"20, 1120",//school3
                     @"22, 3000",//storeAttack
                     nil];
        
        int house1_index = 0;
        int house2_index = 0;
        int shop1_index = 0;
        int shop2_index = 0;
        int shop3_index = 0;
        int park_index = 0;
        int storeAttack_index = 0;
        for (int i = 0; i < [m_Objects count]; i++ )
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case H_OINFO_HOUSE1:
                    m_House1[house1_index] = [[params objectAtIndex:1] intValue];
                    house1_index++;
                    break;
                case H_OINFO_HOUSE2:
                    m_House2[house2_index] = [[params objectAtIndex:1] intValue];
                    house2_index++;
                    break;
                case H_OINFO_SCHOOL1:
                    m_Shop1[shop1_index] = [[params objectAtIndex:1] intValue];
                    shop1_index++;
                    break;
                case H_OINFO_SCHOOL2:
                    m_Shop2[shop2_index] = [[params objectAtIndex:1] intValue];
                    shop2_index++;
                    break;
                case H_OINFO_SCHOOL3:
                    m_Shop3[shop3_index] = [[params objectAtIndex:1] intValue];
                    shop3_index++;
                    break;
                case H_OINFO_BASKETBALL:
                    m_park[park_index] = [[params objectAtIndex:1] intValue];
                    park_index++;
                    break;
                case H_OINFO_STOREATTACK:
                    m_StoreAttack[storeAttack_index] = [[params objectAtIndex:1] intValue];
                    storeAttack_index++;
                    break;
            }
        }
        
        house1Sprite = [CCSprite spriteWithFile:@"home_1_SV.png"];
        [self addChild:house1Sprite z:2];
        house1Sprite.visible = NO;
        
        house2Sprite = [CCSprite spriteWithFile:@"house_2_SV.png"];
        [self addChild:house2Sprite z:1];
        house2Sprite.visible = NO;
        
        shop1Sprite = [CCSprite spriteWithFile:@"barber_shop_SV.png"];
        [self addChild:shop1Sprite z:3];
        shop1Sprite.visible = NO;
        
        shop2Sprite = [CCSprite spriteWithFile:@"hardware_shop_SV.png"];
        [self addChild:shop2Sprite z:2];
        shop2Sprite.visible = NO;
        
        shop3Sprite = [CCSprite spriteWithFile:@"grocery_SV.png"];
        [self addChild:shop3Sprite z:1];
        shop3Sprite.visible = NO;
        
        parkSprite = [CCSprite spriteWithFile:@"Park_SV.png"];
        [self addChild:parkSprite z:0];
        parkSprite.visible = NO;
        
        storeAttackSprite = [CCSprite spriteWithFile:@"Store Attack 1.png"];
        [self addChild:storeAttackSprite z:10];
        storeAttackSprite.visible = NO;
        
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}

- (void) runMove:(int)runDistance speed:(int)speed
{
    [self showHouse1:runDistance];
    [self showHouse2:runDistance];
    [self showPark:runDistance];
    [self showShop1:runDistance];
    [self showShop2:runDistance];
    [self showShop3:runDistance];
}

- (void) showStoreAttack:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_StoreAttack[i];
//        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
//            tmpPos += 1200;
//        }
        int loopNum = [GameManager sharedGameManager].m_loopNum;
        if (tmpPos - (1200 * loopNum + runDistance) > 0) {
            diff[count] = tmpPos - (1200 * loopNum + runDistance);
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);
        }
    }
    storeAttackSprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        storeAttackSprite.visible = YES;
        
        float ss = 36.0;
        float sum = 230.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        storeAttackSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(230, 0), CGPointMake(443, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(443, 148));//b
        storeAttackSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        storeAttackSprite.scale = storeAttackSprite.scale * 3;
    }
}

- (void) showShop1:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Shop1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);
        }
    }
    shop1Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        shop1Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        shop1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        shop1Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        shop1Sprite.scale = shop1Sprite.scale * 3;
    }
}
- (void) showShop2:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Shop2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);
        }
    }
    shop2Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        shop2Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        shop2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        shop2Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        shop2Sprite.scale = shop2Sprite.scale * 3;
    }
}
- (void) showShop3:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Shop3[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);
        }
    }
    shop3Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        shop3Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        shop3Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        shop3Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        shop3Sprite.scale = shop3Sprite.scale * 3;
    }
}
- (void) showHouse1:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 2; i++)
    {
        tmpPos = m_House1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);
        }
    }
    house1Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        house1Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        house1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        house1Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        house1Sprite.scale = house1Sprite.scale * 3;
    }
}
- (void) showHouse2:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 2; i++)
    {
        tmpPos = m_House2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);
        }
    }
    house2Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        house2Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        house2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        house2Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        house2Sprite.scale = house2Sprite.scale * 3;
    }
}
- (void) showPark:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_park[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);
        }
    }
    parkSprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        parkSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        parkSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        parkSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        parkSprite.scale = parkSprite.scale * 1.75 * 2;
    }
}
@end
