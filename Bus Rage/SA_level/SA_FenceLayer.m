//
//  SA_FenceLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import "SA_FenceLayer.h"
#import "MainLayer.h"
#import "ADAdapter.h"
#define SHOW_LIMIT 170

@interface SA_FenceLayer (){
    NSMutableArray * billboards;
}

@end

@implementation SA_FenceLayer
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        billboards = [[NSMutableArray alloc] init];
        //SV:14,15,16,17,18,6,0(tree1),1(tree2),19(firehydrant)
        m_Objects = [NSArray arrayWithObjects:
                     @"1, 10",//tree2
                     //@"18, 30",//hoard
                     @"14, 50",//light1
                     @"19, 70",//firehydrant
                     @"0, 100",//tree1
                     @"15, 150",//light2
                     @"1, 200",//tree2
                     @"16, 250",//light3
                     @"6, 300",//roadsign
                     @"17, 350",//light4
                     
                     @"0, 400",//tree1
                     @"19, 420",//firehydrant
                     @"14, 450",//light1
                     @"1, 480",//tree2
                     @"18, 500",//hoard
                     @"15, 550",//light2
                     @"0, 580",//tree1
                     @"6, 600",//roadsign
                     @"16, 650",//light3
                     @"0, 700",//tree1
                     @"17, 750",//light4
                     
                     //@"18, 800",//hoard
                     @"14, 850",//light1
                     @"19, 870",//firehydrant
                     @"6, 900",//roadsign
                     @"15, 950",//light2
                     @"1, 1000",//tree2
                     @"16, 1050",//light3
                     @"17, 1150",//light4
                     @"22, 4000",//storeAttack
                     nil];
        int light1_index = 0;
        int light2_index = 0;
        int light3_index = 0;
        int light4_index = 0;
        int hoard_index = 0;
        int roadsign_index = 0;
        int tree1_index = 0;
        int tree2_index = 0;
        int firehydrant_index = 0;
        int storeAttack_index = 0;
        for (int i = 0; i < [m_Objects count]; i++ )
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case F_OINFO_LIGHT1:
                    m_streetLight1[light1_index] = [[params objectAtIndex:1] intValue];
                    light1_index++;
                    break;
                case F_OINFO_LIGHT2:
                    m_streetLight2[light2_index] = [[params objectAtIndex:1] intValue];
                    light2_index++;
                    break;
                case F_OINFO_LIGHT3:
                    m_streetLight3[light3_index] = [[params objectAtIndex:1] intValue];
                    light3_index++;
                    break;
                case F_OINFO_LIGHT4:
                    m_streetLight4[light4_index] = [[params objectAtIndex:1] intValue];
                    light4_index++;
                    break;
                case F_OINFO_HOARD:
                    m_hoarding[hoard_index] = [[params objectAtIndex:1] intValue];
                    hoard_index++;
                    break;
                case F_OINFO_ROADSIGN:
                    m_roadSign[roadsign_index] = [[params objectAtIndex:1] intValue];
                    roadsign_index++;
                    break;
                case F_OINFO_FENCE1_L:
                    m_tree1[tree1_index] = [[params objectAtIndex:1] intValue];
                    tree1_index++;
                    break;
                case F_OINFO_FENCE2_L:
                    m_tree2[tree2_index] = [[params objectAtIndex:1] intValue];
                    tree2_index++;
                    break;
                case F_OINFO_FIREHYDRANT:
                    m_fireHydrant[firehydrant_index] = [[params objectAtIndex:1] intValue];
                    firehydrant_index++;
                    break;
                case H_OINFO_STOREATTACK:
                    m_StoreAttack[storeAttack_index] = [[params objectAtIndex:1] intValue];
                    storeAttack_index++;
                    break;
            }
        }
        
        
        
        animation = [[CCAnimation animation] retain];
        animation.delay = 0.2;
        
        frame1 = [[CCSprite alloc] initWithFile:@"Store Attack 1.png"];
        frame2 = [[CCSprite alloc] initWithFile:@"Store Attack 2.png"];
        frame3 = [[CCSprite alloc] initWithFile:@"Store Attack 3.png"];
        frame4 = [[CCSprite alloc] initWithFile:@"Store Attack 4.png"];
        
//        frame1 = [[CCSprite alloc] initWithFile:@"Car showroom Attack.png"];
//        frame2 = [[CCSprite alloc] initWithFile:@"Car showroom Attack 1.png"];
//        frame3 = [[CCSprite alloc] initWithFile:@"Car showroom Attack 2.png"];
//        frame4 = [[CCSprite alloc] initWithFile:@"Car showroom Attack 3.png"];

        
        isCrashed = NO;
        
        streetLight1Sprite = [CCSprite spriteWithFile:@"Street light_SC.png"];
        [self addChild:streetLight1Sprite z:0];
        streetLight1Sprite.visible = NO;
        
        streetLight2Sprite = [CCSprite spriteWithFile:@"Street light_SC.png"];
        [self addChild:streetLight2Sprite z:0];
        streetLight2Sprite.visible = NO;
        
        streetLight3Sprite = [CCSprite spriteWithFile:@"Street light_SC.png"];
        [self addChild:streetLight3Sprite z:0];
        streetLight3Sprite.visible = NO;
        
        streetLight4Sprite = [CCSprite spriteWithFile:@"Street light_SC.png"];
        [self addChild:streetLight4Sprite z:0];
        streetLight4Sprite.visible = NO;
        
        hoardingSprite = [CCSprite spriteWithFile:@"Hoarding_SC.png"];
        hoardingSprite.tag = 4;
        [self addChild:hoardingSprite z:0];
        hoardingSprite.visible = NO;
        
        [billboards addObject:hoardingSprite];
        [ADAdapter insertADSToSprites:billboards];
        
        
        roadSignSprite = [CCSprite spriteWithFile:@"Road_sign_SC.png"];
        [self addChild:roadSignSprite z:0];
        roadSignSprite.visible = NO;
        
        tree1Sprite = [CCSprite spriteWithFile:@"Tree_1_SC.png"];
        [self addChild:tree1Sprite z:0];
        tree1Sprite.visible = NO;
        
        tree2Sprite = [CCSprite spriteWithFile:@"Tree_5_SC.png"];
        [self addChild:tree2Sprite z:0];
        tree2Sprite.visible = NO;
        
        fireHydrantSprite = [CCSprite spriteWithFile:@"Fire_hydrant_SV.png"];
        [self addChild:fireHydrantSprite z:0];
        fireHydrantSprite.visible = NO;
        
        storeAttackSprite = [CCSprite spriteWithFile:@"Store Attack 1.png"];//Store Attack 1.png
        [self addChild:storeAttackSprite z:10];
        storeAttackSprite.visible = NO;
        
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}

- (void) runMove:(int)runDistance speed:(int)speed
{
    if (!isCrashed) {
        
        [self showFireHydrant:runDistance];
        [self showHoarding:runDistance];
        [self showRoadSign:runDistance];
        [self showStreetLight1:runDistance];
        [self showStreetLight2:runDistance];
        [self showStreetLight3:runDistance];
        [self showStreetLight4:runDistance];
        [self showTree1:runDistance];
        [self showTree2:runDistance];
        [self showStoreAttack:runDistance];
        
    }
}

- (void) setMainLayer:(MainLayer*) aMainLayer
{
    mainLayer = aMainLayer;
}

- (void) showStoreAttack:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_StoreAttack[i];
//        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
//            tmpPos += 1200;
//        }
        int loopNum = [GameManager sharedGameManager].m_loopNum;
        if (tmpPos - (1200 * loopNum + runDistance) > 0) {
            diff[count] = tmpPos - (1200 * loopNum + runDistance);
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);
        }
    }
    storeAttackSprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < 200) {
        storeAttackSprite.visible = YES;
        NSLog(@"diff %d", diff_BusHouse);
        if (diff_BusHouse<=65 && diff_BusHouse>0) {
            NSLog(@"YEs, it is time to hide!");
            isCrashed = YES;
            //[mainLayer bus_break];
            [mainLayer crashed];
            [animation addFrame:frame1.displayedFrame];
            [animation addFrame:frame2.displayedFrame];
            [animation addFrame:frame3.displayedFrame];
            [animation addFrame:frame4.displayedFrame];
            
            CCAnimate * animate = [CCAnimate actionWithAnimation:animation restoreOriginalFrame:NO];
            
            [storeAttackSprite runAction:animate];
            
        }
        
        float ss = 36.0;
        float sum = 230.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        if (!isCrashed) {
            
            CGPoint cur_pos;
            cur_pos.y = sum1+40;
            cur_pos.x = sum;
            storeAttackSprite.position = cur_pos;
            
            float dist1 = ccpDistance(CGPointMake(230, 0), CGPointMake(443, 148+40));//a
            float dist2 = ccpDistance(cur_pos, CGPointMake(443, 148+40));//b
            
            frame1.scale = (0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1)*4;
            frame2.scale = (0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1)*4;
            frame3.scale = (0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1)*4;
            frame4.scale = (0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1)*4;
            
            if (frame1) {
                NSLog(@"Normal");
            }
            
            storeAttackSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
            storeAttackSprite.scale = storeAttackSprite.scale*4;
        }
    }

//    int diff_BusHouse = -1;
//    int diff[3];
//    int count = 0;
//    int tmpPos;
//    for (int i = 0; i < 3; i++)
//    {
//        tmpPos = m_StoreAttack[i];
//        //        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
//        //            tmpPos += 1200;
//        //        }
//        int loopNum = [GameManager sharedGameManager].m_loopNum;
//        if (tmpPos - (1200 * loopNum + runDistance) > 0) {
//            diff[count] = tmpPos - (1200 * loopNum + runDistance);
//            count++;
//        }
//    }
//    for (int i = 0; i < count; i++)
//    {
//        if (i == 0) {
//            diff_BusHouse = diff[i];
//        } else {
//            diff_BusHouse = MIN(diff_BusHouse, diff[i]);
//        }
//    }
//    storeAttackSprite.visible = NO;
//    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
//        storeAttackSprite.visible = YES;
//        
//        if (diff_BusHouse<=25 && diff_BusHouse>0) {
//            NSLog(@"YEs, it is time to hide!");
//            [mainLayer crashed];
//            [animation addFrame:frame1.displayedFrame];
//            [animation addFrame:frame2.displayedFrame];
//            [animation addFrame:frame3.displayedFrame];
//            [animation addFrame:frame4.displayedFrame];
//            
//            CCAnimate * animate = [CCAnimate actionWithAnimation:animation restoreOriginalFrame:NO];
//            
//            [storeAttackSprite runAction:animate];
//            isCrashed = YES;
//        }
//        
//        float ss = 36.0;
//        float sum = 230.0;
//        for (int i = 0; i < 50; i ++)
//        {
//            sum += ss;
//            if (i  == diff_BusHouse / 10) {
//                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);
//                break;
//            }
//            ss = ss * 0.831;
//        }
//        
//        float ss1 = 25.0;
//        float sum1 = 0.0;
//        for (int i = 0; i < 50; i ++)
//        {
//            sum1 += ss1;
//            if (i == diff_BusHouse / 10) {
//                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
//                break;
//            }
//            ss1 = ss1 * 0.831;//0.840;
//        }
//        if (!isCrashed) {
//            
//            CGPoint cur_pos;
//            cur_pos.y = sum1*2.4;
//            cur_pos.x = sum*2.1333333;
//            storeAttackSprite.position = cur_pos;
//            
//            float dist1 = ccpDistance(CGPointMake(230*2.1333333, 0), CGPointMake(443*2.1333333, 148*2.4));//a
//            float dist2 = ccpDistance(cur_pos, CGPointMake(443*2.1333333, 148*2.4));//b
//            
//            frame1.scale = (0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1) * 4;
//            frame2.scale = (0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1) * 4;
//            frame3.scale = (0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1) * 4;
//            frame4.scale = (0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1) * 4;
//            
//            storeAttackSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
//            storeAttackSprite.scale = storeAttackSprite.scale * 4;
//        }
//    }
    
}


- (void) showStreetLight1:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_streetLight1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);
        }
    }
    streetLight1Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        streetLight1Sprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        streetLight1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        streetLight1Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showStreetLight2:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_streetLight2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);
        }
    }
    streetLight2Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        streetLight2Sprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        streetLight2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        streetLight2Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showStreetLight3:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_streetLight3[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);
        }
    }
    streetLight3Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        streetLight3Sprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        streetLight3Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        streetLight3Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showStreetLight4:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_streetLight4[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);
        }
    }
    streetLight4Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        streetLight4Sprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        streetLight4Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        streetLight4Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showHoarding:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_hoarding[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);
        }
    }
    hoardingSprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        hoardingSprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        hoardingSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        hoardingSprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showRoadSign:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_roadSign[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);
        }
    }
    roadSignSprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        roadSignSprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        roadSignSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        roadSignSprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showTree1:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_tree1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);
        }
    }
    tree1Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        tree1Sprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        tree1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        tree1Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
    
}
- (void) showTree2:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_tree2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);
        }
    }
    tree2Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        tree2Sprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        tree2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        tree2Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
    
    //    int diff_BusBush = -1;
    //    int diff[3];
    //    int count = 0;
    //    int tmpPos;
    //    for (int i = 0; i < 3; i++)
    //    {
    //        tmpPos = m_roadSign[i];
    //        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
    //            tmpPos += 1200;
    //        }
    //        if (tmpPos - runDistance > 0) {
    //            diff[count] = tmpPos - runDistance;
    //            count++;
    //        }
    //    }
    //    for (int i = 0; i < count; i++)
    //    {
    //        if (i == 0) {
    //            diff_BusBush = diff[i];
    //        } else {
    //            diff_BusBush = MIN(diff_BusBush, diff[i]);
    //        }
    //    }
    //    tree2Sprite.visible = NO;
    //    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
    //        tree2Sprite.visible = YES;
    //
    //        float ss = 82.0;
    //        float sum = -80.0;
    //        for (int i = 0; i < 50; i ++)
    //        {
    //            sum += ss;
    //            if (i  == diff_BusBush / 10) {
    //                sum += ss * 0.831 / 10 * (diff_BusBush % 10);
    //                break;
    //            }
    //            ss = ss * 0.831;
    //        }
    //
    //        float ss1 = 25.0;
    //        float sum1 = 0.0;
    //        for (int i = 0; i < 50; i ++)
    //        {
    //            sum1 += ss1;
    //            if (i == diff_BusBush / 10) {
    //                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
    //                break;
    //            }
    //            ss1 = ss1 * 0.831;//0.840;
    //        }
    //        CGPoint cur_pos;
    //        cur_pos.y = sum1;
    //        cur_pos.x = sum;
    //        tree2Sprite.position = cur_pos;
    //
    //        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
    //        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
    //        tree2Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    //    }
}
- (void) showFireHydrant:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_fireHydrant[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);
        }
    }
    fireHydrantSprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        fireHydrantSprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {
            sum += ss;
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss = ss * 0.831;
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }
            ss1 = ss1 * 0.831;//0.840;
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;
        cur_pos.x = sum;
        fireHydrantSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        fireHydrantSprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1;
        fireHydrantSprite.scale = fireHydrantSprite.scale * 0.4 * 4;
    }
    
}
@end
