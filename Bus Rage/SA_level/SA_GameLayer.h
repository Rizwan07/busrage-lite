//
//  SA_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SA_HousesLayer.h"
#import "SA_FenceLayer.h"
#import "SA_RoadLayer.h"
#import "SA_TreesLayer.h"
#import "SA_HillsLayer.h"
#import "SA_Horizon1Layer.h"
#import "SA_BaseLayer.h"
@class MainLayer;

@interface SA_GameLayer : CCLayer {
    SA_HousesLayer *sa_houseLayer;
    SA_FenceLayer *sa_fenceLayer;
    SA_RoadLayer *sa_roadLayer;
    SA_TreesLayer *sa_treeLayer;
    SA_HillsLayer *sa_hillLayer;
    SA_Horizon1Layer *sa_horizon1Layer;
    SA_BaseLayer *sa_baseLayer;
    CCSprite *cloud_dustSprite;
    MainLayer * mainLayer;
    int m_timecount;
    
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) setMainLayer:(MainLayer*) mainLayer;

@end
