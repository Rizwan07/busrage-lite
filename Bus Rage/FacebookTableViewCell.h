//
//  FacebookTableViewCell.h
//  Bus Rage
//
//  Created by Jin Tie on 5/27/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FacebookTableViewCell : UITableViewCell {
    NSDictionary*              _file; 
    BOOL                       _completeLoading; 
    UIImageView *              _posterImageView;
	UILabel *                  _titleLabel;
	UIActivityIndicatorView *  _activityIndicator;
}
+ (CGFloat)cellHeight;
+ (FacebookTableViewCell *)cellWithReuseIdentifier:(NSString *)reuseIdentifier;
- (void)setEven:(BOOL)even;
- (BOOL)isComplete; 

@property (nonatomic, retain) NSDictionary * file;
@property (nonatomic, retain) IBOutlet UIImageView * posterImageView;
@property (nonatomic, retain) IBOutlet UILabel * titleLabel;
@property (nonatomic, retain) IBOutlet UIActivityIndicatorView * activityIndicator;
@end
