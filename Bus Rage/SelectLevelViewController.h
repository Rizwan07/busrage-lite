//
//  SelectLevelViewController.h
//  Bus Rage
//
//  Created by Jin Tie on 5/9/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectTargetViewController.h"
#import "SimpleAudioEngine.h"

@interface SelectLevelViewController : UIViewController {    
    SelectTargetViewController *_selecttargetViewController;
    NSMutableArray *m_LevelButtonArray;
}

@property (retain) SelectTargetViewController *selecttargetViewController;
@property (nonatomic, retain) IBOutlet UIScrollView *scrollView;
@property (nonatomic, retain) IBOutlet UIImageView *imageView;
@property (nonatomic, retain) IBOutlet UITextView *textView1;
@property (nonatomic, retain) IBOutlet UITextView *textView2;
- (IBAction)backTapped:(id)sender;
- (IBAction)nextTapped:(id)sender;
- (void)picClicked:(id)sender;
@end
