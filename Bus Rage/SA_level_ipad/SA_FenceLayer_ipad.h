//
//  SA_FenceLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@class MainLayer_iPad;

@interface SA_FenceLayer_ipad : CCLayer {
    CCSprite *streetLight1Sprite;
    CCSprite *streetLight2Sprite;
    CCSprite *streetLight3Sprite;
    CCSprite *streetLight4Sprite;
    CCSprite *hoardingSprite;
    CCSprite *roadSignSprite;
    CCSprite *tree1Sprite;
    CCSprite *tree2Sprite;
    CCSprite *fireHydrantSprite;
    
    CCSprite *storeAttackSprite;
    MainLayer_iPad * mainLayer;
    CCAnimation * animation;
    
    int m_streetLight1[3];
    int m_streetLight2[3];
    int m_streetLight3[3];
    int m_streetLight4[3];
    int m_hoarding[3];
    int m_roadSign[3];
    int m_tree1[4];
    int m_tree2[4];
    int m_fireHydrant[3];
    int m_StoreAttack[3];
    BOOL isCrashed;
    
    CCSprite * frame1;
    CCSprite * frame2;
    CCSprite * frame3;
    CCSprite * frame4;
    
//    CCSprite * frameC1;
//    CCSprite * frameC2;
//    CCSprite * frameC3;
//    CCSprite * frameC4;
    
    NSArray*    m_Objects;
//    int levelNumber;
}
- (void) runMove:(int)runDistance speed:(int)speed;

- (void) showStreetLight1:(int)runDistance;
- (void) showStreetLight2:(int)runDistance;
- (void) showStreetLight3:(int)runDistance;
- (void) showStreetLight4:(int)runDistance;
- (void) showHoarding:(int)runDistance;
- (void) showRoadSign:(int)runDistance;
- (void) showTree1:(int)runDistance;
- (void) showTree2:(int)runDistance;
- (void) showFireHydrant:(int)runDistance;
- (void) showStoreAttack:(int)runDistance;

//- (void) setLevelNumber:(int) number;

- (void) setMainLayer:(MainLayer_iPad*) aMainLayer;

@end
