//
//  SA_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SA_HousesLayer_ipad.h"
#import "SA_FenceLayer_ipad.h"
#import "SA_RoadLayer_ipad.h"
#import "SA_TreesLayer_ipad.h"
#import "SA_HillsLayer_ipad.h"
#import "SA_Horizon1Layer_ipad.h"
#import "SA_BaseLayer_ipad.h"
@class MainLayer_iPad;

@interface SA_GameLayer_ipad : CCLayer {
    SA_HousesLayer_ipad *sa_houseLayer;
    SA_FenceLayer_ipad *sa_fenceLayer;
    SA_RoadLayer_ipad *sa_roadLayer;
    SA_TreesLayer_ipad *sa_treeLayer;
    SA_HillsLayer_ipad *sa_hillLayer;
    SA_Horizon1Layer_ipad *sa_horizon1Layer;
    SA_BaseLayer_ipad *sa_baseLayer;
    CCSprite *cloud_dustSprite;
    MainLayer_iPad * mainLayer;
    int m_timecount;
    
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) setMainLayer:(MainLayer_iPad*) mainLayer;

@end
