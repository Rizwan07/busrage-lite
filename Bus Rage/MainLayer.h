//
//  CS_MainLayer.h  CountrySide
//  Bus Rage
//
//  Created by Jin Tie on 4/23/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CBus.h"
#import "CTarget.h"
#import "SimpleAudioEngine.h"
#import "GameManager.h"
#import "RootViewController.h"
#import "CS_GameLayer.h"
#import "MV_GameLayer.h"
#import "OR_GameLayer.h"
#import "SC_GameLayer.h"
#import "SV_GameLayer.h"
#import "SA_GameLayer.h"
#import "CA_GameLayer.h"
#import "FB_GameLayer.h"
#import "GirlWalking.h"
#import "SCBoy.h"
#import "SCGirl.h"
#import "SCTeacher.h"
#import "SAStoreAttack.h"

#import "EnumConstances.h"

@interface MainLayer : CCLayerColor {
    
    RootViewController * _rootViewController;
    int m_timecount;
    int m_runDistance;
    
    int m_BusSpeed;  
    float m_BusSpeedFloat;
    
    CS_GameLayer *cs_gameLayer;
    MV_GameLayer *mv_gameLayer;
    OR_GameLayer *or_gameLayer;
    SC_GameLayer *sc_gameLayer;
    SV_GameLayer *sv_gameLayer;
    
    //SV_GameLayer *sv_gameLayerForLevel6;
    //SV_GameLayer *sv_gameLayerForLevel7;

    SA_GameLayer *sa_gameLayer;
    CA_GameLayer *ca_gameLayer;
    FB_GameLayer *fb_gameLayer;
    
    bool m_bSteering;
    int m_steering_angle;
    //float m_Bus_angle, m_Bus_angle_old;
    bool m_Bus_angle_Old_Right;    
    //bool m_bFlit;
    
    CCMenu *m_menuGameOver, *m_menuSuccess;
    CCMenu *m_menuMissedTarget;
    
    CCSprite *m_ReadySprite1, *m_ReadySprite2, *m_ReadySprite3;
    CCLabelTTF *m_labelSpeed, *m_labelScore, *m_labelTime, *m_labelMiss;
    bool m_bTimeClock;
    int m_Time, m_MissT;
    
    //CCSprite *busSprite;
    CBus *m_Bus;
    //int m_CollideCountLeft, m_CollideCountRight;
    bool m_bCollideLeft, m_bCollideRight;
    //bool m_bBusFlip;
    
    int m_levelNum;
    int m_loopNum;
    
    NSMutableArray *m_targetArray;
    NSMutableArray *m_targetArrChooseForAll;
    
    CCSprite *m_BusHitAreaSprite;
    CGPoint m_pos;
    
    CCLabelTTF *m_target1Label;
    CCLabelTTF *m_target2Label;
    CCLabelTTF *m_target3Label;
    CCLabelTTF *m_target4Label;
    CCLabelTTF *m_target5Label;
    CCLabelTTF *m_target6Label;
    CCLabelTTF *m_target7Label;
    CCSprite *m_ShowMessageTargetDead;
    CCSprite *m_ShowMessageTargetInternalOrganDamage;
    CCSprite *m_ShowMessageTargetArmBroken;
    CCSprite *m_ShowMessageTargetLegBroken;
    CCSprite *m_ShowMessageTargetBackBroken;
    CCSprite *m_ShowMessageTargetComa;
    int m_ShowStatus; //1:Target is Dead 
    //2:Target has Internal Organ Damage 3:Target has Arm Broken 4:Target has Leg Broken 5:Target has Back Broken 6:Target is in a Coma
    
    //Obstacles
    NSMutableArray *m_RockArray;
    NSMutableArray *m_SV4ObstacleArray;
    GirlWalking *m_girlWalk;
    SCBoy *m_scBoy;
    SCGirl *m_scGirl;
    SCTeacher *m_scTeacher;
    bool m_bStart;
    
}
+(CCScene *) scene:(RootViewController *)rootViewController;
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) setBackgroud;
- (void) showTargets:(int)runDistance;
- (void) targetsWalk;
- (void) checkBusTargetHit;

- (void) showObstacles:(int)runDistance;
- (void) checkBusObstaclesHit;
- (void) obstacleAnim_walk;
- (void) SV5AnimShow:(int)runDistance;

- (void) setTargetPassengerDistance;
- (void) lifeNoHitTargetsReplace;
- (void) btnGameOverClick;
//- (void) btnSuccessClick;
- (void) bus_break;
- (void) yesFaceBookClick;
- (void) noFaceBookClick;
- (void) setTime;
- (void) videoPlayFunKill;
- (void) videoPlayFunNoKill;
- (void) crashed;
- (void) videoPlayFunEnd;
@end
