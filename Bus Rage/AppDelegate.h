//
//  AppDelegate.h
//  Bus Rage
//
//  Created by Jin Tie on 5/9/12.
//  Copyright k 2012. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BRBannerViewDelegate.h"

//@class RootViewController;
@class SelectTargetViewController;

@interface AppDelegate : NSObject <UIApplicationDelegate, BRBannerViewDelegate> {
	UIWindow			*window;
//	RootViewController	*viewController;
    SelectTargetViewController* selecttargetViewController;
}

@property (nonatomic, retain) IBOutlet UIWindow *window;
@property (nonatomic, retain) SelectTargetViewController* selecttargetViewController;
@end
