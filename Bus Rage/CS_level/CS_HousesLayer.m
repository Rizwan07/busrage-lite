//
//  CS_HousesLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 4/23/12.
//  Copyright 2012 k. All rights reserved.
//

#import "CS_HousesLayer.h"
#import "MainLayer.h"
#define SHOW_LIMIT 300

@implementation CS_HousesLayer
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        m_Objects = [NSArray arrayWithObjects:
                     @"2, 40",//barn
                     @"0, 75",//house1
                     @"3, 94",//farm                     
                     @"4, 140",//bushes_u
                     @"10, 180",//ditch1
                     @"1, 220",//house2  
                     @"11, 250",//ditch2
                     @"12, 260",//woodenpost
                     @"9, 290",//cabin                     
                     
                     @"2, 340",//barn
                     @"0, 375",//house1
                     @"3, 394",//farm                     
                     @"4, 440",//bushes_u
                     @"10, 480",//ditch1
                     @"1, 520",//house2  
                     @"11, 550",//ditch2
                     @"12, 560",//woodenpost
                     @"9, 590",//cabin 
                     
                     @"7, 620",//farmWithFence 
                     //@"5, 640",//billboard1                                         
                     @"6, 700",//billboard2
                     @"4, 750",//bushes_u
                     @"7, 800",//farmWithFence
                     @"8, 820",//cow                     
                     @"12, 870",//woodenpost
                     @"10, 890",//ditch1
                     @"2, 920",//barn                     
                     @"11, 960",//ditch2   
                     @"0, 1000",//house1
                     @"9, 1100",//cabin
                     nil];
        int house1_index = 0;
        int house2_index = 0;
        int barn_index = 0;
        int farm_index = 0;
        int bushes_u_index = 0;
        int billboard1_index = 0;
        int billboard2_index = 0;
        int farmWithFence_index = 0;
        int cow_index = 0;
        int cabin_index = 0;
        int ditch1_index = 0;
        int ditch2_index = 0;
        int woodenPost_index = 0;
        for (int i = 0; i < [m_Objects count]; i++ )
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case H_OINFO_HOUSE1:                
                    m_House1[house1_index] = [[params objectAtIndex:1] intValue];
                    house1_index++;             
                    break;	
                case H_OINFO_HOUSE2:                
                    m_House2[house2_index] = [[params objectAtIndex:1] intValue];
                    house2_index++;                
                    break;
                case H_OINFO_BARN:
                    m_Barn[barn_index] = [[params objectAtIndex:1] intValue];
                    barn_index++;                
                    break;	
                case H_OINFO_FARM:                
                    m_Farm[farm_index] = [[params objectAtIndex:1] intValue];
                    farm_index++;                
                    break;
                case H_OINFO_BUSHES_U:                
                    m_Bush_U[bushes_u_index] = [[params objectAtIndex:1] intValue];
                    bushes_u_index++;                
                    break;	
                case H_OINFO_BILLBOARD1:                
                    m_BillBoard1[billboard1_index] = [[params objectAtIndex:1] intValue];
                    billboard1_index++;                
                    break;	
                case H_OINFO_BILLBOARD2:                
                    m_BillBoard2[billboard2_index] = [[params objectAtIndex:1] intValue];
                    billboard2_index++;                
                    break;
                case H_OINFO_FARM_FENCE:                
                    m_FarmWithFence[farmWithFence_index] = [[params objectAtIndex:1] intValue];
                    farmWithFence_index++;                
                    break;
                case H_OINFO_COW:                
                    m_Cow[cow_index] = [[params objectAtIndex:1] intValue];
                    cow_index++;                
                    break;
                case H_OINFO_CABIN:                
                    m_Cabin[cabin_index] = [[params objectAtIndex:1] intValue];
                    cabin_index++;                
                    break;
                case H_OINFO_DITCH1:                
                    m_Ditch1[ditch1_index] = [[params objectAtIndex:1] intValue];
                    ditch1_index++;                
                    break;
                case H_OINFO_DITCH2:                
                    m_Ditch2[ditch2_index] = [[params objectAtIndex:1] intValue];
                    ditch2_index++;                
                    break;
                case H_OINFO_WOODENPOST:                
                    m_WoodenPost[woodenPost_index] = [[params objectAtIndex:1] intValue];
                    woodenPost_index++;                
                    break;
            }
        }
        //
        //houseSprite_position = 700;
        //CGSize size = [[CCDirector sharedDirector] winSize];
        
        houseSprite1 = [CCSprite spriteWithFile:@"House_CS.png"];
        //houseSprite.position = ccp(361, 148);
        [self addChild:houseSprite1 z:2];
        houseSprite1.visible = NO;        
        //houseSprite.scale = 0.05;
        
        houseSprite2 = [CCSprite spriteWithFile:@"House_CS.png"];
        [self addChild:houseSprite2 z:1];
        houseSprite2.visible = NO;
        
        barnSprite = [CCSprite spriteWithFile:@"Barn_1_CS.png"];
        [self addChild:barnSprite z:3];
        barnSprite.visible = NO;
        
        farmSprite = [CCSprite spriteWithFile:@"farm_feild_1_CS.png"];
        [self addChild:farmSprite z:0];
        farmSprite.visible = NO;
        
        bushesUSprite = [CCSprite spriteWithFile:@"Bushes_undertree_CS.png"];
        [self addChild:bushesUSprite z:4];
        bushesUSprite.visible = NO;
        
        billBoard1Sprite = [CCSprite spriteWithFile:@"Billboard_CS.png"];
        [self addChild:billBoard1Sprite z:2];
        billBoard1Sprite.visible = NO;
        billBoard2Sprite = [CCSprite spriteWithFile:@"Billboard_CS.png"];
        [self addChild:billBoard2Sprite z:1];
        billBoard2Sprite.visible = NO;
        
        farmWithFenceSprite = [CCSprite spriteWithFile:@"farm_feild_2_CS.png"];
        [self addChild:farmWithFenceSprite z:0];
        farmWithFenceSprite.visible = NO;
        
        cowSprite = [CCSprite spriteWithFile:@"Cow_CS.png"];
        [self addChild:cowSprite z:0];
        cowSprite.visible = NO;
        
        cabinSprite = [CCSprite spriteWithFile:@"Cabin_1_CS.png"];
        [self addChild:cabinSprite z:0];
        cabinSprite.visible = NO;
        
        ditch1Sprite = [CCSprite spriteWithFile:@"Ditch_1_CS.png"];
        [self addChild:ditch1Sprite z:0];
        ditch1Sprite.visible = NO;
        
        ditch2Sprite = [CCSprite spriteWithFile:@"Ditch_2_CS.png"];
        [self addChild:ditch2Sprite z:0];
        ditch2Sprite.visible = NO;
        
        woodenPostSprite = [CCSprite spriteWithFile:@"Wooden_sign_post_CS.png"];
        [self addChild:woodenPostSprite z:0];
        woodenPostSprite.visible = NO;
//        CCSprite *houseSprite1 = [CCSprite spriteWithFile:@"House_CS.png"];
//        houseSprite1.position = ccp(39, 63);
//        //[self addChild:houseSprite1 z:0];
//        houseSprite1.scale = 0.8;//0.4804
//        
        /*CCSprite *lineSprite;
        float ss = 39.334;//62.0;
        float sum = 200.0;//50.0;
        for (int i = 0; i < 50; i ++)
        {  
            lineSprite = [CCSprite spriteWithFile:@"line2.png"];
            lineSprite.position = ccp(sum, 160); 
            [self addChild:lineSprite z:tagRoad + 1];
            
            NSLog(@"%i: %.9f, %.9f", i+1, ss, sum);
            sum += ss;
            ss = ss * 0.831;             
        }       
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            
            lineSprite = [CCSprite spriteWithFile:@"line.png"];
            lineSprite.position = ccp(240, sum1);
            [self addChild:lineSprite z:tagRoad + 1];
            
            NSLog(@"%i: %.9f, %.9f", i+1, ss1, sum1);           
            sum1 += ss1;                       
            ss1 = ss1 * 0.831;//0.840;             
        }*/
        //init_diff = 0.489121944;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance
{
    [self showHouse2:runDistance];
    [self showHouse1:runDistance];
    [self showBarn:runDistance];
    [self showFarm:runDistance];
    [self showBushes_U:runDistance];
    [self showBillBoard1:runDistance];
    [self showBillBoard2:runDistance];
    [self showFarmWithFence:runDistance];
    [self showCow:runDistance]; 
    [self showCabin:runDistance]; 
    [self showDitch1:runDistance];
    [self showDitch2:runDistance];
    [self showWoodenPost:runDistance];
}
- (void) showHouse1:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_House1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    houseSprite1.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        houseSprite1.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = houseSprite1.position;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        houseSprite1.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        houseSprite1.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        houseSprite1.scale = houseSprite1.scale * 4;
    }
}
- (void) showHouse2:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_House2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    houseSprite2.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        houseSprite2.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = houseSprite2.position;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        houseSprite2.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        houseSprite2.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        houseSprite2.scale = houseSprite2.scale * 4;
    }
}
- (void) showBarn:(int)runDistance
{
    int diff_BusBran = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Barn[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBran = diff[i];
        } else {
            diff_BusBran = MIN(diff_BusBran, diff[i]);                
        }
    }
    barnSprite.visible = NO;
    if (diff_BusBran > 0 && diff_BusBran < SHOW_LIMIT) {
        barnSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBran / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBran % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBran / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBran % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = barnSprite.position;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        barnSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        barnSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        barnSprite.scale = barnSprite.scale * 4;
    }
}
- (void) showFarm:(int)runDistance
{
    int diff_BusFarm = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Farm[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusFarm = diff[i];
        } else {
            diff_BusFarm = MIN(diff_BusFarm, diff[i]);                
        }
    }
    farmSprite.visible = NO;
    if (diff_BusFarm > 0 && diff_BusFarm < SHOW_LIMIT - 150) {
        farmSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusFarm / 10) {
                sum += ss * 0.831 / 10 * (diff_BusFarm % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusFarm / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusFarm % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = farmSprite.position;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        farmSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        farmSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        farmSprite.scale = farmSprite.scale * 4;
    }
}
- (void) showBushes_U:(int)runDistance
{
    int diff_BusBushes = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush_U[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBushes = diff[i];
        } else {
            diff_BusBushes = MIN(diff_BusBushes, diff[i]);                
        }
    }
    bushesUSprite.visible = NO;
    if (diff_BusBushes > 0 && diff_BusBushes < SHOW_LIMIT) {
        bushesUSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBushes / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBushes % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBushes / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBushes % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos = bushesUSprite.position;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        bushesUSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        bushesUSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        bushesUSprite.scale = bushesUSprite.scale * 2;
    }
}
- (void) showBillBoard1:(int)runDistance
{
    int diff_BusBillBoard = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_BillBoard1[i];        
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBillBoard = diff[i];
        } else {
            diff_BusBillBoard = MIN(diff_BusBillBoard, diff[i]);                
        }
    }
    billBoard1Sprite.visible = NO;
    if (diff_BusBillBoard > 0 && diff_BusBillBoard < SHOW_LIMIT) {
        billBoard1Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBillBoard / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBillBoard % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBillBoard / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBillBoard % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        billBoard1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        billBoard1Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        billBoard1Sprite.scale = billBoard1Sprite.scale * 2.164;
    }
}
- (void) showBillBoard2:(int)runDistance
{
    int diff_BusBillBoard = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_BillBoard2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBillBoard = diff[i];
        } else {
            diff_BusBillBoard = MIN(diff_BusBillBoard, diff[i]);                
        }
    }
    billBoard2Sprite.visible = NO;
    if (diff_BusBillBoard > 0 && diff_BusBillBoard < SHOW_LIMIT) {
        billBoard2Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBillBoard / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBillBoard % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBillBoard / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBillBoard % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        billBoard2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        billBoard2Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        billBoard2Sprite.scale = billBoard2Sprite.scale * 2.164;
    }
}
- (void) showFarmWithFence:(int)runDistance
{
    int diff_BusFarm = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_FarmWithFence[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusFarm = diff[i];
        } else {
            diff_BusFarm = MIN(diff_BusFarm, diff[i]);                
        }
    }
    farmWithFenceSprite.visible = NO;
    if (diff_BusFarm > 0 && diff_BusFarm < SHOW_LIMIT - 200) {
        farmWithFenceSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusFarm / 10) {
                sum += ss * 0.831 / 10 * (diff_BusFarm % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusFarm / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusFarm % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        farmWithFenceSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        farmWithFenceSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        farmWithFenceSprite.scale = farmWithFenceSprite.scale * 3.4 * 2;
    }
}
     
- (void) showCow:(int)runDistance
{
    int diff_BusCow = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Cow[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusCow = diff[i];
        } else {
            diff_BusCow = MIN(diff_BusCow, diff[i]);                
        }
    }
    cowSprite.visible = NO;
    if (diff_BusCow > 0 && diff_BusCow < SHOW_LIMIT - 200) {
        cowSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusCow / 10) {
                sum += ss * 0.831 / 10 * (diff_BusCow % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusCow / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusCow % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        cowSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        cowSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        cowSprite.scale = cowSprite.scale * 0.5 * 4;
    }
}
- (void) showCabin:(int)runDistance
{
    int diff_BusCabin = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Cabin[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusCabin = diff[i];
        } else {
            diff_BusCabin = MIN(diff_BusCabin, diff[i]);                
        }
    }
    cabinSprite.visible = NO;
    if (diff_BusCabin > 0 && diff_BusCabin < SHOW_LIMIT) {
        cabinSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusCabin / 10) {
                sum += ss * 0.831 / 10 * (diff_BusCabin % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusCabin / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusCabin % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        cabinSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        cabinSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        cabinSprite.scale = cabinSprite.scale * 4;
    }
}
- (void) showDitch1:(int)runDistance
{
    int diff_BusObject = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Ditch1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusObject = diff[i];
        } else {
            diff_BusObject = MIN(diff_BusObject, diff[i]);                
        }
    }
    ditch1Sprite.visible = NO;
    if (diff_BusObject > 0 && diff_BusObject < SHOW_LIMIT) {
        ditch1Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusObject / 10) {
                sum += ss * 0.831 / 10 * (diff_BusObject % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusObject / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusObject % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        ditch1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        ditch1Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        ditch1Sprite.scale = ditch1Sprite.scale * 2;
    }
}
- (void) showDitch2:(int)runDistance
{
    int diff_BusObject = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;    
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Ditch2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusObject = diff[i];
        } else {
            diff_BusObject = MIN(diff_BusObject, diff[i]);                
        }
    }
    ditch2Sprite.visible = NO;
    if (diff_BusObject > 0 && diff_BusObject < SHOW_LIMIT) {
        ditch2Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusObject / 10) {
                sum += ss * 0.831 / 10 * (diff_BusObject % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusObject / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusObject % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        ditch2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        ditch2Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        ditch2Sprite.scale = ditch2Sprite.scale * 2;
    }
}
- (void) showWoodenPost:(int)runDistance
{
    int diff_BusObject = -1;
    int diff[3];
    int count = 0;
    
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_WoodenPost[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusObject = diff[i];
        } else {
            diff_BusObject = MIN(diff_BusObject, diff[i]);                
        }
    }
    woodenPostSprite.visible = NO;
    if (diff_BusObject > 0 && diff_BusObject < SHOW_LIMIT) {
        woodenPostSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusObject / 10) {
                sum += ss * 0.831 / 10 * (diff_BusObject % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusObject / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusObject % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        woodenPostSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200, 0), CGPointMake(361, 148));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361, 148));//b
        woodenPostSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        woodenPostSprite.scale = woodenPostSprite.scale * 1.5 * 2;
    }
}
@end
