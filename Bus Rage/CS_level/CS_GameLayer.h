//
//  CS_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "CS_HousesLayer.h"
#import "CS_FenceLayer.h"
#import "CS_RoadLayer.h"
#import "CS_TreesLayer.h"
#import "CS_HillsLayer.h"
#import "CS_Horizon1Layer.h"
#import "CS_BaseLayer.h"

@interface CS_GameLayer : CCLayer {
    CS_HousesLayer *cs_houseLayer;
    CS_FenceLayer *cs_fenceLayer;
    CS_RoadLayer *cs_roadLayer;
    CS_TreesLayer *cs_treeLayer;
    CS_HillsLayer *cs_hillLayer;
    CS_Horizon1Layer *cs_horizon1Layer;
    CS_BaseLayer *cs_baseLayer;
    
    CCSprite *cloud_dustSprite;
    int m_timecount;
}
- (void) runMove:(int)runDistance speed:(int)speed;
@end
