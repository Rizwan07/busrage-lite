//
//  CS_TreesLayer_ipad.m
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "OR_TreesLayer_ipad.h"

#import "EnumConstances.h"

#define SHOW_LIMIT 300

@implementation OR_TreesLayer_ipad
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        m_Objects = [NSArray arrayWithObjects://OR:0,1,3,4
                     
                     @"0, 140",//tree1
                     @"3, 170",//bush1
                     @"1, 200",//tree2  
                     @"4, 250",//bush2                   
                     
                     @"0, 680",//tree1 
                     @"3, 770",//bush1                       
                     @"4, 850",//bush2
                     @"1, 1000",//tree2                    
                     nil];
        int cactus1_index = 0;
        int cactus2_index = 0;
        int bush1_index = 0;
        int bush2_index = 0;
        for (int i = 0; i < [m_Objects count]; i++)
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case T_OINFO_TREE1:                
                    m_Cactus1[cactus1_index] = [[params objectAtIndex:1] intValue];
                    cactus1_index++;                
                    break;	
                case T_OINFO_TREE2:                
                    m_Cactus2[cactus2_index] = [[params objectAtIndex:1] intValue];
                    cactus2_index++;                
                    break;   
                case T_OINFO_BUSH1:
                    m_Bush1[bush1_index] = [[params objectAtIndex:1] intValue];
                    bush1_index++;
                    break;
                case T_OINFO_BUSH2:
                    m_Bush2[bush2_index] = [[params objectAtIndex:1] intValue];
                    bush2_index++;
                    break;
            }
        }        
        cactus1Sprite = [CCSprite spriteWithFile:@"cactus_1_OR_ipad.png"];
        [self addChild:cactus1Sprite z:2];        
        cactus1Sprite.visible = NO;
        
        cactus2Sprite = [CCSprite spriteWithFile:@"cactus_2_OR_ipad.png"];
        [self addChild:cactus2Sprite z:2];        
        cactus2Sprite.visible = NO;
        
        bush1Sprite = [CCSprite spriteWithFile:@"Bush_OR_ipad.png"];
        [self addChild:bush1Sprite z:1];
        bush1Sprite.visible = NO;
        
        bush2Sprite = [CCSprite spriteWithFile:@"Bush_OR_ipad.png"];
        [self addChild:bush2Sprite z:1];
        bush2Sprite.visible = NO;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    [self showBush1:runDistance];
    [self showBush2:runDistance];
    [self showCactus1:runDistance];
    [self showCactus2:runDistance];    
}
- (void) showCactus1:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Cactus1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    cactus1Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        cactus1Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        cactus1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a 
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        cactus1Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        cactus1Sprite.scale = cactus1Sprite.scale * 4;
    }
}
- (void) showCactus2:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Cactus2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    cactus2Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        cactus2Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        cactus2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a 
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        cactus2Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        cactus2Sprite.scale = cactus2Sprite.scale * 4;
    }
}
- (void) showBush1:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    bush1Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        bush1Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        bush1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a 
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        bush1Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        bush1Sprite.scale = bush1Sprite.scale * 4;
    }
}
- (void) showBush2:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    bush2Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        bush2Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        bush2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a 
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        bush2Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        bush2Sprite.scale = bush2Sprite.scale * 4;
    }
}

@end
