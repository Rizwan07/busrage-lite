//
//  CS_HousesLayer_ipad.m
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "OR_HousesLayer_ipad.h"

#import "EnumConstances.h"

#define SHOW_LIMIT 300

@implementation OR_HousesLayer_ipad
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        m_Objects = [NSArray arrayWithObjects://OR:0,1,2,9,4,5,14,15,16(plant),17(straw)
                     
                     @"14, 20",//cactus1
                     @"2, 30",//saloon
                     @"4, 60",//bush 
                     @"0, 70",//house1   
                     @"15, 110",//cactus2
                     @"1, 130",//house2
                     @"16, 270",//plant                     
                     @"17, 300",//straw                     
                     @"9, 320",//cabin
                     
                     @"14, 620",//cactus1
                     @"5, 630",//board
                     @"4, 660",//bush 
                     @"0, 670",//house1   
                     @"15, 710",//cactus2
                     @"2, 730",//house2
                     @"16, 870",//plant                     
                     @"17, 900",//straw
                     @"14, 950",//cactus1
                     @"15, 1100",//cactus2 
                     nil];
        
        int house1_index = 0;
        int house2_index = 0;
        int saloon_index = 0;
        int cabin_index = 0;
        int bush_index = 0;
        int board_index = 0;
        int cactus1_index = 0;
        int cactus2_index = 0;
        int plant_index = 0;
        int straw_index = 0;
        for (int i = 0; i < [m_Objects count]; i++ )
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case H_OINFO_HOUSE1:                
                    m_House1[house1_index] = [[params objectAtIndex:1] intValue];
                    house1_index++;             
                    break;	
                case H_OINFO_HOUSE2:                
                    m_House2[house2_index] = [[params objectAtIndex:1] intValue];
                    house2_index++;                
                    break;
                case H_OINFO_BARN:
                    m_Saloon[saloon_index] = [[params objectAtIndex:1] intValue];
                    saloon_index++;
                    break;
                case H_OINFO_CABIN:
                    m_Cabin[cabin_index] = [[params objectAtIndex:1] intValue];
                    cabin_index++;
                    break;
                case H_OINFO_BUSHES_U:
                    m_Bush[bush_index] = [[params objectAtIndex:1] intValue];
                    bush_index++;
                    break;
                case H_OINFO_BILLBOARD1:                
                    m_BillBoard[board_index] = [[params objectAtIndex:1] intValue];
                    board_index++;                
                    break;	
                case H_OINFO_TREE1:                
                    m_Cactus1[cactus1_index] = [[params objectAtIndex:1] intValue];
                    cactus1_index++;                
                    break;
                case H_OINFO_TREE2:                
                    m_Cactus2[cactus2_index] = [[params objectAtIndex:1] intValue];
                    cactus2_index++; 
                    break;
                case H_OINFO_ROCK1:                
                    m_Plant[plant_index] = [[params objectAtIndex:1] intValue];
                    plant_index++;                
                    break;
                case H_OINFO_ROCK2:                
                    m_Straw[straw_index] = [[params objectAtIndex:1] intValue];
                    straw_index++;  
                    break;
            }
        }
        
        houseSprite1 = [CCSprite spriteWithFile:@"house_2_OR_ipad.png"];
        [self addChild:houseSprite1 z:2];
        houseSprite1.visible = NO;  
        
        houseSprite2 = [CCSprite spriteWithFile:@"house_2_OR_ipad.png"];
        [self addChild:houseSprite2 z:1];
        houseSprite2.visible = NO;
        
        saloonSprite = [CCSprite spriteWithFile:@"saloon_OR_ipad.png"];
        [self addChild:saloonSprite z:3];
        saloonSprite.visible = NO;
        
        cabinSprite = [CCSprite spriteWithFile:@"Cabin_1_ipad.png"];
        [self addChild:cabinSprite z:0];
        cabinSprite.visible = NO;
        
        bushesUSprite = [CCSprite spriteWithFile:@"Bush_OR_ipad.png"];
        [self addChild:bushesUSprite z:0];
        bushesUSprite.visible = NO;
        
        billBoard1Sprite = [CCSprite spriteWithFile:@"Boarding_OR_ipad.png"];
        [self addChild:billBoard1Sprite z:2];
        billBoard1Sprite.visible = NO;
        
        cactus1Sprite = [CCSprite spriteWithFile:@"cactus_1_OR_ipad.png"];
        [self addChild:cactus1Sprite z:4];
        cactus1Sprite.visible = NO;
        
        cactus2Sprite = [CCSprite spriteWithFile:@"cactus_2_OR_ipad.png"];
        [self addChild:cactus2Sprite z:4];
        cactus2Sprite.visible = NO;
        
        plantSprite = [CCSprite spriteWithFile:@"plant_1_OR_ipad.png"];
        [self addChild:plantSprite z:0];
        plantSprite.visible = NO;
        
        strawSprite = [CCSprite spriteWithFile:@"Strawball_1_OR_ipad.png"];
        [self addChild:strawSprite z:0];
        strawSprite.visible = NO;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    //[self showBillBoard:runDistance];
    [self showBush:runDistance];
    [self showCabin:runDistance];
    [self showCactus1:runDistance];
    [self showCactus2:runDistance];
    [self showHouse1:runDistance];
    [self showHouse2:runDistance];
    [self showPlant:runDistance];
    [self showSaloon:runDistance];
    [self showStraw:runDistance];
    
}
- (void) showHouse1:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_House1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    houseSprite1.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        houseSprite1.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        houseSprite1.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200.0*2.1333333, 0), CGPointMake(361.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361.0*2.1333333, 148.0*2.4));//b
        houseSprite1.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showHouse2:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_House2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    houseSprite2.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        houseSprite2.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        houseSprite2.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200.0*2.1333333, 0), CGPointMake(361.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361.0*2.1333333, 148.0*2.4));//b
        houseSprite2.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showSaloon:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Saloon[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    saloonSprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        saloonSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        saloonSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200.0*2.1333333, 0), CGPointMake(361.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361.0*2.1333333, 148.0*2.4));//b
        saloonSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        saloonSprite.scale = saloonSprite.scale * 3;
    }
}
- (void) showCabin:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Cabin[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    cabinSprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        cabinSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        cabinSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200.0*2.1333333, 0), CGPointMake(361.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361.0*2.1333333, 148.0*2.4));//b
        cabinSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
        cabinSprite.scale = cabinSprite.scale * 4;
    }
}
- (void) showBush:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Bush[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    bushesUSprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        bushesUSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        bushesUSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200.0*2.1333333, 0), CGPointMake(361.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361.0*2.1333333, 148.0*2.4));//b
        bushesUSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
    }
}
- (void) showBillBoard:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_BillBoard[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    billBoard1Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        billBoard1Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        billBoard1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200.0*2.1333333, 0), CGPointMake(361.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361.0*2.1333333, 148.0*2.4));//b
        billBoard1Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
    }
}
- (void) showCactus1:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Cactus1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    cactus1Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        cactus1Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        cactus1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200.0*2.1333333, 0), CGPointMake(361.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361.0*2.1333333, 148.0*2.4));//b
        cactus1Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showCactus2:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Cactus2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    cactus2Sprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        cactus2Sprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        cactus2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200.0*2.1333333, 0), CGPointMake(361.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361.0*2.1333333, 148.0*2.4));//b
        cactus2Sprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}

- (void) showPlant:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Plant[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    plantSprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        plantSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        plantSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200.0*2.1333333, 0), CGPointMake(361.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361.0*2.1333333, 148.0*2.4));//b
        plantSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1 * 2;
    }
}
- (void) showStraw:(int)runDistance
{
    int diff_BusHouse = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_Straw[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusHouse = diff[i];
        } else {
            diff_BusHouse = MIN(diff_BusHouse, diff[i]);                
        }
    }
    strawSprite.visible = NO;
    if (diff_BusHouse > 0 && diff_BusHouse < SHOW_LIMIT) {
        strawSprite.visible = YES;
        
        float ss = 94.8;
        float sum = -200.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusHouse / 10) {
                sum += ss * 0.831 / 10 * (diff_BusHouse % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusHouse / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusHouse % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;  
        strawSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-200.0*2.1333333, 0), CGPointMake(361.0*2.1333333, 148.0*2.4));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(361.0*2.1333333, 148.0*2.4));//b
        strawSprite.scale = 0.8 * (dist2 + 0.0667 * dist1) / 1.0667 / dist1;
    }
}
@end
