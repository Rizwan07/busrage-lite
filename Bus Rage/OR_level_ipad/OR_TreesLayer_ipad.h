//
//  CS_TreesLayer_ipad.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CCLayer.h"

@interface OR_TreesLayer_ipad : CCLayer {
    CCSprite *cactus1Sprite;
    CCSprite *cactus2Sprite;
    CCSprite *bush1Sprite;
    CCSprite *bush2Sprite;
    
    int m_Cactus1[3];
    int m_Cactus2[3];
    int m_Bush1[3];
    int m_Bush2[3];
    
    NSArray*    m_Objects; 
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) showCactus1:(int)runDistance;
- (void) showCactus2:(int)runDistance;
- (void) showBush1:(int)runDistance;
- (void) showBush2:(int)runDistance;

@end
