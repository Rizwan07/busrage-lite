//
//  CS_RoadLayer_ipad.h
//  Bus Rage
//
//  Created by Muhammad Rizwan on 11/7/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "CCLayer.h"

@interface OR_RoadLayer_ipad : CCLayer {
    CCSprite *road1Sprite;
    CCSprite *road2Sprite;
    CCSprite *road3Sprite;
    CCSprite *road4Sprite;    
    int current_road;
    int runCount;
    
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) changeRoadSprite;

@end
