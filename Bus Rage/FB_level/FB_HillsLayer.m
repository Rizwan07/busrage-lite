//
//  FB_HillsLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import "FB_HillsLayer.h"

@implementation FB_HillsLayer
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        hill1Sprite = [CCSprite spriteWithFile:@"Seaguls_1_FB.png"];
        [self addChild:hill1Sprite z:2];
        hill1Sprite.visible = YES;
        hill1Sprite.position = ccp(250, 144);
        hill1Sprite.scale = 0.4;
        
        hill2Sprite = [CCSprite spriteWithFile:@"Seaguls_2_FB.png"];
        [self addChild:hill2Sprite z:1];
        hill2Sprite.visible = YES;
        hill2Sprite.position = ccp(100, 140);
        hill2Sprite.scale = 0.2;
        
        hill3Sprite = [CCSprite spriteWithFile:@"Seaguls_3_FB 2.png"];
        [self addChild:hill3Sprite z:1];
        hill3Sprite.visible = YES;
        hill3Sprite.position = ccp(100, 140);
        hill3Sprite.scale = 0.2;
        
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    if (runDistance < 10) {
        hill1Sprite.position = ccp(530, 270);
        hill2Sprite.position = ccp(300, 300);
        hill3Sprite.position = ccp(100, 400);
    }
    if (speed > 0) {
        CGPoint pos;
        pos = hill1Sprite.position;
        pos.x -= 0.5 * speed/2;
        hill1Sprite.position = pos;
        
        pos = hill2Sprite.position;
        pos.x -= 0.4 * speed/2;
        hill2Sprite.position = pos;
        
        pos = hill3Sprite.position;
        pos.x -= 0.3 * speed/2;
        hill3Sprite.position = pos;
    }
}

@end
