//
//  FB_BaseLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface FB_BaseLayer : CCLayer {
    
    CCSprite* m_groundSprite;
}
- (void) runMove:(int)runDistance speed:(int)speed;
@end
