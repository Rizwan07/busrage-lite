//
//  FB_RoadLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import "FB_RoadLayer.h"
#import "MainLayer.h"
#define SHOW_LIMIT 300

@implementation FB_RoadLayer

-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        road1Sprtie = [CCSprite spriteWithFile:@"road_FB_01.png"];
        [self addChild:road1Sprtie z:1];
        road2Sprtie = [CCSprite spriteWithFile:@"road_FB_02.png"];
        [self addChild:road2Sprtie z:1];
        road3Sprtie = [CCSprite spriteWithFile:@"road_FB_01.png"];
        [self addChild:road3Sprtie z:1];
        road4Sprtie = [CCSprite spriteWithFile:@"road_FB_02.png"];
        [self addChild:road4Sprtie z:1];
        road1Sprtie.position = road2Sprtie.position = road3Sprtie.position = road4Sprtie.position = ccp(size.width/2+10, 160);
        road1Sprtie.visible = road2Sprtie.visible = road3Sprtie.visible = road4Sprtie.visible = NO;
        road1Sprtie.scale = road2Sprtie.scale = road3Sprtie.scale = road4Sprtie.scale = 2;
        road1Sprtie.visible = YES;
        isStoped = NO;
        //        rampSprtie = [CCSprite spriteWithFile:@"Ramp_FB.png"];
        //        [self addChild:rampSprtie z:1];
        //        rampSprtie.position = ccp(443, 163);
        //        rampSprtie.visible = YES;
        //        rampSprtie.scale = 0.8;
        
        
        shipSprite = [CCSprite spriteWithFile:@"Ferry_FB.png"];
        [self addChild:shipSprite z:-1];
        shipSprite.position = ccp(600, 180);
        shipSprite.visible = YES;
        
        current_road = 1;
        runCount = 0;
        
    }
	return self;
}

- (void) dealloc
{
	[super dealloc];
}

- (void) setMainLayer:(MainLayer*) _mainLayer
{
    mainLayer = _mainLayer;
}

- (void) changeRoadSprite
{
    current_road++;
    if (current_road == 5)
        current_road = 1;
    road1Sprtie.visible = road2Sprtie.visible = road3Sprtie.visible = road4Sprtie.visible = NO;
    switch (current_road) {
        case 1:
            road1Sprtie.visible = YES;
            break;
        case 2:
            road2Sprtie.visible = YES;
            break;
        case 3:
            road3Sprtie.visible = YES;
            break;
        case 4:
            road4Sprtie.visible = YES;
            break;
        default:
            break;
    }
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    runCount++;
    if (runCount > 40000)
        runCount = 0;
    
    if (speed == 0)
        return;
    
    if (speed == 10) {
        [self changeRoadSprite];
    }
    if (speed == 8) {
        if (runCount % 2 == 0) {
            [self changeRoadSprite];
        }
    }
    if (speed == 6) {
        if (runCount % 2 == 0) {
            [self changeRoadSprite];
        }
    }
    if (speed == 4) {
        if (runCount % 2 == 0) {
            [self changeRoadSprite];
        }
    }
    if (speed == 2) {
        if (runCount % 4 == 0) {
            [self changeRoadSprite];
        }
    }
    
    if (speed > 0) {
        CGPoint pos;
        pos = shipSprite.position;
        pos.x -= 0.05 * speed/2;
        shipSprite.position = pos;
        NSLog(@"-- %f",shipSprite.position.x);
        
    }
    if (!isStoped && shipSprite.position.x<400) {
        isStoped = YES;
        [mainLayer crashed];
    }
    
}

@end
