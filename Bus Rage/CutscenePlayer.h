//
//  CutscenePlayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/20/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MediaPlayer/MPMoviePlayerController.h"

@protocol ScenePlayerDelegate 

- (void) didFinishedPlay;

@end

@interface CutscenePlayer : NSObject
{
	MPMoviePlayerController* theMovie;
    int m_current_num;
}

@property (nonatomic, retain) MPMoviePlayerController* theMovie;
@property (nonatomic, retain) id<ScenePlayerDelegate> delegate;

-(void) load:(int)num forIpad:(BOOL)isiPad;
-(void) play;

+(CutscenePlayer*) sharedInstance;
-(void) myMovieFinishedCallback: (NSNotification*) aNotification;
@end
