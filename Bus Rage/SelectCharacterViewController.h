//
//  SelectCharacterViewController.h
//  Bus Rage
//
//  Created by Jin Tie on 5/9/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SelectLevelViewController.h"
#import "SimpleAudioEngine.h"

@interface SelectCharacterViewController : UIViewController <UITextFieldDelegate> {
    SelectLevelViewController *_selectlevelViewController;
}

@property (retain) SelectLevelViewController *selectlevelViewController;
@property (nonatomic, retain) IBOutlet UILabel *titleLabel;
@property (nonatomic, retain) IBOutlet UILabel *maleDriverLabel;
@property (nonatomic, retain) IBOutlet UITextField *enterNameField;
@property (nonatomic, retain) IBOutlet UIImageView *driverImageView;
@property (nonatomic, retain) IBOutlet UIButton *maleThumbnailButton;
@property (nonatomic, retain) IBOutlet UIButton *femaleThumbnailButton;
@property (nonatomic, retain) IBOutlet UIButton *nextButton;
- (IBAction)backTapped:(id)sender;
- (IBAction)nextTapped:(id)sender;
- (IBAction)maleTapped:(id)sender;
- (IBAction)femaleTapped:(id)sender;
@end
