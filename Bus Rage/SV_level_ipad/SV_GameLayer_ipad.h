//
//  SV_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SV_HousesLayer_ipad.h"
#import "SV_FenceLayer_ipad.h"
#import "SV_RoadLayer_ipad.h"
#import "SV_TreesLayer_ipad.h"
#import "SV_HillsLayer_ipad.h"
#import "SV_Horizon1Layer_ipad.h"
#import "SV_BaseLayer_ipad.h"

@interface SV_GameLayer_ipad : CCLayer {
    SV_HousesLayer_ipad *sv_houseLayer;
    SV_FenceLayer_ipad *sv_fenceLayer;
    SV_RoadLayer_ipad *sv_roadLayer;
    SV_TreesLayer_ipad *sv_treeLayer;
    SV_HillsLayer_ipad *sv_hillLayer;
    SV_Horizon1Layer_ipad *sv_horizon1Layer;
    SV_BaseLayer_ipad *sv_baseLayer;
    CCSprite *cloud_dustSprite;
    int m_timecount;
}
- (void) runMove:(int)runDistance speed:(int)speed;
@end
