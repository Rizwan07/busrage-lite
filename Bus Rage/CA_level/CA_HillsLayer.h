//
//  CA_HillsLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 6/11/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface CA_HillsLayer : CCLayer {
    CCSprite *hill1Sprite;
    CCSprite *hill2Sprite;
}
- (void) runMove:(int)runDistance speed:(int)speed;
@end
