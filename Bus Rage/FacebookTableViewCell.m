//
//  FacebookTableViewCell.m
//  Bus Rage
//
//  Created by Jin Tie on 5/27/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "FacebookTableViewCell.h"

@interface FacebookTableViewCell (Private)
- (void)_refreshFromFile;
+ (FacebookTableViewCell *)_cellFromNib;
@end

@implementation FacebookTableViewCell
@synthesize posterImageView=_posterImageView, titleLabel=_titleLabel, activityIndicator=_activityIndicator;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
+ (CGFloat)cellHeight {
	static CGFloat sHeight = 0.0f;
	if (sHeight == 0.0f) {
		sHeight = [[self _cellFromNib] frame].size.height;
	}
	return sHeight;
}
+ (FacebookTableViewCell *)cellWithReuseIdentifier:(NSString *)reuseIdentifier {
	FacebookTableViewCell * cell = [FacebookTableViewCell _cellFromNib];
	return cell;
}
-(BOOL)isComplete
{
    return _completeLoading; 
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context {
    [self _refreshFromFile];
}

- (void)dealloc {
	self.file = nil;
	[_activityIndicator release];
	[_titleLabel release];
	[_posterImageView release];
    [super dealloc];
}

#pragma mark -
#pragma mark Accessors
- (void)setFile:(NSDictionary *)file {
    _file = file; 
    [self _refreshFromFile];
}

- (NSDictionary *)file {
	return _file;
}

- (void)setEven:(BOOL)even {
	self.backgroundView.hidden = !even;
}

@end

@implementation FacebookTableViewCell (Private)
+ (FacebookTableViewCell *)_cellFromNib {
    
    
	NSArray * array; 
    
    array = [[NSBundle mainBundle] loadNibNamed:@"FacebookTableViewCell" owner:nil options:nil];
    
    
	//ZSAssert([array count] == 1, @"Wrong number of objects in NIB file !");
	//ZSAssert([[array lastObject] isKindOfClass:[ZSMovieTableViewCell class]], @"Unexpected object in NIB file !");
	FacebookTableViewCell * cell = (FacebookTableViewCell *)[array lastObject];
	cell.backgroundView = [[[UIView alloc] initWithFrame:cell.bounds] autorelease];
	//cell.backgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ZSMovieTableViewCellBackground1.png"]];
	cell.backgroundView.opaque = NO;
    
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
    cell.textLabel.numberOfLines = 2;
    
	cell.selectedBackgroundView = [[[UIView alloc] initWithFrame:cell.bounds] autorelease];
	//cell.selectedBackgroundView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"ZSMovieTableViewCellSelectedBackground.png"]];
	cell.selectedBackgroundView.opaque = NO;
	return cell;
}

- (void)_refreshFromFile {
    if (self.file != nil){
        self.titleLabel.text = [self.file objectForKey:@"name"]; 
        //NSLog(@"%@", self.titleLabel.text);
    }else{
        self.titleLabel.text = @""; 
    }
    
    if (self.file){
        if ([self.file objectForKey:@"id"]){
            [self.activityIndicator stopAnimating];
            
            _completeLoading = NO; 
            NSString *str = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture",[self.file objectForKey:@"id"]];
            NSURL* url = [NSURL URLWithString:str]; 
            
            NSOperationQueue *queue = [[NSOperationQueue new] autorelease];
            NSInvocationOperation *operation = [[NSInvocationOperation alloc] initWithTarget:self
                                                                                    selector:@selector(loadImage:)object:url]; 
            [queue addOperation:operation]; 
            [operation release]; 
            
            [self.activityIndicator startAnimating];
        }
    }
    
	if (self.file){
        NSMutableString * subtitle = [[NSMutableString alloc] init];
        
        if ([self.file objectForKey:@"now"]){
            [subtitle appendFormat:@"%@", [self.file objectForKey:@"now"] ];
        }
        
        if ([self.file objectForKey:@"next"]){
            [subtitle appendFormat:@"%@", [self.file objectForKey:@"next"] ];
        }
        //self.subtitleLabel.text = subtitle;
        [subtitle release];
        
    }else{
        //self.subtitleLabel.text = @""; 
    }
    
}

-(void)loadImage:(NSURL*)url{
    NSData* imageData = [[NSData alloc] initWithContentsOfURL:url]; 
    UIImage* image = [[[UIImage alloc]initWithData:imageData]autorelease]; 
    [imageData release]; 
    [self performSelectorOnMainThread:@selector(displayImage:)withObject:image waitUntilDone:NO]; 
}

-(void)displayImage:(UIImage *)image{
    [self.posterImageView setImage:image]; 
    [self.activityIndicator stopAnimating];
    _completeLoading = YES; 
}
@end
