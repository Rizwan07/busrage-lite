//
//  SC_RoadLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface SC_RoadLayer : CCLayer {
    CCSprite *road1Sprtie;
    CCSprite *road2Sprtie;
    CCSprite *road3Sprtie;
    CCSprite *road4Sprtie;
    int current_road;
    int runCount;
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) changeRoadSprite;
@end
