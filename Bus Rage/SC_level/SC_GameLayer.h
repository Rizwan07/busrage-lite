//
//  SC_GameLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"
#import "SC_HousesLayer.h"
#import "SC_FenceLayer.h"
#import "SC_RoadLayer.h"
#import "SC_TreesLayer.h"
#import "SC_HillsLayer.h"
#import "SC_Horizon1Layer.h"
#import "SC_BaseLayer.h"

@interface SC_GameLayer : CCLayer {
    SC_HousesLayer *sc_houseLayer;
    SC_FenceLayer *sc_fenceLayer;
    SC_RoadLayer *sc_roadLayer;
    SC_TreesLayer *sc_treeLayer;
    SC_HillsLayer *sc_hillLayer;
    SC_Horizon1Layer *sc_horizon1Layer;
    SC_BaseLayer *sc_baseLayer;
    CCSprite *cloud_dustSprite;
    int m_timecount;
}
- (void) runMove:(int)runDistance speed:(int)speed;
@end
