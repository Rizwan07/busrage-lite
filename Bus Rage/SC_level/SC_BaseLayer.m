//
//  SC_BaseLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 5/26/12.
//  Copyright 2012 k. All rights reserved.
//

#import "SC_BaseLayer.h"


@implementation SC_BaseLayer
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        m_groundSprite = [CCSprite spriteWithFile:@"ground_SV_ipad.png"];
        m_groundSprite.position = ccp(1024, 384);
        m_groundSprite.scale = 2;
        [self addChild:m_groundSprite z:0];
        
        CCSprite *skySprite = [CCSprite spriteWithFile:@"ground_sky_ipad.png"];
        skySprite.position = ccp(240*2.1333333, 235*2.4);
        skySprite.scale = 2;
        [self addChild:skySprite z:1];        
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{    
    CGPoint pos = m_groundSprite.position;
    
    if (speed > 0) {
        pos.y -= speed;
        pos.x -= speed*2;
        if (pos.y < -10 || pos.x < 0) {
            pos.y = 384; pos.x = 1024;
        }
        m_groundSprite.position = pos;
    }
}
@end
