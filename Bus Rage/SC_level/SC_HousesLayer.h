//
//  SC_HousesLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface SC_HousesLayer : CCLayer {
    CCSprite *school1Sprite;
    CCSprite *school2Sprite;
    CCSprite *school3Sprite;
    CCSprite *house1Sprite;
    CCSprite *house2Sprite; 
    CCSprite *basketballSprite;
                                   
    int m_School1[4];
    int m_School2[4];
    int m_School3[4];
    int m_House1[3];
    int m_House2[3];
    int m_Basketball[3];
    
    NSArray*    m_Objects; 
}
- (void) runMove:(int)runDistance speed:(int)speed;
- (void) showSchool1:(int)runDistance;
- (void) showSchool2:(int)runDistance;
- (void) showSchool3:(int)runDistance;
- (void) showHouse1:(int)runDistance;
- (void) showHouse2:(int)runDistance;
- (void) showBasketball:(int)runDistance;
@end
