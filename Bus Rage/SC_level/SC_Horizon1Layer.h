//
//  SC_Horizon1Layer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface SC_Horizon1Layer : CCLayer {
    CCSprite *horizonSprite;
}
- (void) runMove:(int)runDistance speed:(int)speed;
@end
