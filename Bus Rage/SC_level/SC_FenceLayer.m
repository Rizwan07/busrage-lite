//
//  SC_FenceLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import "SC_FenceLayer.h"
#import "MainLayer.h"
#define SHOW_LIMIT 170

@implementation SC_FenceLayer
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        CGSize size = [[CCDirector sharedDirector] winSize];
        
        roadFence1 = [CCSprite spriteWithFile:@"fence_roadside_SC_01.png"];
        [self addChild:roadFence1 z:1];   
        roadFence2 = [CCSprite spriteWithFile:@"fence_roadside_SC_02.png"];
        [self addChild:roadFence2 z:1]; 
        roadFence3 = [CCSprite spriteWithFile:@"fence_roadside_SC_03.png"];
        [self addChild:roadFence3 z:1]; 
        roadFence4 = [CCSprite spriteWithFile:@"fence_roadside_SC_04.png"];
        [self addChild:roadFence4 z:1]; 
        roadFence1.position = roadFence2.position = roadFence3.position = roadFence4.position = ccp(size.width/2, 160);
        roadFence1.visible = roadFence2.visible = roadFence3.visible = roadFence4.visible = NO;
        roadFence1.visible = YES;
        
        current_fence = 1;
        runCount = 0;
        //SC:14,15,16,17,18,6,0(tree1),1(tree2)       
        m_Objects = [NSArray arrayWithObjects: 
                     @"1, 10",//tree2
                     //@"18, 30",//hoard
                     @"14, 50",//light1
                     @"0, 100",//tree1
                     @"15, 150",//light2
                     @"1, 200",//tree2
                     @"16, 250",//light3
                     @"6, 300",//roadsign
                     @"17, 350",//light4
                     
                     @"0, 400",//tree1
                     @"14, 450",//light1
                     @"1, 480",//tree2
                     @"18, 500",//hoard
                     @"15, 550",//light2
                     @"0, 580",//tree1
                     @"6, 600",//roadsign
                     @"16, 650",//light3
                     @"0, 700",//tree1
                     @"17, 750",//light4
                     
                     //@"18, 800",//hoard
                     @"14, 850",//light1
                     @"6, 900",//roadsign
                     @"15, 950",//light2
                     @"1, 1000",//tree2
                     @"16, 1050",//light3
                     @"17, 1150",//light4                      
                     nil];
        int light1_index = 0;
        int light2_index = 0;
        int light3_index = 0;
        int light4_index = 0;
        int hoard_index = 0;
        int roadsign_index = 0;
        int tree1_index = 0;
        int tree2_index = 0;
        for (int i = 0; i < [m_Objects count]; i++ )
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case F_OINFO_LIGHT1:                
                    m_streetLight1[light1_index] = [[params objectAtIndex:1] intValue];
                    light1_index++;                
                    break;
                case F_OINFO_LIGHT2:                
                    m_streetLight2[light2_index] = [[params objectAtIndex:1] intValue];
                    light2_index++;                
                    break;
                case F_OINFO_LIGHT3:                
                    m_streetLight3[light3_index] = [[params objectAtIndex:1] intValue];
                    light3_index++;                
                    break;
                case F_OINFO_LIGHT4:                
                    m_streetLight4[light4_index] = [[params objectAtIndex:1] intValue];
                    light4_index++;                
                    break;                    
                case F_OINFO_HOARD:                
                    m_hoarding[hoard_index] = [[params objectAtIndex:1] intValue];
                    hoard_index++;                
                    break;
                case F_OINFO_ROADSIGN:                
                    m_roadSign[roadsign_index] = [[params objectAtIndex:1] intValue];
                    roadsign_index++;                
                    break;
                case F_OINFO_FENCE1_L:
                    m_tree1[tree1_index] = [[params objectAtIndex:1] intValue];
                    tree1_index++;
                    break;
                case F_OINFO_FENCE2_L:
                    m_tree2[tree2_index] = [[params objectAtIndex:1] intValue];
                    tree2_index++;
                    break;
            }
        }       
        
        streetLight1Sprite = [CCSprite spriteWithFile:@"Street light_SC.png"];
        [self addChild:streetLight1Sprite z:0];        
        streetLight1Sprite.visible = NO;
        
        streetLight2Sprite = [CCSprite spriteWithFile:@"Street light_SC.png"];
        [self addChild:streetLight2Sprite z:0];        
        streetLight2Sprite.visible = NO;
        
        streetLight3Sprite = [CCSprite spriteWithFile:@"Street light_SC.png"];
        [self addChild:streetLight3Sprite z:0];        
        streetLight3Sprite.visible = NO;
        
        streetLight4Sprite = [CCSprite spriteWithFile:@"Street light_SC.png"];
        [self addChild:streetLight4Sprite z:0];        
        streetLight4Sprite.visible = NO;
        
        hoardingSprite = [CCSprite spriteWithFile:@"Hoarding_SC.png"];
        [self addChild:hoardingSprite z:0];        
        hoardingSprite.visible = NO;
        
        roadSignSprite = [CCSprite spriteWithFile:@"Road_sign_SC.png"];
        [self addChild:roadSignSprite z:0];        
        roadSignSprite.visible = NO;
        
        tree1Sprite = [CCSprite spriteWithFile:@"Tree_1_SC.png"];
        [self addChild:tree1Sprite z:0];
        tree1Sprite.visible = NO;
        
        tree2Sprite = [CCSprite spriteWithFile:@"Tree_5_SC.png"];
        [self addChild:tree2Sprite z:0];
        tree2Sprite.visible = NO;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    [self showFence:speed];
    [self showHoarding:runDistance];
    [self showRoadSign:runDistance];
    [self showStreetLight1:runDistance];
    [self showStreetLight2:runDistance];
    [self showStreetLight3:runDistance];
    [self showStreetLight4:runDistance];
    [self showTree1:runDistance];
    [self showTree2:runDistance];
}
- (void) changeFenceSprite
{
    current_fence++;
    if (current_fence == 5)
        current_fence = 1;
    roadFence1.visible = roadFence2.visible = roadFence3.visible = roadFence4.visible = NO;
    switch (current_fence) {
        case 1:
            roadFence1.visible = YES;
            break;
        case 2:
            roadFence2.visible = YES;
            break;
        case 3:
            roadFence3.visible = YES;
            break;
        case 4:
            roadFence4.visible = YES;
            break;
        default:
            break;
    }
}

- (void) showFence:(int)speed
{  
    runCount++;
    if (runCount > 40000)
        runCount = 0;
    
    if (speed == 0)
        return;
    
    if (speed == 10) {
        [self changeFenceSprite];
    }
    if (speed == 8) {
        if (runCount % 2 == 0) {
            [self changeFenceSprite];
        }
    }
    if (speed == 6) {
        if (runCount % 2 == 0) {
            [self changeFenceSprite];
        }
    }   
    if (speed == 4) {
        if (runCount % 2 == 0) {
            [self changeFenceSprite];
        }
    }
    if (speed == 2) {
        if (runCount % 2 == 0) {
            [self changeFenceSprite];
        }
    }
    
}
- (void) showStreetLight1:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_streetLight1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    streetLight1Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        streetLight1Sprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        streetLight1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        streetLight1Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showStreetLight2:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_streetLight2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    streetLight2Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        streetLight2Sprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        streetLight2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        streetLight2Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showStreetLight3:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_streetLight3[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    streetLight3Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        streetLight3Sprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        streetLight3Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        streetLight3Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showStreetLight4:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_streetLight4[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    streetLight4Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        streetLight4Sprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        streetLight4Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        streetLight4Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showHoarding:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_hoarding[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    hoardingSprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        hoardingSprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        hoardingSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        hoardingSprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showRoadSign:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_roadSign[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    roadSignSprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        roadSignSprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        roadSignSprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        roadSignSprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showTree1:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_tree1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    tree1Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        tree1Sprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        tree1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        tree1Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
- (void) showTree2:(int)runDistance
{
    int diff_BusBush = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_tree2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusBush = diff[i];
        } else {
            diff_BusBush = MIN(diff_BusBush, diff[i]);                
        }
    }
    tree2Sprite.visible = NO;
    if (diff_BusBush > 0 && diff_BusBush < SHOW_LIMIT) {
        tree2Sprite.visible = YES;
        
        float ss = 82.0;
        float sum = -80.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusBush / 10) {
                sum += ss * 0.831 / 10 * (diff_BusBush % 10);            
                break;
            }
            ss = ss * 0.831;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusBush / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusBush % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.y = sum1;  
        cur_pos.x = sum;
        tree2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-80.0, 0), CGPointMake(405, 148.0));//a
        float dist2 = ccpDistance(cur_pos, CGPointMake(405, 148.0));//b
        tree2Sprite.scale = 0.5 * (dist2 + 0.25 * dist1) / 1.25 / dist1 * 2;
    }
}
@end
