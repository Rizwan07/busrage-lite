//
//  RootViewController.m
//  Bus Rage
//
//  Created by Jin Tie on 5/9/12.
//  Copyright k 2012. All rights reserved.
//

//
// RootViewController + iAd
// If you want to support iAd, use this class as the controller of your iAd
//

#import "cocos2d.h"

#import "RootViewController.h"
#import "GameConfig.h"
#import "MainLayer.h"
#import "MainLayer_iPad.h"
#import "CutscenePlayer.h"

@implementation RootViewController
//@synthesize homeButton = _homeButton;
@synthesize m_bAccelerator, m_bBrake, m_bSteeringLeft, m_bSteeringRight;
@synthesize m_bBusTargetHit;
@synthesize m_bStop;
//@synthesize driverImage;
//@synthesize target1Image, target2Image, target3Image;
@synthesize target1ImageX, target2ImageX, target3ImageX, target4ImageX, target5ImageX, target6ImageX, target7ImageX;
//@synthesize faceBookR;

@synthesize m_bMessageShow;
/*
 // The designated initializer.  Override if you create the controller programmatically and want to perform customization that is not appropriate for viewDidLoad.
 - (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil])) {
	// Custom initialization
	}
	return self;
 }
 */

/*
 // Implement loadView to create a view hierarchy programmatically, without using a nib.
 - (void)loadView {
 }
 */

- (void)setupCocos2D {
    EAGLView *glView = [EAGLView viewWithFrame:self.view.bounds
								   pixelFormat:kEAGLColorFormatRGB565	// kEAGLColorFormatRGBA8
								   depthFormat:0                        // GL_DEPTH_COMPONENT16_OES
						];
    glView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
    [self.view insertSubview:glView atIndex:0];
    [[CCDirector sharedDirector] setOpenGLView:glView];
    
    self.view.transform = CGAffineTransformIdentity; 
    self.view.transform = CGAffineTransformMakeRotation((M_PI * (-90)/180.0));
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        self.view.bounds = CGRectMake(0.0, 0.0, 480.0, 320.0);
    }
    else {
        self.view.bounds = CGRectMake(0.0, 0.0, 1024.0, 768.0);
    }
    
    //CCScene *scene = [HelloWorldLayer sceneWithCalendar:1 rootViewController:self];    
    //[[CCDirector sharedDirector] runWithScene:scene];
    
    //load image
    
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"character_ani.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"obstacleAnim.plist"];
    [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"StoreAttack.plist"];
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"Bus.plist"];
        
        CCScene *scene = [MainLayer scene:self];     

        [[CCDirector sharedDirector] stopAnimation];
        [[CCDirector sharedDirector] runWithScene:scene];
    }
    else {
        [[CCSpriteFrameCache sharedSpriteFrameCache] addSpriteFramesWithFile:@"Bus_ipad.plist"];
        CCScene *scene = [MainLayer_iPad scene:self];     
        
        [[CCDirector sharedDirector] stopAnimation];
        [[CCDirector sharedDirector] runWithScene:scene];
    }
    

}
- (NSString*) getImageName:(int)Num
{
    NSString *sLine = [m_imageObjects objectAtIndex:Num];
    NSArray *params = [sLine componentsSeparatedByString:@", "];
    return [params objectAtIndex:0];
}
- (void) viewWillAppear:(BOOL)animated
{
    if ([GameManager sharedGameManager].m_bEnabledMusic) {       
        [[SimpleAudioEngine sharedEngine] resumeBackgroundMusic];
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isIpad"];
        CCScene *scene = [MainLayer scene:self];    
        [[CCDirector sharedDirector] replaceScene:scene];
    }
    else {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isIpad"];
        CCScene *scene = [MainLayer_iPad scene:self];
        [[CCDirector sharedDirector] replaceScene:scene];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    //top bar image setting
    if ([GameManager sharedGameManager].m_bMale) {
        [driverImage setImage:[UIImage imageNamed:@"bus driver_male_correction_render.png"]];
    } else {
        [driverImage setImage:[UIImage imageNamed:@"bus driver_female_render.png"]];
    }
    target1Image.image = target2Image.image = target3Image.image = target4Image.image = target5Image.image = target6Image.image = nil;    
    
    if ([[GameManager sharedGameManager] isSelectAll]) {
        switch ([[GameManager sharedGameManager] m_levelNum]) {
            case 1:
                [target1Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum1-1]]];
                [target2Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum2-1]]];
                break;
            case 2:
                [target1Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum1-1]]];
                [target2Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum2-1]]];
                break;
            case 3:
                [target1Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum1-1]]];
                [target2Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum2-1]]];
                [target3Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum3-1]]];
                break;
            case 4:
                [target1Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum1-1]]];
                [target2Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum2-1]]];
                break;
            case 5:
                [target1Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum1-1]]];
                [target2Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum2-1]]];
                break;
            case 6:
                [target1Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum1-1]]];
                [target2Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum2-1]]];
                [target3Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum3-1]]];
                [target4Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum4-1]]];
                break;
            case 7:
                [target1Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum1-1]]];
                [target2Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum2-1]]];
                [target3Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum3-1]]];
                [target4Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum4-1]]];
//                [target5Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum5-1]]];
//                [target6Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum6-1]]];
                break;
            case 8:
                [target1Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum1-1]]];
                [target2Image setImage:[UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum2-1]]];
                break;
            default:
                break;
        }
    }
    else {
        NSString *objectId;
        target1Image.image = [UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum1-1]];
        objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum1 - 1];
        if (![objectId isEqualToString:@""]) {
            target1Image.image = [GameManager sharedGameManager].fbTargetImage1;
        }
        if ([self currentTargetSelectNum] >= 2) {
            target2Image.image = [UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum2-1]];
            objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum2 - 1];
            if (![objectId isEqualToString:@""]) {
                target2Image.image = [GameManager sharedGameManager].fbTargetImage2;
            }
        }
        if ([self currentTargetSelectNum] >= 3) {
            target3Image.image = [UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum3-1]];
            objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum3 - 1];
            if (![objectId isEqualToString:@""]) {
                target3Image.image = [GameManager sharedGameManager].fbTargetImage3;
            }
        }
        if ([self currentTargetSelectNum] >= 4) {
            target4Image.image = [UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum4-1]];
            objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum4 - 1];
            if (![objectId isEqualToString:@""]) {
                target4Image.image = [GameManager sharedGameManager].fbTargetImage4;
            }
        }
//        if ([self currentTargetSelectNum] >= 5) {
//            target5Image.image = [UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum5-1]];
//            objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum5 - 1];
//            if (![objectId isEqualToString:@""]) {
//                target5Image.image = [GameManager sharedGameManager].fbTargetImage5;
//            }
//        }
//        if ([self currentTargetSelectNum] >= 6) {
//            target6Image.image = [UIImage imageNamed:[self getImageName:[GameManager sharedGameManager].targetNum6-1]];
//            objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:[GameManager sharedGameManager].targetNum6 - 1];
//            if (![objectId isEqualToString:@""]) {
//                target6Image.image = [GameManager sharedGameManager].fbTargetImage6;
//            }
//        }
    }
    m_bGameEnd = YES;    
    m_bMessageShow = NO;
    [super viewWillAppear:animated];    
}
- (void) viewWillDisappear:(BOOL)animated {
}
// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.
- (void)viewDidLoad {
    [super viewDidLoad];
    
    //_showingUI = YES;
    //_mailCenterOrig = _mailButton.center;
    //_homeCenterOrig = _homeButton.center;
    m_imageObjects = [[NSArray alloc] initWithObjects:
                      @"latin_female_render.png, LATIN",                //1 //female_latin
                      @"latin_male_render.png, LATIN",                  //2 //mexican   
                      
                      @"american_female_render_blonde.png, AMERICAN",   //3
                      @"american_female_render(brunette).png, AMERICAN",//4 //female_american
                      @"american_female_render(redhead).png, AMERICAN", //5
                      
                      @"american_male_render_blonde.png, AMERICAN",     //6
                      @"american_male_render(brunette).png, AMERICAN",  //7 //American
                      @"american_male_render(redhead).png, AMERICAN",   //8
                      
                      @"Arab_female_render.png, ARAB",                  //9 //female_arab
                      @"Arab_male_render.png, ARAB",                    //10 //Arab
                      @"Asian_female_character_background.png, ASIAN",  //11 //female_asian                         
                      @"Chinese_Character_backgound.png, ASIAN",        //12 //Asian                         
                      @"german_female_render.png, GERMAN",              //13 //female_german
                      @"german_male_render.png, GERMAN",                //14 //german
                      @"italian_female_render.png, ITALIAN",            //15 //female_italian
                      @"italian_male_line_render.png, ITALIAN",         //16 //italian
                      @"AfricanAmerican_female_render.png, AFRICAN",    //17 //female_African_american
                      @"AfricanAmerican_male_render.png, AFRICAN",      //18 //African 
                      @"russian_female_rendered.png, RUSSIAN",          //19 //female_russian
                      @"russian_male_render.png, RUSSIAN",              //20 //russian
                      @"nurse_female_render.png, NURSE",                //21 //female_nurse
                      @"nurse_male.png, NURSE",                         //22 //nurse
                      @"woman in suit_render.png, SUIT",                //23 //female_business_suit
                      @"man_in_suit_render.png, SUIT",                  //24 //man in suit
                      @"basketball_male.png, BASKETBALL",               //25 //Basketball
                      @"coverall_male_render.png, COVERALL",            //26 //Coverall
                      @"doctor_female_render.png, DOCTOR",              //27 //female_doctor
                      @"doctor_male_render.png, DOCTOR",                //28 //Doctor
                      @"football_male_render.png, FOOTBALL",            //29 //football
                      nil];
    [self setupCocos2D];
    checkXTimer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(updateStatus) userInfo:nil repeats:YES];
    isFirstCall = NO;
    isCutSceen = NO;
}

//- (void) gameEnd
//{
//    m_bGameEnd = NO;
//    UIAlertView* alert = [[UIAlertView alloc] initWithTitle:nil message:@"Success! Do you want to post at face book?" delegate:self cancelButtonTitle:@"YES" otherButtonTitles:@"NO", nil];	
//    //alert.
//    //[alert setMessage:@"Do you want to post at face book?"];
//    //[alert addButtonWithTitle:@"Ok"];
//    //[alert setDelegate:self];
//    //alert.transform = CGAffineTransformIdentity; 
//    //alert.transform = CGAffineTransformMakeRotation(()); 
//    //alert.bounds = CGRectMake(0.0, 0.0, 180, 120);    
//    [alert show];
////    while (alert.hidden == NO && alert.superview != nil) {
////        [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01f]];
////    }
//    
//    
//    [alert release]; 
//}
- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (buttonIndex == 0) {
        //return YES
        //[self facebookLogin];  
        m_bGameEnd = YES;
        //[self performSelector:@selector(homeTapped:)];
        [self performSelector:@selector(levelViewControllerToBack:)];
        //[self levelViewControllerToBack];
    } else if (buttonIndex == 1) {
        //return NO
        m_bGameEnd = YES;
        //[self performSelector:@selector(homeTapped:)];
        [self performSelector:@selector(levelViewControllerToBack:)];
        //[self levelViewControllerToBack];
    }
}
//- (IBAction)levelViewControllerToBack:(id)sender {
//    m_bStop = YES;
//    if ([GameManager sharedGameManager].m_bEnabledSound) {
//        [[SimpleAudioEngine sharedEngine] playEffect:@"Next_screen_swoosh.mp3"];
//    }
//    int count = [self.navigationController.viewControllers count];
//    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count - 4] animated:YES];
//}
- (void) levelViewControllerToBack
{
    self.target1ImageX.hidden = self.target2ImageX.hidden = self.target3ImageX.hidden  = self.target4ImageX.hidden = self.target5ImageX.hidden = self.target6ImageX.hidden = YES;
    
    if ([GameManager sharedGameManager].m_bEnabledMusic) {       
        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    }
    //m_bStop = YES;
    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Next_screen_swoosh.mp3"];
    }
    
//    if(self.faceBookR.isSessionValid)  {
//        [self challenge];
//    }
//    else {
        int count = [self.navigationController.viewControllers count];
        [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count - 3] animated:YES];
//    }
}

- (void)challenge {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *fbUserId = [[NSUserDefaults standardUserDefaults] objectForKey:@"fbUserId"];
    
    if (fbUserId != nil) {
        
        NSMutableURLRequest *request = [[NSMutableURLRequest alloc]init];
        
        [request setURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://developer.avenuesocial.com/azeemsal/busrage/application/json/json.php?"]]];
        
        //user_id=&user_name=&score=&level=&target_id=accesstoken=
        
        
        [request setHTTPMethod:@"POST"];
        //
        NSString *boundary = @"---------------------------14737809831466499882746641449";
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@",boundary];
        [request addValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        
        NSMutableData *body = [NSMutableData data];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [body appendData:[@"Content-Disposition: form-data; name=\"method\";\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[@"Content-Type: text/plain\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"play"]dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [body appendData:[@"Content-Disposition: form-data; name=\"user_id\";\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",fbUserId] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Disposition: form-data; name=\"driver_name\";\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[[NSString stringWithFormat:@"%@",[[GameManager sharedGameManager] m_DriverName]] dataUsingEncoding:NSUTF8StringEncoding]];
        //[body appendData:[[NSString stringWithFormat:@"%@",[[[GameManager sharedGameManager] fbProfileData] valueForKey:@"name"]]dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        [body appendData:[@"Content-Disposition: form-data; name=\"driver\";\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        if([GameManager sharedGameManager].m_bMale)
        {
            
            
            [body appendData:[[NSString stringWithFormat:@"male"]dataUsingEncoding:NSUTF8StringEncoding]];
        }
        else
        {
            [body appendData:[[NSString stringWithFormat:@"female"]dataUsingEncoding:NSUTF8StringEncoding]];
            
        }
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [body appendData:[@"Content-Disposition: form-data; name=\"score\";\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        //  [body appendData:[[NSString stringWithString:@"Content-Type: text/plain\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%d",[GameManager sharedGameManager].m_GameScore]dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [body appendData:[@"Content-Disposition: form-data; name=\"level\";\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        // [body appendData:[[NSString stringWithString:@"Content-Type: text/plain\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%d",[GameManager sharedGameManager].m_levelNum]dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        [body appendData:[@"Content-Disposition: form-data; name=\"user_name\";\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        // [body appendData:[[NSString stringWithString:@"Content-Type: text/plain\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData: [[NSString stringWithFormat:@"%@",[[[GameManager sharedGameManager]m_targetDic]valueForKey:@"user_name"]]dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
        [body appendData:[@"Content-Disposition: form-data; name=\"target_id\";\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        // [body appendData:[[NSString stringWithString:@"Content-Type: text/plain\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        
        NSLog(@"SBJSON String:%@",[[SBJSON new]stringWithObject:[[GameManager sharedGameManager] m_targetDic ]]);
        
        
        
        
        SBJSON *obj_Json = [SBJSON new];
        
        NSString *str = [[NSString alloc]init];
        str = [obj_Json stringWithObject:[GameManager sharedGameManager].m_targetDic];
        
        [body appendData:[[NSString stringWithFormat:@"%@",str]dataUsingEncoding:NSUTF8StringEncoding]];
        //[str release];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
        if([GameManager sharedGameManager].challengeId.length >0)
        {
            
            [body appendData:[@"Content-Disposition: form-data; name=\"challenge\";\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
            // [body appendData:[[NSString stringWithString:@"Content-Type: text/plain\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@",[GameManager sharedGameManager].challengeId]dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
            
            
            
            
        }
        
        
        [body appendData:[@"Content-Disposition: form-data; name=\"accesstoken\";\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
        // [body appendData:[[NSString stringWithString:@"Content-Type: application/octet-stream\r\n\r\n"] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@",[defaults objectForKey:@"ACCESS_TOKEN_KEY"]]dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n",boundary] dataUsingEncoding:NSUTF8StringEncoding]];
        
        
        
        [request setHTTPBody:body];
        
        
        NSString *test = [[NSString alloc]initWithData:body encoding:NSUTF8StringEncoding];
        NSLog(@"Body:%@",test);
        
        
        NSLog(@"Request:%@",request);
        
        [NSURLConnection sendAsynchronousRequest:request queue:[NSOperationQueue new] completionHandler:^(NSURLResponse *response1,NSData *data1,NSError *error1){
            
            NSString *returnStr = [[NSString alloc]initWithData:data1 encoding:NSUTF8StringEncoding];
            
            NSLog(@"Return String:%@",returnStr);
            
            
        }];
    
    }
    
    

}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
//    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
//    [alert show];
    int count = [self.navigationController.viewControllers count];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count - 3] animated:YES];
    
}
- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
    int count = [self.navigationController.viewControllers count];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count - 3] animated:YES];
}

//- (void) levelViewControllerToBack {
//    //m_bStop = YES;
//    int count = [self.navigationController.viewControllers count];
//    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count - 4] animated:YES];
//}
- (void) updateStatus
{
    //NSLog(@"timer");
    self.target1ImageX.hidden = self.target2ImageX.hidden = self.target3ImageX.hidden  = self.target4ImageX.hidden = self.target5ImageX.hidden = self.target6ImageX.hidden = YES;
    
    if ([GameManager sharedGameManager].m_target1HitNum == 3) {
        self.target1ImageX.hidden = NO;
    }
    if ([GameManager sharedGameManager].m_target2HitNum == 3) {
        self.target2ImageX.hidden = NO;
    }
    if ([GameManager sharedGameManager].m_target3HitNum == 3) {
        self.target3ImageX.hidden = NO;
    }
    if ([GameManager sharedGameManager].m_target4HitNum == 3) {
        self.target4ImageX.hidden = NO;
    }
//    if ([GameManager sharedGameManager].m_target5HitNum == 3) {
//        self.target5ImageX.hidden = NO;
//    }
//    if ([GameManager sharedGameManager].m_target6HitNum == 3) {
//        self.target6ImageX.hidden = NO;
//    }

//    if ([GameManager sharedGameManager].m_currentLevelTargetNum == 0 && !m_bBusTargetHit && m_bGameEnd) {
//        //game end
//        [self gameEnd];     
//        
//    }
}

// Override to allow orientations other than the default portrait orientation.
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
	
	/*//
	// There are 2 ways to support auto-rotation:
	//  - The OpenGL / cocos2d way
	//     - Faster, but doesn't rotate the UIKit objects
	//  - The ViewController way
	//    - A bit slower, but the UiKit objects are placed in the right place
	//
	
#if GAME_AUTOROTATION==kGameAutorotationNone
	//
	// EAGLView won't be autorotated.
	// Since this method should return YES in at least 1 orientation, 
	// we return YES only in the Portrait orientation
	//
	//return ( interfaceOrientation == UIInterfaceOrientationPortrait );
	return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
#elif GAME_AUTOROTATION==kGameAutorotationCCDirector
	//
	// EAGLView will be rotated by cocos2d
	//
	// Sample: Autorotate only in landscape mode
	//
	if( interfaceOrientation == UIInterfaceOrientationLandscapeLeft ) {
		[[CCDirector sharedDirector] setDeviceOrientation: kCCDeviceOrientationLandscapeRight];
	} else if( interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
		[[CCDirector sharedDirector] setDeviceOrientation: kCCDeviceOrientationLandscapeLeft];
	}
	
	// Since this method should return YES in at least 1 orientation, 
	// we return YES only in the Portrait orientation
	//return ( interfaceOrientation == UIInterfaceOrientationPortrait );
	return (interfaceOrientation == UIInterfaceOrientationLandscapeLeft);
#elif GAME_AUTOROTATION == kGameAutorotationUIViewController
	//
	// EAGLView will be rotated by the UIViewController
	//
	// Sample: Autorotate only in landscpe mode
	//
	// return YES for the supported orientations
	
    return ( UIInterfaceOrientationIsLandscape( interfaceOrientation ) );    
	
#else
#error Unknown value in GAME_AUTOROTATION
	
#endif // GAME_AUTOROTATION
	
	
	// Shold not happen*/
	return NO;
}

//
// This callback only will be called when GAME_AUTOROTATION == kGameAutorotationUIViewController
//
#if GAME_AUTOROTATION == kGameAutorotationUIViewController
-(void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration
{
	//
	// Assuming that the main window has the size of the screen
	// BUG: This won't work if the EAGLView is not fullscreen
	///
	CGRect screenRect = [[UIScreen mainScreen] bounds];
	CGRect rect = CGRectZero;

	
	if(toInterfaceOrientation == UIInterfaceOrientationPortrait || toInterfaceOrientation == UIInterfaceOrientationPortraitUpsideDown)		
		rect = screenRect;
	
	else if(toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight)
		rect.size = CGSizeMake( screenRect.size.height, screenRect.size.width );
	
	CCDirector *director = [CCDirector sharedDirector];
	EAGLView *glView = [director openGLView];
	float contentScaleFactor = [director contentScaleFactor];
	
	if( contentScaleFactor != 1 ) {
		rect.size.width *= contentScaleFactor;
		rect.size.height *= contentScaleFactor;
	}
	glView.frame = rect;
}
#endif // GAME_AUTOROTATION == kGameAutorotationUIViewController


- (void)didReceiveMemoryWarning {
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}

- (void)viewDidUnload {
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [[CCDirector sharedDirector] end];
    [m_imageObjects release]; m_imageObjects = nil;
    [homeButton release]; homeButton = nil;
    [driverImage release]; driverImage = nil;
    [target1Image release]; target1Image = nil;
    [target1ImageX release]; target1ImageX = nil;
    [target2Image release]; target2Image = nil;
    [target2ImageX release]; target2ImageX = nil;
    [target3Image release]; target3Image = nil;
    [target3ImageX release]; target3ImageX = nil;
    [target4Image release]; target4Image = nil;
    [target4ImageX release]; target4ImageX = nil;
    [target5Image release]; target5Image = nil;
    [target5ImageX release]; target5ImageX = nil;
    [target6Image release]; target6Image = nil;
    [target6ImageX release]; target6ImageX = nil;
    [target7Image release]; target7Image = nil;
    [target7ImageX release]; target7ImageX = nil;
    [viewForBillBoard release]; viewForBillBoard = nil;

}

- (void)dealloc {
    [checkXTimer invalidate];
    checkXTimer = nil;
    [super dealloc];
}

- (IBAction)homeTapped:(id)sender {
    
    self.target1ImageX.hidden = self.target2ImageX.hidden = self.target3ImageX.hidden  = self.target4ImageX.hidden = self.target5ImageX.hidden = self.target6ImageX.hidden = YES;
    
    if ([GameManager sharedGameManager].m_bEnabledMusic) {       
        [[SimpleAudioEngine sharedEngine] pauseBackgroundMusic];
    }
    m_bStop = YES;
    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Next_screen_swoosh.mp3"];
    }
    
    int count = [self.navigationController.viewControllers count];
    [self.navigationController popToViewController:[self.navigationController.viewControllers objectAtIndex:count - 3] animated:YES];
    
    //[self levelViewControllerToBack];
    //[self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)acceleratorDown:(id)sender {
    m_bAccelerator = YES;
    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Bus_accelerate.mp3"];
    }
}

- (IBAction)acceleratorUp:(id)sender {
    m_bAccelerator = NO;
}

- (IBAction)brakeDown:(id)sender {
    m_bBrake = YES;
    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Bus_braking.mp3"];
    }
}

- (IBAction)brakeUp:(id)sender {
    m_bBrake = NO;
}

- (void) launchCutSceenPlayer:(int)index
{
	[[CutscenePlayer sharedInstance] load:index forIpad:YES];
    
	MPMoviePlayerController* player = [[CutscenePlayer sharedInstance] theMovie];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [player.view setFrame:CGRectMake(0.0, 0.0, 480.0, 320.0)];
    }
    else {
        [player.view setFrame:CGRectMake(0.0, 0.0, 1024.0, 768.0)];
    }
	[self.view addSubview:player.view];	
	
    [[CutscenePlayer sharedInstance] setDelegate:self];
	//player.view.transform = CGAffineTransformMakeRotation(M_PI / 2); 
	//player.view.center = CGPointMake(160.0, 240.0);
	
	[[CutscenePlayer sharedInstance] play];
}

- (void) didFinishedPlay {
    //m_bBusTargetHit = NO;
    m_bMessageShow = YES;
    
    if (isCutSceen) {
        [self performSelector:@selector(launchSecuensesCutSceen)];
        //[self launchSecuensesCutSceen];
        isCutSceen = NO;
    }
}

- (void) launchSecuensesCutSceen
{
    static int k = 2;
    [[CutscenePlayer sharedInstance] load:k forIpad:YES];
    
	MPMoviePlayerController* player = [[CutscenePlayer sharedInstance] theMovie];
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [player.view setFrame:CGRectMake(0.0, 0.0, 480.0, 320.0)];
    }
    else {
        [player.view setFrame:CGRectMake(0.0, 0.0, 1024.0, 768.0)];
    }
	[self.view addSubview:player.view];
	
    [[CutscenePlayer sharedInstance] setDelegate:self];
	//player.view.transform = CGAffineTransformMakeRotation(M_PI / 2);
	//player.view.center = CGPointMake(160.0, 240.0);
	isCutSceen = YES;
	[[CutscenePlayer sharedInstance] play];
    
    k=k==2?4:2;
}

- (int) currentTargetSelectNum
{
    int num = 0;
    for (int i = 0; i < [[GameManager sharedGameManager].m_TargetArray count]; i++) 
    {
        if ([[[GameManager sharedGameManager].m_TargetArray objectAtIndex:i] boolValue]) {
            num++;
        }
    }
    return num;
}

#pragma mark - FaceBook releated methods START

- (void)checkIn {
    [fbConnect publishImageStreamWithImageUrl:fb_publish_image andName:@"Bus Rage" caption:@"Awesome Game" description:@"I am playing iPhone Bus Rage game and I am really loving it" andHref:fb_app_url];

    //[fbConnect publishStreamWithName:@"Bus Rage" caption:@"Awesome Game" description:@"I am playing iPhone Bus Rage game and I am really loving it" andHref:fb_app_url];
}

- (void) facebookLogin
{
    if (TRUE) {
        
        fbConnect=[FBConnectSingleton retrieveSingleton];
        [fbConnect setDelegate:self];
        
        if (fbConnect.isLogIn) {
            [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(checkIn) userInfo:nil repeats:NO];
        }
        else
            [fbConnect login];
        
    }
    else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Network"
                                                        message:@"Please enable your Internet"
                                                       delegate:nil cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark -
#pragma mark FB Delegate

-(void)FBConnectSingletonRequestDidLoadWithResult:(id)result{
    NSLog(@"Resukts:%@",result);
    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"id"] forKey:@"fbUserId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [self checkIn];
}

#pragma mark FaceBook releated methods END

@end

