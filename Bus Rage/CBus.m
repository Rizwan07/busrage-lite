//
//  CBus.m
//  Bus Rage
//
//  Created by Jin Tie on 5/3/12.
//  Copyright 2012 k. All rights reserved.
//

#import "CBus.h"
#import "GameManager.h"

@implementation CBus
@synthesize m_animRun;
@synthesize m_animLeft, m_animRight;//, m_animLeftCollide, m_animRightCollide, m_animLeftFlip, m_animRightFlip;   
@synthesize m_bRunState;
- (id) init {
    CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
    NSMutableArray *frames = [NSMutableArray array];
    NSString *fileName;
    CCSpriteFrame *frame;
    CCAnimation *animation;
    
    self = [super init];
    if (self) {
        m_BusSprite = [CCSprite spriteWithSpriteFrameName:[NSString stringWithFormat:@"Center_view_%02i.png", 1]];
        
        [self addChild:m_BusSprite z:0];
        //m_BusSprite.visible = NO;
        [frames removeAllObjects];
        
        //run animation
        for (int i = 1; i <= 2; i++)
        {
            fileName = [NSString stringWithFormat:@"Center_view_%02i.png", i];
            frame = [frameCache spriteFrameByName:fileName];
            [frames addObject:frame];
            
            
           // NSLog(@"Offset : %@, Size : %@",NSStringFromCGPoint(frame.offsetInPixels), NSStringFromCGSize(frame.originalSizeInPixels));
            
        }    
//        for (int i = 2; i >= 1; i--)
//        {
//            fileName = [NSString stringWithFormat:@"Center_view_%02i.png", i];
//            frame = [frameCache spriteFrameByName:fileName];
//            [frames addObject:frame];
//        } 
        animation = [CCAnimation animationWithFrames:frames delay: 0.5f];
		self.m_animRun = [CCAnimate actionWithAnimation:animation];
        [frames removeAllObjects]; 
        m_bRunState = NO;        
    }
    return self;
}
- (void) dealloc
{       
    [super dealloc];
}
- (void) setAnimRun
{
    CCRepeatForever *forever = [CCRepeatForever actionWithAction:m_animRun];
    [m_BusSprite runAction:forever];    
}
- (void) stopAnimRun
{
    [m_BusSprite stopAllActions];    
}
- (void) setAnimLeft
{
    [m_BusSprite stopAllActions];
    [m_BusSprite runAction:[CCSequence actions:m_animLeft, [CCCallFuncN actionWithTarget:self selector:@selector(runContinue)], nil]];
}
- (void) runContinue
{
    [m_BusSprite stopAllActions];
    [self setAnimRun];
}
- (void) setAnimRight
{
    [m_BusSprite stopAllActions];
    [m_BusSprite runAction:[CCSequence actions:m_animRight, [CCCallFuncN actionWithTarget:self selector:@selector(runContinue)], nil]];
}
//- (void) setAnimCollideLeft
//{
//    [m_BusSprite stopAllActions];
//    [m_BusSprite runAction:[CCSequence actions:m_animLeftCollide, [CCCallFuncN actionWithTarget:self selector:@selector(runContinue)], nil]];
//}
//- (void) setAnimCollideRight
//{
//    [m_BusSprite stopAllActions];
//    [m_BusSprite runAction:[CCSequence actions:m_animRightCollide, [CCCallFuncN actionWithTarget:self selector:@selector(runContinue)], nil]];
//}
//- (void) flitLeftContinue
//{
//    CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
//    [m_BusSprite setDisplayFrame:[frameCache spriteFrameByName:@"left_view_03_collide_01_flip_05.png"]]; 
//}
//- (void) setFlipFlag
//{
////    CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
////    [m_BusSprite setDisplayFrame:[frameCache spriteFrameByName:@"Center_view_01.png"]];
//
//}
//- (void) setAnimFlipLeft
//{
//    [GameManager sharedGameManager].m_bBusFlip = YES;
//    [m_BusSprite stopAllActions];
//    //[m_BusSprite runAction:m_animLeftFlip];
//    [m_BusSprite runAction:[CCSequence actions:m_animLeftFlip, [CCCallFuncN actionWithTarget:self selector:@selector(flitLeftContinue)], [CCDelayTime actionWithDuration:1], [CCCallFunc actionWithTarget:self selector:@selector(setFlipFlag)], nil]];
//}
//- (void) flitRightContinue
//{
//    CCSpriteFrameCache *frameCache = [CCSpriteFrameCache sharedSpriteFrameCache];
//    [m_BusSprite setDisplayFrame:[frameCache spriteFrameByName:@"Right_view_03_flip_05.png"]]; 
//}
//- (void) setAnimFlipRight
//{
//    [GameManager sharedGameManager].m_bBusFlip = YES;
//    [m_BusSprite stopAllActions];
//    //[m_BusSprite runAction:m_animRightFlip];
//    [m_BusSprite runAction:[CCSequence actions:m_animRightFlip, [CCCallFuncN actionWithTarget:self selector:@selector(flitRightContinue)], [CCDelayTime actionWithDuration:1], [CCCallFunc actionWithTarget:self selector:@selector(setFlipFlag)], nil]];
//}
@end
