//
//  SelectTargetViewController.m
//  Bus Rage
//
//  Created by Jin Tie on 5/9/12.
//  Copyright (c) 2012 k. All rights reserved.
//

#import "SelectTargetViewController.h"
#import <mach/mach.h>
#import "GameManager.h"
#import "FacebookTableViewCell.h"

@implementation SelectTargetViewController

@synthesize rootViewController = _rootViewController;
@synthesize targetImageView;
@synthesize scrollView;
@synthesize targetNationLabel;
@synthesize enterNameField;
@synthesize titleLabel;
//@synthesize m_imageObjects;
//@synthesize faceBookT;
@synthesize nextButton;
@synthesize activityIndicator;
@synthesize facebookTableView;
@synthesize facebookImage;
@synthesize messageLabel, messageView;
@synthesize m_friendButton;

- (void) print_free_memory 
{//kgh
    mach_port_t host_port;
    mach_msg_type_number_t host_size;
    vm_size_t pagesize;
    
    host_port = mach_host_self();
    host_size = sizeof(vm_statistics_data_t) / sizeof(integer_t);
    host_page_size(host_port, &pagesize);
    
    vm_statistics_data_t vm_stat;
    
    if (host_statistics(host_port, HOST_VM_INFO, (host_info_t)&vm_stat, &host_size) != KERN_SUCCESS)
        NSLog(@"Failed to fetch vm statistics");
    
    /* Stats in bytes */
    natural_t mem_used = (vm_stat.active_count + vm_stat.inactive_count + vm_stat.wire_count) * pagesize;
    natural_t mem_free = vm_stat.free_count * pagesize;
    natural_t mem_total = mem_used + mem_free;
    int iUsed = round(mem_used/100000);
    int iFree = round(mem_free/100000);
    int iTotal = round(mem_total/100000);
    NSLog(@"used: %d free: %d total: %d", iUsed, iFree, iTotal);    
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        
//        facebookImageObjectIDArray = [[NSMutableArray alloc] init];
//        for (int i = 0; i < 29; i++) 
//        {
//            NSString *tmpString = [NSString stringWithFormat:@""];
//            [facebookImageObjectIDArray addObject:tmpString];
//        }
    }
    return self;
}
- (void)didReceiveMemoryWarning
{
    // Releases the view if it doesn't have a superview.
    [super didReceiveMemoryWarning];
    
    // Release any cached data, images, etc that aren't in use.
}
- (void) dealloc
{
//    [facebookImageObjectIDArray removeAllObjects];
//    [facebookImageObjectIDArray release];
    [friends removeAllObjects];
    [friends release];
    [chooseForAllBtn release];
    [super dealloc];
}
#pragma mark
#pragma Private Methods

- (void)_addKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] addObserver:self 
                                             selector:@selector(keyboardWillAnimate:) 
                                                 name:UIKeyboardWillShowNotification 
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillAnimate:) 
                                                 name:UIKeyboardWillHideNotification 
                                               object:nil];
}


- (void)_removeKeyboardNotification
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
#pragma mark - View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];    
    
    currentTargetChoose4All = -1;
    
    // Do any additional setup after loading the view from its nib.    
    m_imageObjects = [[NSArray alloc] initWithObjects:
                      @"latin_female_render.png, LATIN",                //1 //female_latin
                      @"latin_male_render.png, LATIN",                  //2 //mexican   
                      
                      @"american_female_render_blonde.png, AMERICAN",   //3
                      @"american_female_render(brunette).png, AMERICAN",//4 //female_american
                      @"american_female_render(redhead).png, AMERICAN", //5
                      
                      @"american_male_render_blonde.png, AMERICAN",     //6
                      @"american_male_render(brunette).png, AMERICAN",  //7 //American
                      @"american_male_render(redhead).png, AMERICAN",   //8
                      
                      @"Arab_female_render.png, ARAB",                  //9 //female_arab
                      @"Arab_male_render.png, ARAB",                    //10 //Arab
                      @"Asian_female_character_background.png, ASIAN",  //11 //female_asian                         
                      @"Chinese_Character_backgound.png, ASIAN",        //12 //Asian                         
                      @"german_female_render.png, GERMAN",              //13 //female_german
                      @"german_male_render.png, GERMAN",                //14 //german
                      @"italian_female_render.png, ITALIAN",            //15 //female_italian
                      @"italian_male_line_render.png, ITALIAN",         //16 //italian
                      @"AfricanAmerican_female_render.png, AFRICAN",    //17 //female_African_american
                      @"AfricanAmerican_male_render.png, AFRICAN",      //18 //African 
                      @"russian_female_rendered.png, RUSSIAN",          //19 //female_russian
                      @"russian_male_render.png, RUSSIAN",              //20 //russian
                      @"nurse_female_render.png, NURSE",                //21 //female_nurse
                      @"nurse_male.png, NURSE",                         //22 //nurse
                      @"woman in suit_render.png, SUIT",                //23 //female_business_suit
                      @"man_in_suit_render.png, SUIT",                  //24 //man in suit
                      @"basketball_male.png, BASKETBALL",               //25 //Basketball
                      @"coverall_male_render.png, COVERALL",            //26 //Coverall
                      @"doctor_female_render.png, DOCTOR",              //27 //female_doctor
                      @"doctor_male_render.png, DOCTOR",                //28 //Doctor
                      @"football_male_render.png, FOOTBALL",            //29 //football
                      nil];
    friends = [[NSMutableArray alloc] initWithCapacity:1];
    
    // Custom initialization
    m_TargetButtonArray = [[NSMutableArray alloc] init];
    //NSLog(@"--%d", m_imageObjects.count);
    for (int i = 0; i < m_imageObjects.count; i++) {
        NSString *sLine = [m_imageObjects objectAtIndex:i];
        NSArray *params = [sLine componentsSeparatedByString:@", "];
        
        UIImage *image = [UIImage imageNamed:[params objectAtIndex:0]];
        UIButton *button = nil;
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            button = [[UIButton alloc] initWithFrame:CGRectMake(11, 8 + i * 80, 53, 65)];
        }
        else {
            button = [[UIButton alloc] initWithFrame:CGRectMake(11, 8 + i * 180, 120, 150)];
        }
        [button addTarget:self action:@selector(picClicked:) forControlEvents:UIControlEventTouchUpInside];
        //[button addTarget:self action:@selector(picDubleClicked:) forControlEvents:UIControlEventTouchDownRepeat];
        [button setImage:image forState:UIControlStateNormal];
        //[button setFrame:CGRectMake(11, 8+i*80, 53, 65)];        
        button.tag = i+1;
        [scrollView addSubview:button];
        [m_TargetButtonArray addObject:button];
        [button release];
    }
    
    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
        [scrollView setContentSize:CGSizeMake(74, m_imageObjects.count * 80)];
    }
    else {
        [scrollView setContentSize:CGSizeMake(160, m_imageObjects.count * 180)];
    }
    
    
    //enterNameField.hidden = YES;
    [self performSelector:@selector(_addKeyboardNotification)];
    nextButton.enabled = NO;
    bFacebookImageConnect = NO;
    currentSelectThumbnailNum = -1;
    
    // Message Label for showing confirmation and status messages
    CGFloat yLabelViewOffset = self.view.bounds.size.height-self.navigationController.navigationBar.frame.size.height-30;
    messageView = [[UIView alloc]
                   initWithFrame:CGRectMake(0, yLabelViewOffset, self.view.bounds.size.width, 30)];
    messageView.backgroundColor = [UIColor lightGrayColor];
    
    UIView *messageInsetView = [[UIView alloc] initWithFrame:CGRectMake(1, 1, self.view.bounds.size.width-1, 28)];
    messageInsetView.backgroundColor = [UIColor colorWithRed:255.0/255.0
                                                       green:248.0/255.0
                                                        blue:228.0/255.0
                                                       alpha:1];
    messageLabel = [[UILabel alloc]
                    initWithFrame:CGRectMake(4, 1, self.view.bounds.size.width-10, 26)];
    messageLabel.text = @"";
    messageLabel.font = [UIFont fontWithName:@"Helvetica" size:14.0];
    messageLabel.backgroundColor = [UIColor colorWithRed:255.0/255.0
                                                   green:248.0/255.0
                                                    blue:228.0/255.0
                                                   alpha:0.6];
    [messageInsetView addSubview:messageLabel];
    [messageView addSubview:messageInsetView];
    [messageInsetView release];
    messageView.hidden = YES;
    [self.view addSubview:messageView];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    switch ([GameManager sharedGameManager].m_levelNum) {
        case 1:        
        case 2:
        case 4:
        case 5:
        case 8:
            [GameManager sharedGameManager].m_currentLevelTargetNum = 2;
            break;
        case 3:
            [GameManager sharedGameManager].m_currentLevelTargetNum = 3;
            break;
        case 6:
            [GameManager sharedGameManager].m_currentLevelTargetNum = 4;
            break;
        case 7:
            [GameManager sharedGameManager].m_currentLevelTargetNum = 4;
            break;
        default:
            break;
    }
    currentSelectThumbnailNum = -1;
    [enterNameField setText:@""];
    [enterNameField setEnabled:NO];
    [m_friendButton setEnabled:NO];
    
    for (int i = 0; i < [[GameManager sharedGameManager].m_TargetArray count]; i++) 
    {
        if ([[[GameManager sharedGameManager].m_TargetArray objectAtIndex:i] boolValue]) {
            UIButton* button = [m_TargetButtonArray objectAtIndex:i];
            [button setBackgroundImage:[UIImage imageNamed:@"thumbnail_select_BG.png"] forState:UIControlStateNormal];
        }
    }
    for (int i = 0; i < 29; i++)
    {
        BOOL orgValue = [[[GameManager sharedGameManager].m_TargetArray objectAtIndex:i] boolValue]; 
        if (!orgValue) {
            UIButton *button = [m_TargetButtonArray objectAtIndex:i];
            [button setBackgroundImage:[UIImage imageNamed:@"thumbnail_unselect_BG.png"] forState:UIControlStateNormal];
        }        
    }
    if ([self currentTargetSelectNum] >= 1) {
        nextButton.enabled = YES;
    } else {
        nextButton.enabled = NO;
    }
    if ([GameManager sharedGameManager].bNetWork_Con == NO)
    {
        m_friendButton.hidden = YES;
        alert1 = [[UIAlertView alloc] initWithTitle:@"No Network" message:@"Cannot connect to facebook\n You must connect to a WiFi or cellular data network to access facebook" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert1 show];
        
        [self performSelector:@selector(waitting) withObject:nil afterDelay:0.6];
        
        return;
    }
    [GameManager sharedGameManager].bNetWork_Con = YES;
    m_friendButton.hidden = NO;
    m_bDoubleClickRun = NO;
}

- (void) waitting {
    
    while (alert1.hidden == NO && alert1.superview != nil) {
        [[NSRunLoop mainRunLoop] runUntilDate:[NSDate dateWithTimeIntervalSinceNow:0.01f]];
    }
    [alert1 release];
}
- (void)viewDidUnload
{
    [chooseForAllBtn release];
    chooseForAllBtn = nil;
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
    [self performSelector:@selector(_removeKeyboardNotification)];
    [m_imageObjects release];
    [m_TargetButtonArray removeAllObjects];
    [m_TargetButtonArray release];
    [_rootViewController release];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // Return YES for supported orientations
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (IBAction)backTapped:(id)sender {
    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Next_screen_swoosh.mp3"];
    }
    [self.navigationController popViewControllerAnimated:YES];
    if (currentSelectThumbnailNum != -1) {
        NSString *sLine = [m_imageObjects objectAtIndex:currentSelectThumbnailNum];
        NSArray *params = [sLine componentsSeparatedByString:@", "]; 
        if ([enterNameField.text isEqualToString:@""]) {
            [[GameManager sharedGameManager].m_TargetNameArray replaceObjectAtIndex:currentSelectThumbnailNum withObject:[params objectAtIndex:1]];            
        }        
    }
}

- (IBAction)nextTapped:(id)sender {    
    if ([GameManager sharedGameManager].m_bEnabledSound) {
        [[SimpleAudioEngine sharedEngine] playEffect:@"Next_screen_swoosh.mp3"];
    }
    
    if (![[GameManager sharedGameManager] isSelectAll]) {
        
        if ([GameManager sharedGameManager].m_levelNum == 1 && [self currentTargetSelectNum] < 2) {
            [self showAlert:1 limitNum:2 messageIndex:2];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 2 && [self currentTargetSelectNum] < 2) {
            [self showAlert:2 limitNum:2 messageIndex:2];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 3 && [self currentTargetSelectNum] < 3) {
            [self showAlert:3 limitNum:3 messageIndex:2];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 4 && [self currentTargetSelectNum] < 2) {
            [self showAlert:4 limitNum:2 messageIndex:2];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 5 && [self currentTargetSelectNum] < 2) {
            [self showAlert:5 limitNum:2 messageIndex:2];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 6 && [self currentTargetSelectNum] < 4) {
            [self showAlert:6 limitNum:4 messageIndex:2];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 7 && [self currentTargetSelectNum] < 4) {
            [self showAlert:7 limitNum:4 messageIndex:2];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 8 && [self currentTargetSelectNum] < 2) {
            [self showAlert:8 limitNum:2 messageIndex:2];
            return;
        }
        if (currentSelectThumbnailNum != -1) {
            NSString *sLine = [m_imageObjects objectAtIndex:currentSelectThumbnailNum];
            NSArray *params = [sLine componentsSeparatedByString:@", "]; 
            if ([enterNameField.text isEqualToString:@""]) {
                [[GameManager sharedGameManager].m_TargetNameArray replaceObjectAtIndex:currentSelectThumbnailNum withObject:[params objectAtIndex:1]];            
            }        
        }
        int j = 0;
        for (int i = 0; i < [[GameManager sharedGameManager].m_TargetArray count]; i++)
        {
            BOOL orgValue = [[[GameManager sharedGameManager].m_TargetArray objectAtIndex:i] boolValue];
            
            if (orgValue) {
                NSString *objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:i];
                //NSLog(@"objectId:%@", objectId);
                if (j == 0) {
                    [GameManager sharedGameManager].targetNum1 = i + 1;//1...29   
                    if (![objectId isEqualToString:@""]) {
                        [GameManager sharedGameManager].fbTargetImage1 = [[GameManager sharedGameManager].m_TargetfacebookImageArray objectAtIndex:i];//[self imageForObject:objectId];
                        //NSLog(@"objectID:%@", objectId);
                    }    
                    
                }
                if (j == 1) {
                    [GameManager sharedGameManager].targetNum2 = i + 1;
                    if (![objectId isEqualToString:@""]) {
                        [GameManager sharedGameManager].fbTargetImage2 = [[GameManager sharedGameManager].m_TargetfacebookImageArray objectAtIndex:i];//[self imageForObject:objectId];
                        //NSLog(@"objectID:%@", objectId);
                    }                
                }
                if (j == 2) {
                    [GameManager sharedGameManager].targetNum3 = i + 1;
                    if (![objectId isEqualToString:@""]) {
                        [GameManager sharedGameManager].fbTargetImage3 = [[GameManager sharedGameManager].m_TargetfacebookImageArray objectAtIndex:i];//[self imageForObject:objectId];
                        //NSLog(@"objectID:%@", objectId);
                    }                
                }
                if (j == 3) {
                    [GameManager sharedGameManager].targetNum4 = i + 1;
                    if (![objectId isEqualToString:@""]) {
                        [GameManager sharedGameManager].fbTargetImage4 = [[GameManager sharedGameManager].m_TargetfacebookImageArray objectAtIndex:i];//[self imageForObject:objectId];
                        //NSLog(@"objectID:%@", objectId);
                    }
                }
//                if (j == 4) {
//                    [GameManager sharedGameManager].targetNum5 = i + 1;
//                    if (![objectId isEqualToString:@""]) {
//                        [GameManager sharedGameManager].fbTargetImage5 = [[GameManager sharedGameManager].m_TargetfacebookImageArray objectAtIndex:i];//[self imageForObject:objectId];
//                        //NSLog(@"objectID:%@", objectId);
//                    }
//                }
//                if (j == 5) {
//                    [GameManager sharedGameManager].targetNum6 = i + 1;
//                    if (![objectId isEqualToString:@""]) {
//                        [GameManager sharedGameManager].fbTargetImage6 = [[GameManager sharedGameManager].m_TargetfacebookImageArray objectAtIndex:i];//[self imageForObject:objectId];
//                        //NSLog(@"objectID:%@", objectId);
//                    }
//                }
//                if (j == 6) {
//                    [GameManager sharedGameManager].targetNum7 = i + 1;
//                    if (![objectId isEqualToString:@""]) {
//                        [GameManager sharedGameManager].fbTargetImage7 = [[GameManager sharedGameManager].m_TargetfacebookImageArray objectAtIndex:i];//[self imageForObject:objectId];
//                        //NSLog(@"objectID:%@", objectId);
//                    }
//                }
                j++;
                
                //NSLog(@"%d, targetName:%@, objectID:%@", i+1, [[GameManager sharedGameManager].m_TargetNameArray objectAtIndex:i], [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:i]);
            }
        }
        facebookImage.image = [UIImage imageNamed:@"thumbnail_unselect_BG.png"];
    }

    
//    if (_selectPassengerViewController == nil) {
//        self.selectPassengerViewController = [[[SelectPassengerViewController alloc] initWithNibName:nil bundle:nil] autorelease];
//    }
//    RootViewController *rootViewController;
//    if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
//        rootViewController = [[RootViewController alloc] initWithNibName:@"RootViewController" bundle:nil];
//    }
//    else {
//        rootViewController = [[RootViewController alloc] initWithNibName:@"RootViewController_iPad" bundle:nil];
//    }
//    rootViewController.m_bStop = NO;
//    [self.navigationController pushViewController:rootViewController animated:YES];
//    
//    [rootViewController release];
    
    if (_rootViewController == nil) {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            self.rootViewController = [[[RootViewController alloc] initWithNibName:@"RootViewController" bundle:nil] autorelease];
        }
        else {
            self.rootViewController = [[[RootViewController alloc] initWithNibName:@"RootViewController_iPad" bundle:nil] autorelease];
        }
    }
    _rootViewController.m_bStop = NO;
    [self.navigationController pushViewController:_rootViewController animated:YES];
    
}

- (IBAction)chooseforallTapped:(id)sender {
    
    [[GameManager sharedGameManager] setIsSelectAll:YES];
    [chooseForAllBtn setEnabled:NO];
    [m_friendButton setEnabled:NO];
    for (int i = 0; i < 29; i++)
    {
        NSString *tmpString = [NSString stringWithFormat:@""];
        [[[GameManager sharedGameManager] m_TargetfacebookImageObjectIDArray] replaceObjectAtIndex:i withObject:tmpString];
    }
    
    NSLog(@"Selected %d", currentTargetChoose4All);
    
    switch ([[GameManager sharedGameManager] m_levelNum]) {
        case 1:
            [GameManager sharedGameManager].targetNum1 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum2 = currentTargetChoose4All;
            break;
        case 2:
            [GameManager sharedGameManager].targetNum1 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum2 = currentTargetChoose4All;
            break;
        case 3:
            [GameManager sharedGameManager].targetNum1 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum2 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum3 = currentTargetChoose4All;
            break;
        case 4:
            [GameManager sharedGameManager].targetNum1 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum2 = currentTargetChoose4All;
            break;
        case 5:
            [GameManager sharedGameManager].targetNum1 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum2 = currentTargetChoose4All;
            break;
        case 6:
            [GameManager sharedGameManager].targetNum1 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum2 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum3 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum4 = currentTargetChoose4All;
            break;
        case 7:
            [GameManager sharedGameManager].targetNum1 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum2 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum3 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum4 = currentTargetChoose4All;
//            [GameManager sharedGameManager].targetNum5 = currentTargetChoose4All;
//            [GameManager sharedGameManager].targetNum6 = currentTargetChoose4All;
            break;
        case 8:
            [GameManager sharedGameManager].targetNum1 = currentTargetChoose4All;
            [GameManager sharedGameManager].targetNum2 = currentTargetChoose4All;
            break;
        default:
            break;
    }
    
    //[self facebookLogin];
}

- (int) currentTargetSelectNum
{
    int num = 0;
    for (int i = 0; i < [[GameManager sharedGameManager].m_TargetArray count]; i++) 
    {
        if ([[[GameManager sharedGameManager].m_TargetArray objectAtIndex:i] boolValue]) {
            num++;
        }
    }
    return num;
}
- (void) showAlert:(int)levelNum limitNum:(int)limitNum messageIndex:(int)Index
{
    UIAlertView* alert = [[UIAlertView alloc] init];	
    switch (Index) {
        case 0:
            [alert setMessage:[NSString stringWithFormat:@"You can not select over %d in level%d.", limitNum, levelNum]];
            break;
        case 1:
            //[alert setMessage:[NSString stringWithFormat:@"You cannot select any passengers in level%d.", levelNum]];
            break;
        case 2:
            [alert setMessage:[NSString stringWithFormat:@"You must select %d targets in level%d.", limitNum, levelNum]];
            break;  
            
        default:
            break;
    }
    
    [alert addButtonWithTitle:@"Ok"];
    [alert setDelegate:self];
    [alert show];
    [alert release];
}
//- (IBAction)picDubleClicked:(id)sender {
////    NSLog(@"dubleclick");
//    UIButton* view = (UIButton*)sender;
//    //NSString *sLine = [m_imageObjects objectAtIndex:view.tag-1];
//    //NSArray *params = [sLine componentsSeparatedByString:@", "];
//    BOOL orgValue = [[[GameManager sharedGameManager].m_TargetArray objectAtIndex:view.tag-1] boolValue];
//    if (orgValue) {
//        [view setBackgroundImage:[UIImage imageNamed:@"thumbnail_unselect_BG.png"] forState:UIControlStateNormal];
//        [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray replaceObjectAtIndex:view.tag-1 withObject:@""];
//        [[GameManager sharedGameManager].m_TargetArray replaceObjectAtIndex:view.tag-1 withObject:@"0"];
//        [[GameManager sharedGameManager].m_TargetNameArray replaceObjectAtIndex:view.tag-1 withObject:@""];
//        UIImage *image = [UIImage imageNamed:@"thumbnail_unselect_BG.png"];
//        [[GameManager sharedGameManager].m_TargetfacebookImageArray replaceObjectAtIndex:view.tag-1 withObject:image];
//        facebookImage.image = image;
//    }
//    m_bDoubleClickRun = YES;
//}
- (IBAction)picClicked:(id)sender {    
    
    [[GameManager sharedGameManager] setIsSelectAll:NO];
    
    if (m_bDoubleClickRun) {
        m_bDoubleClickRun = NO;
        return;
    }
    
    UIButton* view = (UIButton*)sender;    
    
    NSString *sLine = [m_imageObjects objectAtIndex:view.tag-1];
    NSArray *params = [sLine componentsSeparatedByString:@", "];    
    
    //targetNationLabel.text = [params objectAtIndex:1];   
    BOOL orgValue = [[[GameManager sharedGameManager].m_TargetArray objectAtIndex:view.tag-1] boolValue];
    
    if([[GameManager sharedGameManager].m_targetDic valueForKey:[NSString stringWithFormat:@"%d",view.tag]])
    {
        if ([[[GameManager sharedGameManager].m_targetDic allKeys] containsObject:[NSString stringWithFormat:@"%d",view.tag]] ) {
            
            [[GameManager sharedGameManager].m_targetDic removeObjectForKey:[NSString stringWithFormat:@"%d",view.tag]];
        }
        selectedTargetTag = 9999;
    }
    else
    {
        
        [[GameManager sharedGameManager].m_targetDic setObject:[[NSMutableDictionary alloc]init] forKey:[NSString stringWithFormat:@"%d",view.tag]];
        [[[GameManager sharedGameManager].m_targetDic valueForKey:[NSString stringWithFormat:@"%d",view.tag]]setValue:@"" forKey:@"fb_id"];
        [[[GameManager sharedGameManager].m_targetDic valueForKey:[NSString stringWithFormat:@"%d",view.tag]]setValue:@"" forKey:@"fb_Name"];
        
        selectedTargetTag = view.tag;
        
    }
    NSLog(@"Dictionary FB:%@",[GameManager sharedGameManager].m_targetDic);
    
    if (orgValue) {
        [view setBackgroundImage:[UIImage imageNamed:@"thumbnail_unselect_BG.png"] forState:UIControlStateNormal];
        [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray replaceObjectAtIndex:view.tag-1 withObject:@""];
        [[GameManager sharedGameManager].m_TargetArray replaceObjectAtIndex:view.tag-1 withObject:@"0"];
        [[GameManager sharedGameManager].m_TargetNameArray replaceObjectAtIndex:view.tag-1 withObject:@""];
        UIImage *image = [UIImage imageNamed:@"thumbnail_unselect_BG.png"];
        [[GameManager sharedGameManager].m_TargetfacebookImageArray replaceObjectAtIndex:view.tag-1 withObject:image];
        facebookImage.image = image;
        if ([self currentTargetSelectNum] == 1) {
            [chooseForAllBtn setHidden:NO];
            [chooseForAllBtn setEnabled:YES];
            for (int i = 0; i < [[GameManager sharedGameManager].m_TargetArray count]; i++)
            {
                if ([[[GameManager sharedGameManager].m_TargetArray objectAtIndex:i] boolValue]) {
                    currentTargetChoose4All = i+1;
                    break;
                }
            }
        }
        else {
            currentTargetChoose4All = -1;
            [chooseForAllBtn setHidden:YES];
            [chooseForAllBtn setEnabled:NO];
        }
        return;
    } else {
        for (int i = 0; i < [[GameManager sharedGameManager].m_TargetArray count]; i++) 
        {
            if ([[[GameManager sharedGameManager].m_TargetArray objectAtIndex:i] boolValue]) {
                UIButton* button = [m_TargetButtonArray objectAtIndex:i];
                [button setBackgroundImage:[UIImage imageNamed:@"thumbnail_select_BG.png"] forState:UIControlStateNormal];
            }
        }
        if ([GameManager sharedGameManager].m_levelNum == 1 && [self currentTargetSelectNum] == 2) {
            [self showAlert:1 limitNum:2 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 2 && [self currentTargetSelectNum] == 2) {
            [self showAlert:2 limitNum:2 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 3 && [self currentTargetSelectNum] == 3) {
            [self showAlert:3 limitNum:3 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 4 && [self currentTargetSelectNum] == 2) {
            [self showAlert:4 limitNum:2 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 5 && [self currentTargetSelectNum] == 2) {
            [self showAlert:5 limitNum:2 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 6 && [self currentTargetSelectNum] == 4) {
            [self showAlert:6 limitNum:4 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 7 && [self currentTargetSelectNum] == 4) {
            [self showAlert:7 limitNum:4 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 8 && [self currentTargetSelectNum] == 2) {
            [self showAlert:8 limitNum:2 messageIndex:0];
            return;
        }
        //New select
        [[GameManager sharedGameManager].m_TargetArray replaceObjectAtIndex:view.tag-1 withObject:@"1"];//set bool in m_TargetArray
        //if already currentSelectThumbnailNum != -1 then target name save
        if (currentSelectThumbnailNum != -1) {
            if ([enterNameField.text isEqualToString:@""]) {
                NSString *sLine1 = [m_imageObjects objectAtIndex:currentSelectThumbnailNum];
                NSArray *params1 = [sLine1 componentsSeparatedByString:@", "];
                [[GameManager sharedGameManager].m_TargetNameArray replaceObjectAtIndex:currentSelectThumbnailNum withObject:[params1 objectAtIndex:1]];            
            }        
        }
        
        targetImageView.image = [UIImage imageNamed:[params objectAtIndex:0]];//show image
        currentSelectThumbnailNum = view.tag-1;
        facebookImage.image = [UIImage imageNamed:@"thumbnail_unselect_BG.png"];
        [view setBackgroundImage:[UIImage imageNamed:@"thumbnail_cur_select_BG.png"] forState:UIControlStateNormal];
        enterNameField.text = @"";
        [enterNameField setEnabled:YES];
        [m_friendButton setEnabled:YES];
    }
    
    // select target value
    
//    if (!orgValue) {
//    } else {  
//        [enterNameField setEnabled:YES];
//        if (currentSelectThumbnailNum == view.tag-1) {
//            //[view setBackgroundImage:[UIImage imageNamed:@"thumbnail_cur_select_BG.png"] forState:UIControlStateNormal];
//            return;
//        }
//        if (currentSelectThumbnailNum != -1) {
//            if ([enterNameField.text isEqualToString:@""]) {
//                NSString *sLine1 = [m_imageObjects objectAtIndex:currentSelectThumbnailNum];
//                NSArray *params1 = [sLine1 componentsSeparatedByString:@", "];
//                [[GameManager sharedGameManager].m_TargetNameArray replaceObjectAtIndex:currentSelectThumbnailNum withObject:[params1 objectAtIndex:1]];            
//            }        
//        }
//        targetImageView.image = [UIImage imageNamed:[params objectAtIndex:0]];//show image
//        currentSelectThumbnailNum = view.tag-1;
//        
//        [view setBackgroundImage:[UIImage imageNamed:@"thumbnail_cur_select_BG.png"] forState:UIControlStateNormal];
//        NSString *tmpStr = [[GameManager sharedGameManager].m_TargetNameArray objectAtIndex:view.tag-1];
//        NSString *sLine1 = [m_imageObjects objectAtIndex:view.tag-1];
//        NSArray *params1 = [sLine1 componentsSeparatedByString:@", "]; 
//        if ([tmpStr isEqualToString:[params1 objectAtIndex:1]]) {
//            enterNameField.text = @"";
//        } else {
//            enterNameField.text = [[GameManager sharedGameManager].m_TargetNameArray objectAtIndex:view.tag-1];
//        }
//        
//        NSString *objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:view.tag-1];
//        facebookImage.image = [UIImage imageNamed:@"thumbnail_unselect_BG.png"];
//        if (![objectId isEqualToString:@""])
//            facebookImage.image = [[GameManager sharedGameManager].m_TargetfacebookImageArray objectAtIndex:view.tag-1];// [self imageForObject:objectId];
//        [m_friendButton setEnabled:YES];
//    }
    
    //nextButton enable or disable
    if ([self currentTargetSelectNum] >= 1) {
        nextButton.enabled = YES;
    } else {
        nextButton.enabled = NO;
    }
    
    if ([self currentTargetSelectNum] == 1) {
        
        for (int i = 0; i < [[GameManager sharedGameManager].m_TargetArray count]; i++)
        {
            if ([[[GameManager sharedGameManager].m_TargetArray objectAtIndex:i] boolValue]) {
                currentTargetChoose4All = i+1;
                break;
            }
        }
        
        //currentTargetChoose4All = [view tag];
        [chooseForAllBtn setHidden:NO];
        [chooseForAllBtn setEnabled:YES];
    }
    else {
        //currentTargetChoose4All = -1;
        [chooseForAllBtn setHidden:YES];
        [chooseForAllBtn setEnabled:NO];
    }
    
}
/*- (IBAction)picClicked:(id)sender {    
    
    if (m_bDoubleClickRun) {
        m_bDoubleClickRun = NO;
        return;
    }
    UIButton* view = (UIButton*)sender;    
    
    NSString *sLine = [m_imageObjects objectAtIndex:view.tag-1];
    NSArray *params = [sLine componentsSeparatedByString:@", "];    
       
    //targetNationLabel.text = [params objectAtIndex:1];    
    for (int i = 0; i < [[GameManager sharedGameManager].m_TargetArray count]; i++) 
    {
        if ([[[GameManager sharedGameManager].m_TargetArray objectAtIndex:i] boolValue]) {
            UIButton* button = [m_TargetButtonArray objectAtIndex:i];
            [button setBackgroundImage:[UIImage imageNamed:@"thumbnail_select_BG.png"] forState:UIControlStateNormal];
        }
    }
    // select target value
    BOOL orgValue = [[[GameManager sharedGameManager].m_TargetArray objectAtIndex:view.tag-1] boolValue]; 
    
    if (!orgValue) {
        if ([GameManager sharedGameManager].m_levelNum == 1 && [self currentTargetSelectNum] == 2) {
            [self showAlert:1 limitNum:2 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 2 && [self currentTargetSelectNum] == 2) {
            [self showAlert:2 limitNum:2 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 3 && [self currentTargetSelectNum] == 3) {
            [self showAlert:3 limitNum:3 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 4 && [self currentTargetSelectNum] == 2) {
            [self showAlert:4 limitNum:2 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 5 && [self currentTargetSelectNum] == 2) {
            [self showAlert:5 limitNum:2 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 6 && [self currentTargetSelectNum] == 3) {
            [self showAlert:6 limitNum:3 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 7 && [self currentTargetSelectNum] == 1) {
            [self showAlert:7 limitNum:1 messageIndex:0];
            return;
        }
        if ([GameManager sharedGameManager].m_levelNum == 8 && [self currentTargetSelectNum] == 1) {
            [self showAlert:8 limitNum:1 messageIndex:0];
            return;
        }
        //New select
        [[GameManager sharedGameManager].m_TargetArray replaceObjectAtIndex:view.tag-1 withObject:@"1"];//set bool in m_TargetArray
        
        //if already currentSelectThumbnailNum != -1 then target name save
        if (currentSelectThumbnailNum != -1) {
            if ([enterNameField.text isEqualToString:@""]) {
                NSString *sLine1 = [m_imageObjects objectAtIndex:currentSelectThumbnailNum];
                NSArray *params1 = [sLine1 componentsSeparatedByString:@", "];
                [[GameManager sharedGameManager].m_TargetNameArray replaceObjectAtIndex:currentSelectThumbnailNum withObject:[params1 objectAtIndex:1]];            
            }        
        }
        
        targetImageView.image = [UIImage imageNamed:[params objectAtIndex:0]];//show image
        currentSelectThumbnailNum = view.tag-1;
        facebookImage.image = [UIImage imageNamed:@"thumbnail_unselect_BG.png"];
        [view setBackgroundImage:[UIImage imageNamed:@"thumbnail_cur_select_BG.png"] forState:UIControlStateNormal];
        enterNameField.text = @"";
        [enterNameField setEnabled:YES];
        [m_friendButton setEnabled:YES];
    } else {  
        [enterNameField setEnabled:YES];
        if (currentSelectThumbnailNum == view.tag-1) {
            //[view setBackgroundImage:[UIImage imageNamed:@"thumbnail_cur_select_BG.png"] forState:UIControlStateNormal];
            return;
        }
        if (currentSelectThumbnailNum != -1) {
            if ([enterNameField.text isEqualToString:@""]) {
                NSString *sLine1 = [m_imageObjects objectAtIndex:currentSelectThumbnailNum];
                NSArray *params1 = [sLine1 componentsSeparatedByString:@", "];
                [[GameManager sharedGameManager].m_TargetNameArray replaceObjectAtIndex:currentSelectThumbnailNum withObject:[params1 objectAtIndex:1]];            
            }        
        }
        targetImageView.image = [UIImage imageNamed:[params objectAtIndex:0]];//show image
        currentSelectThumbnailNum = view.tag-1;
        
        [view setBackgroundImage:[UIImage imageNamed:@"thumbnail_cur_select_BG.png"] forState:UIControlStateNormal];
        NSString *tmpStr = [[GameManager sharedGameManager].m_TargetNameArray objectAtIndex:view.tag-1];
        NSString *sLine1 = [m_imageObjects objectAtIndex:view.tag-1];
        NSArray *params1 = [sLine1 componentsSeparatedByString:@", "]; 
        if ([tmpStr isEqualToString:[params1 objectAtIndex:1]]) {
            enterNameField.text = @"";
        } else {
            enterNameField.text = [[GameManager sharedGameManager].m_TargetNameArray objectAtIndex:view.tag-1];
        }
        
        NSString *objectId = [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray objectAtIndex:view.tag-1];
        facebookImage.image = [UIImage imageNamed:@"thumbnail_unselect_BG.png"];
        if (![objectId isEqualToString:@""])
            facebookImage.image = [[GameManager sharedGameManager].m_TargetfacebookImageArray objectAtIndex:view.tag-1];// [self imageForObject:objectId];
        [m_friendButton setEnabled:YES];
    }
 
    //nextButton enable or disable
    if ([self currentTargetSelectNum] >= 1) {
        nextButton.enabled = YES;
    } else {
        nextButton.enabled = NO;
    }
}*/

- (IBAction)friendsTapped:(id)sender {
    if (bFacebookImageConnect) {
        facebookTableView.hidden = NO;
    } else {
        [self facebookLogin];
    }    
}

- (void) touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UITouch *touch;
    if ([[event allTouches] count] == 1) {
        touch = [[[event allTouches] allObjects] objectAtIndex:0];
        CGPoint point = [touch locationInView:self.view];
        /*if (CGRectContainsPoint(targetNationLabel.frame, point)) {
            targetNationLabel.hidden = YES;
            enterNameField.hidden = NO;
            titleLabel.text = @"Name Target";
        }*/   
        if (!CGRectContainsPoint(scrollView.frame, point) && !CGRectContainsPoint(facebookTableView.frame, point) && !CGRectContainsPoint(targetNationLabel.frame, point)) {
            facebookTableView.hidden = YES;
        }  
    }
}
#pragma mark
#pragma TextField Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self performSelector:@selector(blankBackgroundTapped:) withObject:nil];
    
    return YES;
}
#pragma mark
#pragma Actions

- (void)blankBackgroundTapped:(UIControl *)sender
{
    [enterNameField resignFirstResponder];
    [[GameManager sharedGameManager].m_TargetNameArray replaceObjectAtIndex:currentSelectThumbnailNum withObject:enterNameField.text];

}
- (void)keyboardWillAnimate:(NSNotification *)notification
{
    CGRect keyboardBounds;
    [[notification.userInfo valueForKey:UIKeyboardFrameEndUserInfoKey] getValue:&keyboardBounds];
    NSNumber *duration = [notification.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSNumber *curve = [notification.userInfo objectForKey:UIKeyboardAnimationCurveUserInfoKey];
    
    keyboardBounds = [self.view convertRect:keyboardBounds toView:nil];
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:[duration doubleValue]];
    [UIView setAnimationCurve:[curve intValue]];
    if([notification name] == UIKeyboardWillShowNotification)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - keyboardBounds.size.height, self.view.frame.size.width, self.view.frame.size.height)];        
    }
    else if([notification name] == UIKeyboardWillHideNotification)
    {
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + keyboardBounds.size.height, self.view.frame.size.width, self.view.frame.size.height)];
    }
    [UIView commitAnimations];
}
#pragma mark - FaceBook releated methods START

- (void) facebookLogin
{
    
    if (TRUE) {
        
        fbConnect=[FBConnectSingleton retrieveSingleton];
        [fbConnect setDelegate:self];
        if ([fbConnect isLogIn]) {
            [fbConnect getFriendsList];
        }
        else {
            [fbConnect login];
        }
        
    }
    else {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"No Network"
                                                        message:@"Please enable your Internet"
                                                       delegate:nil cancelButtonTitle:@"Ok"
                                              otherButtonTitles:nil];
        [alert show];
    }
}

#pragma mark -
#pragma mark FB Delegate

-(void)FBConnectSingletonRequestDidLoadWithResult:(id)result{
    NSLog(@"Result:%@",result);
    
    [[NSUserDefaults standardUserDefaults] setObject:[result objectForKey:@"id"] forKey:@"fbUserId"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [fbConnect getFriendsList];
}

-(void)FBConnectSingletonFriendListRecievedWithList:(NSArray *)results {
    
    NSLog(@"Friends %@",results);
    
    if ([results count] > 0) {
        for (NSUInteger i = 0; i < [results count] && i < 25; i++) {
            [friends addObject:[results objectAtIndex:i]];
        }
        [facebookTableView reloadData];
        facebookTableView.hidden = NO;
        bFacebookImageConnect = YES;
    } else {
        [self showMessage:@"You have no friends."];
    }
}

-(void)fbDidNotLogin:(BOOL)cancelled {
    
    NSLog(@"did not login");
    UIAlertView* alert = [[UIAlertView alloc] init];
    [alert setMessage:@"Failed to login to face book"];
	[alert addButtonWithTitle:@"Ok"];
	[alert setDelegate:self];
	[alert show];
	[alert release];
    //[ActivityView removeFromSuperview];
}

#pragma mark - Private Helper Methods
/*
 * Helper method to return the picture endpoint for a given Facebook
 * object. Useful for displaying user, friend, or location pictures.
 */
- (UIImage *)imageForObject:(NSString *)objectID {
    // Get the object image
    NSString *url = [[NSString alloc] initWithFormat:@"https://graph.facebook.com/%@/picture",objectID];
    UIImage *image = [UIImage imageWithData:[NSData dataWithContentsOfURL:[NSURL URLWithString:url]]];
    [url release];
    return image;
}

/*
 * This method is used to display API confirmation and
 * error messages to the user.
 */
- (void)showMessage:(NSString *)message {
    CGRect labelFrame = messageView.frame;
    labelFrame.origin.y = [UIScreen mainScreen].bounds.size.height - self.navigationController.navigationBar.frame.size.height - 20;
    messageView.frame = labelFrame;
    messageLabel.text = message;
    messageView.hidden = NO;
    
    // Use animation to show the message from the bottom then
    // hide it.
    [UIView animateWithDuration:0.5
                          delay:1.0
                        options: UIViewAnimationCurveEaseOut
                     animations:^{
                         CGRect labelFrame = messageView.frame;
                         labelFrame.origin.y -= labelFrame.size.height;
                         messageView.frame = labelFrame;
                     }
                     completion:^(BOOL finished){
                         if (finished) {
                             [UIView animateWithDuration:0.5
                                                   delay:3.0
                                                 options: UIViewAnimationCurveEaseOut
                                              animations:^{
                                                  CGRect labelFrame = messageView.frame;
                                                  labelFrame.origin.y += messageView.frame.size.height;
                                                  messageView.frame = labelFrame;
                                              }
                                              completion:^(BOOL finished){
                                                  if (finished) {
                                                      messageView.hidden = YES;
                                                      messageLabel.text = @"";
                                                  }
                                              }];
                         }
                     }];
}

/*
 * This method hides the message, only needed if view closed
 * and animation still going on.
 */
- (void)hideMessage {
    messageView.hidden = YES;
    messageLabel.text = @"";
}
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self hideMessage];
}
#pragma mark - UITableView Datasource and Delegate Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40.0;
}

// Customize the number of sections in the table view.
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [friends count];
}

// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    FacebookTableViewCell *cell = (FacebookTableViewCell *)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [FacebookTableViewCell cellWithReuseIdentifier:CellIdentifier];
        // Show disclosure only if this view is related to showing nearby places, thus allowing
        // the user to check-in.
        //if ([self.myAction isEqualToString:@"places"]) {
        //    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        //}
    }
    
    /*cell.textLabel.text = [[friends objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.textLabel.font = [UIFont boldSystemFontOfSize:14.0];
    cell.textLabel.lineBreakMode = UILineBreakModeWordWrap;
    cell.textLabel.numberOfLines = 2;
    // If extra information available then display this.
    if ([[friends objectAtIndex:indexPath.row] objectForKey:@"details"]) {
        cell.detailTextLabel.text = [[friends objectAtIndex:indexPath.row] objectForKey:@"details"];
        cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:12.0];
        cell.detailTextLabel.lineBreakMode = UILineBreakModeCharacterWrap;
        cell.detailTextLabel.numberOfLines = 2;
    }
    // The object's image
    //cell.imageView.image = [self imageForObject:[[friends objectAtIndex:indexPath.row] objectForKey:@"id"]];
    cell.imageView.image = [UIImage imageNamed:@"thumbnail_unselect_BG.png"];   
    // Configure the cell.*/
    
    NSDictionary * streamItem = nil;
    
    if ([indexPath row] < [friends count]){
        streamItem = [friends objectAtIndex:[indexPath row]]; 
    }
    
    cell.file = streamItem;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // Only handle taps if the view is related to showing nearby places that
    // the user can check-in to.
    //if ([self.myAction isEqualToString:@"places"]) {
    //    [self apiGraphUserCheckins:indexPath.row];
    //}
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (currentSelectThumbnailNum > -1) {
        facebookImage.image = [self imageForObject:[[friends objectAtIndex:indexPath.row] objectForKey:@"id"]];
        enterNameField.text = [[friends objectAtIndex:indexPath.row] objectForKey:@"name"];
        NSString *tmpString;// = [[GameManager sharedGameManager].facebookImageObjectIDArray objectAtIndex:currentSelectThumbnailNum];
        tmpString = [[friends objectAtIndex:indexPath.row] objectForKey:@"id"];
        [[GameManager sharedGameManager].m_TargetfacebookImageObjectIDArray replaceObjectAtIndex:currentSelectThumbnailNum withObject:tmpString];
        NSLog(@"TMPString Waqas:%@",tmpString);
        NSLog(@"Test Waqas > Selected Target:%@",[GameManager sharedGameManager].m_TargetArray);
        [[GameManager sharedGameManager].m_TargetfacebookImageArray replaceObjectAtIndex:currentSelectThumbnailNum withObject:facebookImage.image];
        NSLog(@"Test Waqas > Selected Image Target:%@",facebookImage.image);
        
        NSLog(@"Selected Targeted Image :%d",selectedTargetTag);
        
        [[[GameManager sharedGameManager].m_targetDic valueForKey:[NSString stringWithFormat:@"%d",selectedTargetTag]]setValue:tmpString forKey:@"fb_id"];
        [[[GameManager sharedGameManager].m_targetDic valueForKey:[NSString stringWithFormat:@"%d",selectedTargetTag]]setValue:enterNameField.text forKey:@"fb_Name"];
        
        NSLog(@"Dic:%@",[GameManager sharedGameManager].m_targetDic);
        
        facebookTableView.hidden = YES;
    }
}
@end
