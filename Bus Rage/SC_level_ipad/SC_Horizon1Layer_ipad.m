//
//  SC_Horizon1Layer.m
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import "SC_Horizon1Layer_ipad.h"


@implementation SC_Horizon1Layer_ipad
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        horizonSprite = [CCSprite spriteWithFile:@"Horizon_SV_ipad.png"];
        [self addChild:horizonSprite z:0];
        horizonSprite.visible = YES;
        horizonSprite.position = ccp(447*2.1333333, 160*2.4);//447...33 
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    if (runDistance == 0 || horizonSprite.position.x < 33.0*2.1333333) {
        horizonSprite.position = ccp(447*2.1333333, 160*2.4);
    }
    if (speed > 0) {
        CGPoint pos;
        pos = horizonSprite.position;
        pos.x -= 0.5 * speed/2;
        horizonSprite.position = pos; 
    }     
}
@end
