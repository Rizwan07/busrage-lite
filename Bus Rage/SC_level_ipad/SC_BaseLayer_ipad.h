//
//  SC_BaseLayer.h
//  Bus Rage
//
//  Created by Jin Tie on 5/26/12.
//  Copyright 2012 k. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "cocos2d.h"

@interface SC_BaseLayer_ipad : CCLayer {
    CCSprite* m_groundSprite;
}
- (void) runMove:(int)runDistance speed:(int)speed;
@end
