//
//  SC_TreesLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import "SC_TreesLayer_ipad.h"
#import "EnumConstances.h"

#define SHOW_LIMIT 170

@implementation SC_TreesLayer_ipad
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        m_Objects = [NSArray arrayWithObjects://SC:7,8, 9,10,11

                     @"9, 100",//school1
                     @"10, 150",//school2
                     @"11, 200",//school3
                     //@"7, 250",//house1
                     //@"8, 300",//house2
                     
                     @"9, 350",//school1
                     @"7, 400",//house1
                     @"8, 450",//house2
                     @"10, 500",//school2
                     @"11, 550",//school3
                     
                     
                     @"9, 650",//school1
                     @"10, 700",//school2
                     @"11, 750",//school3
                     @"7, 800",//house1
                     @"8, 850",//house2
                     nil];
        int house1_index = 0;
        int houes2_index = 0;
        int school1_index = 0;
        int school2_index = 0;
        int school3_index = 0;
        for (int i = 0; i < [m_Objects count]; i++)
        {
            NSString *sLine = [m_Objects objectAtIndex:i];
            NSArray *params = [sLine componentsSeparatedByString:@", "];
            int nKind = [[params objectAtIndex:0] intValue];
            switch (nKind) {
                case T_OINFO_HOUSE1:                
                    m_House1[house1_index] = [[params objectAtIndex:1] intValue];
                    house1_index++;                
                    break;	
                case T_OINFO_HOUSE2:                
                    m_House2[houes2_index] = [[params objectAtIndex:1] intValue];
                    houes2_index++;                
                    break;   
                case T_OINFO_SCHOOL1:
                    m_School1[school1_index] = [[params objectAtIndex:1] intValue];
                    school1_index++;
                    break;
                case T_OINFO_SCHOOL2:
                    m_School2[school2_index] = [[params objectAtIndex:1] intValue];
                    school2_index++;
                    break;
                case T_OINFO_SCHOOL3:
                    m_School3[school3_index] = [[params objectAtIndex:1] intValue];
                    school3_index++;
                    break;
            }
        }        
        house1Sprite = [CCSprite spriteWithFile:@"home_1_SV_ipad.png"];
        [self addChild:house1Sprite z:2];        
        house1Sprite.visible = NO;
        
        house2Sprite = [CCSprite spriteWithFile:@"house_2_SV_ipad.png"];
        [self addChild:house2Sprite z:2];        
        house2Sprite.visible = NO;
        
        school1Sprite = [CCSprite spriteWithFile:@"school_1_SC_ipad.png"];
        [self addChild:school1Sprite z:1];
        school1Sprite.visible = NO;
        
        school2Sprite = [CCSprite spriteWithFile:@"school_2_SC_ipad.png"];
        [self addChild:school2Sprite z:1];
        school2Sprite.visible = NO;
        
        school3Sprite = [CCSprite spriteWithFile:@"school_3_SC_ipad.png"];
        [self addChild:school3Sprite z:1];
        school3Sprite.visible = NO;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    [self showHouse1:runDistance];
    [self showHouse2:runDistance];
    [self showSchool1:runDistance];
    [self showSchool2:runDistance];
    [self showSchool3:runDistance];    
}
- (void) showSchool1:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 4; i++)
    {
        tmpPos = m_School1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    school1Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        school1Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;
        school1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a 
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        school1Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        school1Sprite.scale = school1Sprite.scale * 8;
    }
}
- (void) showSchool2:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 4; i++)
    {
        tmpPos = m_School2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    school2Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        school2Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;
        school2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a 
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        school2Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        school2Sprite.scale = school2Sprite.scale * 8;
    }
}
- (void) showSchool3:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 4; i++)
    {
        tmpPos = m_School3[i]; 
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    school3Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        school3Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;
        school3Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a 
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        school3Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        school3Sprite.scale = school3Sprite.scale * 8;
    }
}
- (void) showHouse1:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_House1[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    house1Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        house1Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;
        house1Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a 
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        house1Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        house1Sprite.scale = house1Sprite.scale * 4;
    }
}
- (void) showHouse2:(int)runDistance
{
    int diff_BusTree = -1;
    int diff[3];
    int count = 0;
    int tmpPos;
    for (int i = 0; i < 3; i++)
    {
        tmpPos = m_House2[i];
        if (runDistance > 900 && tmpPos <= 300 && tmpPos > 0) {
            tmpPos += 1200;
        }
        if (tmpPos - runDistance > 0) {
            diff[count] = tmpPos - runDistance;
            count++;
        }
    }
    for (int i = 0; i < count; i++)
    {
        if (i == 0) {
            diff_BusTree = diff[i];
        } else {
            diff_BusTree = MIN(diff_BusTree, diff[i]);                
        }
    }
    house2Sprite.visible = NO;
    if (diff_BusTree > 0 && diff_BusTree < SHOW_LIMIT) {
        house2Sprite.visible = YES;
        
        float ss = 70.0;
        float sum = -350.0;
        for (int i = 0; i < 50; i ++)
        {  
            sum += ss;         
            if (i  == diff_BusTree / 10) {
                sum += ss * 0.897 / 10 * (diff_BusTree % 10);            
                break;
            }
            ss = ss * 0.897;             
        }
        
        float ss1 = 25.0;
        float sum1 = 0.0;
        for (int i = 0; i < 50; i ++)
        {        
            sum1 += ss1;
            if (i == diff_BusTree / 10) {
                sum1 += ss1 * 0.831 / 10 * (diff_BusTree % 10);
                break;
            }        
            ss1 = ss1 * 0.831;//0.840;             
        }
        CGPoint cur_pos;
        cur_pos.x = sum*2.1333333;
        cur_pos.y = sum1*2.4;
        house2Sprite.position = cur_pos;
        
        float dist1 = ccpDistance(CGPointMake(-350.0*2.1333333, 0), CGPointMake(326.0*2.1333333, 147.0*2.4));//a 
        float dist2 = ccpDistance(cur_pos, CGPointMake(326.0*2.1333333, 147.0*2.4));//b
        house2Sprite.scale = 0.2 * (dist2 + 0.17647 * dist1) / 1.17647 / dist1;
        house2Sprite.scale = house2Sprite.scale * 4;
    }
}
@end
