//
//  SC_HillsLayer.m
//  Bus Rage
//
//  Created by Jin Tie on 5/15/12.
//  Copyright 2012 k. All rights reserved.
//

#import "SC_HillsLayer_ipad.h"


@implementation SC_HillsLayer_ipad
-(id) init
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self = [super init])) {
        
        hill1Sprite = [CCSprite spriteWithFile:@"school_1_SC_ipad.png"];
        [self addChild:hill1Sprite z:2];        
        hill1Sprite.visible = YES;
        hill1Sprite.position = ccp(250*2.1333333, 144*2.4);
        hill1Sprite.scale = 0.6;
        
        hill2Sprite = [CCSprite spriteWithFile:@"school_2_SC_ipad.png"];
        [self addChild:hill2Sprite z:1];        
        hill2Sprite.visible = YES;
        hill2Sprite.position = ccp(100*2.1333333, 140*2.4);
        hill2Sprite.scale = 0.6;
	}
	return self;
}
- (void) dealloc
{
	[super dealloc];
}
- (void) runMove:(int)runDistance speed:(int)speed
{
    if (runDistance < 10) {
        hill1Sprite.position = ccp(250*2.1333333, 144*2.4);
        hill2Sprite.position = ccp(100*2.1333333, 140*2.4);
    }
    if (speed > 0) {
        CGPoint pos;
        pos = hill1Sprite.position;
        pos.x -= 0.4 * speed/2;
        hill1Sprite.position = pos; 
        
        pos = hill2Sprite.position;
        pos.x -= 0.4 * speed/2;
        hill2Sprite.position = pos;  
    }
    
}
@end
